
import pandas as pd
import os
from google.cloud import bigquery
from google.cloud import storage
import pickle
os.environ['GOOGLE_APPLICATION_CREDENTIALS']=f"/home/jovyan/.config/gcloud/legacy_credentials/{os.getenv('JUPYTERHUB_USER')}/adc.json"
import numpy as np
import sys
from datetime import timedelta
import datetime


#Statistical LTV
from lifetimes import BetaGeoFitter, GammaGammaFitter
from lifetimes.utils import calibration_and_holdout_data, summary_data_from_transaction_data

#ML Approach to LTV
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
import tensorflow_docs as tfdocs
import tensorflow_docs.plots
import tensorflow_docs.modeling

#Evaluation
from sklearn.metrics import r2_score
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import mean_squared_error

#Plotting
import matplotlib.pyplot as plt
import seaborn as sns
np.set_printoptions(threshold=sys.maxsize)
pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 500)
#from pandas_profiling import ProfileReport


project_id = 'gcp-wow-rwds-ai-cfv-dev'


#query = """drop table if exists akelly.campaignanalysis"""

#CampaignId='29956'
#supermarkets
query = """select * from akelly.ecom_base_2020_to_20213m_with_kimchi3m_with_cust_seg3m
"""

data = pd.read_gbq(query, project_id=project_id)




