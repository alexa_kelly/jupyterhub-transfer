from feature_store import FeatureStore
from feature_store.model import Feature


def main():
    FEATURE_STORE_ENV = 'prod'

    fs = FeatureStore(env=FEATURE_STORE_ENV)

    supers_instore_response = fs.management.define_feature_set(
        feature_group_name='cfv',
        feature_set_name='supers_instore_cfv',
        key_columns=['crn'],
        description="Supers Instore CFV Segments",
        meta={"project": "cfv"},
        owners = ['akelly5@woolworths.com.au'],
        features=[
            Feature('ref_dt', 'Original CFV ref_dt',),
            Feature('cfv_instore_score_spend', 'Final Supers Instore spend score',),
            Feature('instore_CAV', 'Final Supers Instore CFV Segment', ),
        ],
    )




    supers_online_response = fs.management.define_feature_set(
        feature_group_name='cfv',
        feature_set_name='supers_online_cfv',
        key_columns=['crn'],
        description="Supers Online CFV Segments",
        meta={"project": "cfv"},
        owners = ['akelly5@woolworths.com.au'],
        features=[
            Feature('ref_dt', 'Original CFV ref_dt',),
            Feature('cfv_ecom_score_spend', 'Final Supers online spend score',),
            Feature('ecom_CAV', 'Final Supers online CFV Segment', ),
        ],
    )




    supers_online3m_response = fs.management.define_feature_set(
        feature_group_name='cfv',
        feature_set_name='supers_online3m_cfv',
        key_columns=['crn'],
        description="Supers Online 3m CFV Segments",
        meta={"project": "cfv"},
        owners = ['akelly5@woolworths.com.au'],
        features=[
            Feature('ref_dt', 'Original CFV ref_dt',),
            Feature('cfv_ecom3m_score_spend', 'Final Supers online 3m spend score',),
            Feature('ecom3m_CAV', 'Final supers online 3m CFV Segment', ),
        ],
    )

    bigw_instore_response = fs.management.define_feature_set(
        feature_group_name='cfv',
        feature_set_name='bigw_instore_cfv',
        key_columns=['crn'],
        description="BigW Instore CFV Segments",
        meta={"project": "cfv"},
        owners = ['akelly5@woolworths.com.au'],
        features=[
            Feature('ref_dt', 'Original CFV ref_dt',),
            Feature('cfv_bigw_instore_score_spend', 'Final bigw Instore spend score',),
            Feature('bigw_instore_CAV', 'Final Supers bigw CFV Segment', ),
        ],
    )

    bigw_online_response = fs.management.define_feature_set(
        feature_group_name='cfv',
        feature_set_name='bigw_online_cfv',
        key_columns=['crn'],
        description="BigW Online CFV Segments",
        meta={"project": "cfv"},
        owners = ['akelly5@woolworths.com.au'],
        features=[
            Feature('ref_dt', 'Original CFV ref_dt',),
            Feature('cfv_bigw_online_score_spend', 'Final bigw online spend score',),
            Feature('bigw_online_CAV', 'Final bigw online CFV Segment', ),
        ],
    )

    bws_instore_response = fs.management.define_feature_set(
        feature_group_name='cfv',
        feature_set_name='bws_instore_cfv',
        key_columns=['crn'],
        description="BWS Instore CFV Segments",
        meta={"project": "cfv"},
        owners = ['akelly5@woolworths.com.au'],
        features=[
            Feature('ref_dt', 'Original CFV ref_dt',),
            Feature('cfv_bws_instore_score_spend', 'Final bws Instore spend score',),
            Feature('bws_instore_CAV', 'Final bws Instore CFV Segment', ),
        ],
    )

    bws_online_response = fs.management.define_feature_set(
        feature_group_name='cfv',
        feature_set_name='bws_online_cfv',
        key_columns=['crn'],
        description="BWS Online CFV Segments",
        meta={"project": "cfv"},
        owners = ['akelly5@woolworths.com.au'],
        features=[
            Feature('ref_dt', 'Original CFV ref_dt',),
            Feature('cfv_bws_online_score_spend', 'Final bws online spend score',),
            Feature('bws_online_CAV', 'Final bws online CFV Segment', ),
        ],
    )

if __name__ == '__main__':
    main()








CREATE OR REPLACE TABLE gcp-wow-rwds-ai-features-prod.staging.supers_instore_cfv
(
crn STRING      
,ref_dt DATE        
,cfv_instore_score_spend FLOAT64        
,instore_CAV STRING     
)
PARTITION BY ref_dt;

CREATE OR REPLACE TABLE gcp-wow-rwds-ai-features-prod.staging.supers_online_cfv
(
crn STRING      
,ref_dt DATE        
,cfv_ecom_score_spend FLOAT64        
,ecom_CAV STRING     
)
PARTITION BY ref_dt;

CREATE OR REPLACE TABLE gcp-wow-rwds-ai-features-prod.staging.supers_online3m_cfv
(
crn STRING      
,ref_dt DATE        
,cfv_ecom3m_score_spend FLOAT64        
,ecom3m_CAV STRING     
)
CREATE OR REPLACE TABLE gcp-wow-rwds-ai-features-prod.staging.bigw_instore_cfv
(
crn STRING      
,ref_dt DATE        
,cfv_bigw_instore_score_spend FLOAT64        
,bigw_instore_CAV STRING     
)
PARTITION BY ref_dt;

CREATE OR REPLACE TABLE gcp-wow-rwds-ai-features-prod.staging.bigw_online_cfv
(
crn STRING      
,ref_dt DATE        
,cfv_bigw_online_score_spend FLOAT64        
,bigw_online_CAV STRING     
)
PARTITION BY ref_dt;


CREATE OR REPLACE TABLE gcp-wow-rwds-ai-features-prod.staging.bws_instore_cfv
(
crn STRING      
,ref_dt DATE        
,cfv_bws_instore_score_spend FLOAT64        
,bws_instore_CAV STRING     
)
PARTITION BY ref_dt;

CREATE OR REPLACE TABLE gcp-wow-rwds-ai-features-prod.staging.bws_online_cfv
(
crn STRING      
,ref_dt DATE        
,cfv_bws_online_score_spend FLOAT64        
,bws_online_CAV STRING     
)
PARTITION BY ref_dt;



















