
import pandas as pd
import os
from google.cloud import bigquery
from google.cloud import storage
import pickle
os.environ['GOOGLE_APPLICATION_CREDENTIALS']=f"/home/jovyan/.config/gcloud/legacy_credentials/{os.getenv('JUPYTERHUB_USER')}/adc.json"
import numpy as np
import sys
np.set_printoptions(threshold=sys.maxsize)
pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 500)
from pandas_profiling import ProfileReport
import matplotlib.pyplot as plt
import matplotlib
import seaborn as sns
from matplotlib.ticker import ScalarFormatter
from sklearn.metrics import mean_squared_error
import statsmodels.api as sm
import subprocess
from feature_store import FeatureStore
import datetime
project_id = 'gcp-wow-rwds-ai-cfv-dev'

sys.path.insert(0, '/home/jovyan/cfv/score')

project_id = 'gcp-wow-rwds-ai-cfv-prod'
BQ_ENV = 'dev_score'
FS_ENV = 'test'



    def get_ref_ts(self, table: str) -> datetime.datetime:
        """
        Read CFV BQ output and return relevant ref_ts that is required for Featue Storm Ingestion.
        
        Currently, expects a view/table to be supplied that contains only one unique ref_dt.
        """
        sql = f'SELECT ref_dt FROM {table} GROUP BY ref_dt'
        ref_dts = self._bq_client.query(sql).to_dataframe()['ref_dt'].to_list()
        assert len(ref_dts) == 1 , "Number of ref_dts in target view is not exactly 1"
        ref_dt = ref_dts[0]
        ref_ts = datetime.datetime(ref_dt.year, ref_dt.month, ref_dt.day)
        return ref_ts


FS_FEATURE_GROUP = 'cfv'
FS_SUPERS_INSTORE_FEATURE_SET = 'supers_instore_cfv'
FS_SUPERS_INSTORE_BQ_TABLE = f'{project_id}.{BQ_ENV}.fs_cfv_supers_instore_output'
FS_SUPERS_ONLINE_FEATURE_SET = 'supers_online_cfv'
FS_SUPERS_ONLINE_BQ_TABLE = f'{project_id}.{BQ_ENV}.fs_cfv_supers_online_output'



feature_set = FS_SUPERS_INSTORE_FEATURE_SET 
table = FS_SUPERS_INSTORE_BQ_TABLE
ref_ts = get_ref_ts(table)

response = self._fs.batch.ingest_features_from_bq(
    source_table=table,
    feature_group=FS_FEATURE_GROUP,
    feature_set=feature_set,
    ref_ts=ref_ts,
    grant_data_viewer_role_to_feature_store=False
)





_bq_client = bigquery.Client('gcp-wow-rwds-ai-cfv-prod')


_bq_client = bigquery.Client('gcp-wow-rwds-ai-cfv-dev')
_fs = FeatureStore(env='dev')
#chmod +x ./install_requirements.sh
#pip install --upgrade pip setuptools wheel
#pip install --upgrade streamlit
#pip install --upgrade pyarrow
#sudo pip install akl.tar.gz --no-binary :all:
#pip install akl.tar.gz--no-cache-dir
#pip --verbose install akl.tar.gz
#AKL_VERSION=0.5
#gsutil cp gs://wx-personal/rds-release/akl/akl-$AKL_VERSION.tar.gz akl.tar.gz
#Collecting pyarrow==0.12.0
#pip install akl-0.5.tar.gz

from model_configfortest import Config
from utils import copy_file, Logger
import pandas as pd
import argparse
import pickle
import pyarrow.parquet as pq
import lightgbm as lgb
import numpy as np
from akl.preprocessor.numeric import PreprocessorNum
from akl.preprocessor.categorical import PreprocessorCat



pd.options.mode.chained_assignment = None  

testprod.shape
 
testprod=pd.read_parquet('gs://wx-321b539d-04ab-4be0-a707-524efb7a98d8/scoring/dev/runs/20220106testprodfull/output/partition_9/base_data.parquet')
testprod=pd.DataFrame(testprod.count()/len(testprod))
testprod.reset_index(level=0, inplace=True)
testprod.columns=['var','nonnulls prod']
testprod.head()


 
testdev=pd.read_parquet('gs://wx-lty-cfv-dev/scoring/dev/runs/20220106test/output/partition_9/base_data.parquet')
testdev.shape
testdev=pd.DataFrame(testdev.count()/len(testdev))
testdev.reset_index(level=0, inplace=True)
testdev.columns=['var','nonnulls dev']
testdev.head()

comparenulls=pd.merge(testprod,testdev,how='inner',on='var')
comparenulls.head()

comparenulls['prod less dev']=comparenulls['nonnulls prod']-comparenulls['nonnulls dev']

nullcheck=test2.count()/len(test2)
for i in nullcheck:
 print(i)
 
 
 

for i in test2.columns:
 print(i)

1100000000122142116
1100000000093432605
testdev=pd.read_parquet('gs://wx-lty-cfv-dev/scoring/dev/runs/20220106test/base_audience.parquet')
test2=pd.read_parquet('gs://wx-lty-cfv-dev/scoring/prod/runs/20220106revised/base_audience.parquet')





test.head()
test2.shape

e3m=pd.read_parquet('gs://wx-lty-cfv-dev/scoring/dev/runs/20220106/output/combined/ecom3m_spend_scored.parquet')


e3m.head()
e3m[e3m.crn=='1100000000122142116']
e[e.crn=='1100000000122142116']




e=pd.read_parquet('gs://wx-lty-cfv-dev/scoring/dev/runs/20220106/output/combined/ecom_spend_scored.parquet')


e3ms=pd.read_parquet('gs://wx-lty-cfv-dev/scoring/dev/runs/20220106/ecom3m_scored_segmented.parquet')
e3ms.head()
e3ms[e3ms.crn=='1100000000122142116']


e3ms=pd.read_parquet('gs://wx-lty-cfv-dev/scoring/dev/runs/20220106/instore_scored_segmented.parquet')

d=pd.read_parquet('gs://wx-lty-cfv-dev/scoring/dev/runs/20220106/output/combined/instore_spend_scored.parquet')
d.groupby('score').count()
d['score'].plot()

 
 instore_scored_segmented.parquet
 instore_spend_scored.parquet

OUTPUT_DATA_LOCATION= 'gs://wx-lty-cfv-dev/scoring/prod/runs/20220106/output'
bernoulli_path=f'{OUTPUT_DATA_LOCATION}/combined/instore_bernoulli_scored.parquet'
spend_path=f'{OUTPUT_DATA_LOCATION}/combined/instore_spend_scored.parquet'
n=48
MODELS_LOCATION='gs://wx-lty-cfv-dev/scoring/prod/models'
scaling_factor_path=f'{MODELS_LOCATION}/instore_spend/scaling_factor.parquet'
dollar_bands_path=f'{MODELS_LOCATION}/instore_spend/dollar_bands.parquet'
BASE_DATA_LOCATION='gs://wx-lty-cfv-dev/scoring/prod/runs/20220106'
output_path=f'{BASE_DATA_LOCATION}/instore_scored_segmented.parquet'





def bernoulli_classifier(bernoulli_path: str) -> pd.DataFrame:
    """
    Read combined Bernoulli scored audience from GCS, sort and return as dataframe
    """
    df = (pd.read_parquet(bernoulli_path, engine='pyarrow')
        .sort_values(by='score', ascending=False)
        .rename(columns={"score": "score_bernoulli"})
        )

    return df


def load_spend(spend_path: str) -> pd.DataFrame:
    """
    Read combined spend scored audience from GCS and return as dataframe
    """
    df = (pd.read_parquet(spend_path, engine='pyarrow')
        .rename(columns={"score": "score_spend"})
        )

    return df


def merger(df_bernoulli: pd.DataFrame, df_spend: pd.DataFrame, n: int) -> pd.DataFrame:
    """
    Merge Bernoulli and Spend model scores. Take the top n% (0 < n < 100) as spend and calculate percentile rank.
    """
    df = (df_bernoulli
        .merge(df_spend, how='left', on=['crn', 'ref_dt'])
        )
    df['pred_band'] = pd.qcut(df.head(int(df.shape[0]*(n/100)))['score_spend'].rank(method='first'), q=100).cat.rename_categories(list(range(1, 101)))
   # df['score_spend'].loc[df['score_bernoulli']<0.50] =-1
    df['pred_band'] = df['pred_band'].cat.codes
    df['pred_band'] = df['pred_band'] + 1

    return df
   
   
   
   
   
   
   
   
   
11751147/16746216

def merger_threshold(df_bernoulli: pd.DataFrame, df_spend: pd.DataFrame, n: int) -> pd.DataFrame:
    """
    Merge Bernoulli and Spend model scores. Take the score_bernoulli > n as spend and calculate percentile rank.
    n is used as a threshold value for bernoulli score instead of percentage
    """
    df = (df_bernoulli
        .merge(df_spend, how='left', on=['crn', 'ref_dt'])
        )
    df['pred_band'] = pd.qcut(df.loc[df['score_bernoulli'] > n, 'score_spend'].rank(method='first'), q=100).cat.rename_categories(list(range(1, 101)))
    df['score_spend'].loc[df['score_bernoulli']<58] =-1
    df['pred_band'] = df['pred_band'].cat.codes
    df['pred_band'] = df['pred_band'] + 1

    return df


def adjusted_score_getter(scaling_factor_path: str, df: pd.DataFrame) -> pd.DataFrame:
    """
    From combined dataframe, get scaling factors and merge by percentile. Calculate adjusted spend by multiplying raw spend
    by scaling factor.
    """
    scaling_factor = pd.read_parquet(scaling_factor_path, engine='pyarrow')
    df = (df
        .merge(scaling_factor[['pred_band', 'scaling_factor']], how='left', on=['pred_band'])
        )
    df['score_spend'].loc[df['scaling_factor'].isnull()] =-1
    df['adjusted_score_spend'] = df['score_spend'] 

    return df


def dollar_bands_getter(dollar_bands_path: str, df: pd.DataFrame) -> pd.DataFrame:
    """
    Get dollar bands that define CFV segments, and assign segment based on adjusted spend.
    """
    dollar_bands_ref = pd.read_parquet(dollar_bands_path, engine='pyarrow')
    dollar_bands_ref['Upper Bound'] = dollar_bands_ref['Upper Bound'].fillna(np.inf)
    dollar_bands_ref['Lower Bound'] = dollar_bands_ref['Lower Bound'].fillna(0)
    l1 = sorted([-np.inf] + list(np.unique(dollar_bands_ref[['Lower Bound', 'Upper Bound']].values)))
    l2 = ['zero'] + dollar_bands_ref['Category'].tolist()
    df['pred_bin'] = pd.cut(df['adjusted_score_spend'], bins=l1,labels=l2)
    df['ref_dt'] = df['ref_dt'].dt.date

    return df


   

   
df_bernoulli = bernoulli_classifier(bernoulli_path)
df_spend = load_spend(spend_path)
df_merge = merger(df_bernoulli, df_spend, n)
df_merge2 = adjusted_score_getter(scaling_factor_path, df_merge)
df_segments = dollar_bands_getter(dollar_bands_path, df_merge2)

   
 df_merge2.head()  
   

    def bernoulli_classifier(bernoulli_path: str) -> pd.DataFrame:
        """
        Read combined Bernoulli scored audience from GCS, sort and return as dataframe
        """
        df = (pd.read_parquet(bernoulli_path, engine='pyarrow')
            .sort_values(by='score', ascending=False)
            .rename(columns={"score": "score_bernoulli"})
            )

        return df


    def load_spend(spend_path: str) -> pd.DataFrame:
        """
        Read combined spend scored audience from GCS and return as dataframe
        """
        df = (pd.read_parquet(spend_path, engine='pyarrow')
            .rename(columns={"score": "score_spend"})
            )

        return df


    def merger2(df_bernoulli: pd.DataFrame, df_spend: pd.DataFrame, n: int) -> pd.DataFrame:
        """
        Merge Bernoulli and Spend model scores. Take the top n% (0 < n < 100) as spend and calculate percentile rank.
        """
        df = (df_bernoulli
            .merge(df_spend, how='left', on=['crn', 'ref_dt'])
            )
        df['pred_band2'] = pd.qcut(df.head(int(df.shape[0]*(n/100)))['score_bernoulli'].rank(method='first'), q=100).cat.rename_categories(list(range(1, 101)))
        df['pred_band2'] = df['pred_band2'].cat.codes
        df['pred_band2'] = df['pred_band2'] + 1

        return df
  
        df['pred_band'] = pd.qcut(df.head(int(df.shape[0]*(n/100)))['score_bernoulli'].rank(method='first'), q=100).cat.rename_categories(list(range(1, 101)))
        df['pred_band'] = df['pred_band'].cat.codes
        df['pred_band'] = df['pred_band'] + 1

 
df.head()

    def adjusted_score_getter(scaling_factor_path: str, df: pd.DataFrame) -> pd.DataFrame:
        """
        From combined dataframe, get scaling factors and merge by percentile. Calculate adjusted spend by multiplying raw spend
        by scaling factor.
        """
        scaling_factor = pd.read_parquet(scaling_factor_path, engine='pyarrow')
        df = (df
            .merge(scaling_factor[['pred_band', 'scaling_factor']], how='left', on=['pred_band'])
            )

        df['adjusted_score_spend'] = df['score_spend'] * df['scaling_factor']
        df['adjusted_score_spend'] = df['adjusted_score_spend'].fillna(-1)

        return df


    def dollar_bands_getter(dollar_bands_path: str, df: pd.DataFrame) -> pd.DataFrame:
        """
        Get dollar bands that define CFV segments, and assign segment based on adjusted spend.
        """
        dollar_bands_ref = pd.read_parquet(dollar_bands_path, engine='pyarrow')
        dollar_bands_ref['Upper Bound'] = dollar_bands_ref['Upper Bound'].fillna(np.inf)
        dollar_bands_ref['Lower Bound'] = dollar_bands_ref['Lower Bound'].fillna(0)
        l1 = sorted([-np.inf] + list(np.unique(dollar_bands_ref[['Lower Bound', 'Upper Bound']].values)))
        l2 = ['zero'] + dollar_bands_ref['Category'].tolist()
        df['pred_bin'] = pd.cut(df['score_spend'], bins=l1,labels=l2)
        df['ref_dt'] = df['ref_dt'].dt.date

        return df

   
   

   
df_bernoulli = bernoulli_classifier(bernoulli_path)
df_spend = load_spend(spend_path)
df_merge = merger(df_bernoulli, df_spend, n)
df_adjusted = adjusted_score_getter(scaling_factor_path, df_merge)



df_segments = dollar_bands_getter(dollar_bands_path, df_merge2)
df_merge2 = merger2(df_bernoulli, df_spend, n)



df_merge2.head()



df_merge[df_merge.crn=='3300000000001852256']
df_adjusted[df_adjusted.crn=='3300000000001852256']

df_spend[df_spend.crn=='1000000000001133377']

df_adjusted[df_adjusted.adjusted_score_spend==-1].iloc[1:10]



df_adjusted[df_adjusted.adjusted_score_spend==-1]['score_bernoulli'].agg(['min','mean','max'])


df_merge2.groupby(['pred_band'])['score_bernoulli'].agg(['min','mean','max'])



df_bernoulli.head(int(df_bernoulli.shape[0]*(n/100))
        df_bernoulli['pred_band'] = pd.qcut()['score_spend'].rank(method='first'), q=100).cat.rename_categories(list(range(1, 101)))
        df_bernoulli['pred_band'] = df_bernoulli['pred_band'].cat.codes
        df_bernoulli['pred_band'] = df_bernoulli['pred_band'] + 1


df=df_merge2

e3ms[e3ms.crn=='3300000000001852256']
df.head()
df_spend[df_spend.crn=='3300000000001852256']
df_segments[df_segments.crn=='3300000000001852256']
df.head()
df[df.crn=='3300000000001852256']

11751147
8708033
 
df_merge.groupby('pred_band').count() 
 
df_segments.groupby('pred_bin').count() 
df.groupby(['ref_dt','pred_bin'])['score_bernoulli'].agg(['min','mean','max'])

    fig = plt.figure(figsize=(10,6))

    sns.kdeplot(df_segments['score_spend'], color='green', shade=True, Label='Target')

    # Setting the X and Y Label
    plt.xlabel('Test Total Spend')
    plt.ylabel('Probability Density')
    plt.title('Total distribution', fontsize=20)
    fig.legend(labels=['Target','Predicted'])

    plt.show()

df.count()
#16749196


df.head()

minusone=df[df['adjusted_score_spend']==-1]
minusone.head()


df['score_spend'].loc[df['scaling_factor'].isnull()] =-1

#8374153

[print('3') for i in df['scaling_factor'] if i.isnull()]
    if df['scaling_factor']==None:
     print('2')


outted=pd.read_parquet(output_path)
outted.shape
df['adjusted_score_spend'].max()

8374621/16749241

CFV_PROJECT='gcp-wow-rwds-ai-cfv-dev'
BQ_ENV='dev'
STAGING_SCHEMA = {
    "crn": "STRING",
    "ref_dt": "DATE",
    "score_spend": "FLOAT64",
    "score_bernoulli": "FLOAT64",
    "pred_band": "INT64",
    "scaling_factor": "FLOAT64",
    "adjusted_score_spend": "FLOAT64",
    "pred_bin": "STRING",
}
_bq_client = bigquery.Client(CFV_PROJECT)
_bq_storage = bigquery_storage_v1.BigQueryReadClient()
_stg_schema = get_staging_schema(STAGING_SCHEMA)
_stg_tbl_prefix = f'{CFV_PROJECT}.{BQ_ENV}.stg_'
_sql_merge = Config.OUTPUT_MERGE_SQL
_sql_merge_score_combo = Config.OUTPUT_MERGE_SQL_SUPERS_COMBO
_sql_merge_group_view= Config.OUTPUT_MERGE_SQL_GROUP_VIEW
_sql_merge_score_combo_stage= Config.STAGE_OUTPUT_MERGE_SQL_SUPERS_COMBO





q="""select * from 
supers.supers_target where crn in('1100000000001091762', '3300000000005406425')"""
pd.read_gbq(q, project_id=project_id)

q="""select * from 
gcp-wow-ent-im-wowx-cust-prod.adp_wowx_dm_customer_view.dim_customer_v 
where CustomerRegistrationNumber in ('1100000000001091762','3300000000005406425')"""
pd.read_gbq(q, project_id=project_id)


q="""select * from gcp-wow-ent-im-wowx-cust-prod.adp_wowx_dm_customer_view.loyalty_card_detail_v
where CustomerRegistrationNumber in ('1100000000001091762') or PrimaryCustomerRegistrationNumber in('3300000000005406425')"""
d3=pd.read_gbq(q, project_id=project_id)

d3=d3.fillna(0)

q="""select * from gcp-wow-ent-im-wowx-cust-prod.adp_wowx_dm_customer_view.loyalty_card_detail_v
where CustomerRegistrationNumber in ('1100000000001091762') or PrimaryCustomerRegistrationNumber in('3300000000005406425')"""
d3=pd.read_gbq(q, project_id=project_id)

	-1924965719876027837
-5671888743652571469	
q="""select * from 
gcp-wow-ent-im-wowx-cust-prod.adp_wowx_dm_customer_view.dim_customer_v 
where CustomerRegistrationNumber in ('1100000000005283355','1100000000094231533')"""
d0=pd.read_gbq(q, project_id=project_id)




q='select a.crn, a.ref_dt, a.supers_tot_spend as new_supers_tot_spend, b.supers_tot_spend as old_supers_tot_spend, a.supers_tot_spend - b.supers_tot_spend as diff_supers_tot_spend from akelly.NEW_instore_base_2020_to_2021_with_kimchi_with_cust_seg_offer  as a join supers.supers_target as b on a.crn=b.crn and a.ref_dt=b.ref_dt;'
d=pd.read_gbq(q, project_id=project_id)

 

q="""select * from gcp-wow-ent-im-wowx-cust-prod.adp_wowx_dm_customer_view.loyalty_card_detail_v
where CustomerRegistrationNumber in ('1100000000094231533') or PrimaryCustomerRegistrationNumber in('1100000000094231533')"""
d3=pd.read_gbq(q, project_id=project_id)


scaling_factor_path='gs://wx-lty-cfv-dev/scoring/dev/models/ecom_spend/scaling_factor.parquet'
scaling_factor = pd.read_parquet(scaling_factor_path, engine='pyarrow')


gcloud container clusters get-credentials cfv-prod --region us-central1 --project gcp-wow-rwds-ai-cfv-dev
and then
kubectl port-forward service/argo-server 2746:2746
and then go to
http://localhost:2746/workflows/









base_aud=pd.read_parquet('gs://wx-lty-cfv-dev/scoring/dev/runs/20211212/base_audience.parquet', engine='pyarrow')
base_aud.head()


q="""select * from  wx-bq-poc.ecom.ecom_onln_features where ref_dt = '2021-12-05' order by 1 limit 5;"""
d1=pd.read_gbq(q, project_id=project_id)
