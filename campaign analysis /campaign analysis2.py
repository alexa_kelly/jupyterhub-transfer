
import pandas as pd
import os
from google.cloud import bigquery
from google.cloud import storage
import pickle
os.environ['GOOGLE_APPLICATION_CREDENTIALS']=f"/home/jovyan/.config/gcloud/legacy_credentials/{os.getenv('JUPYTERHUB_USER')}/adc.json"
import numpy as np
import sys
np.set_printoptions(threshold=sys.maxsize)
pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 500)
#from pandas_profiling import ProfileReport

#spreadsheet location https://docs.google.com/spreadsheets/d/17ZsLzPjtbOIQmqhaWn62roa-kcuG41FgnGBYfC_qJB4/edit#gid=0
project_id = 'gcp-wow-rwds-ai-cfv-dev'
20211108/
dd=pd.read_parquet('gs://wx-lty-cfv-dev/scoring/dev/runs/20211108/base_audience.parquet')
dd=pd.read_parquet('gs://wx-lty-cfv-dev/scoring/dev/runs/20220106/base_audience.parquet')

dd=pd.read_parquet('gs://wx-lty-cfv-dev/scoring/prod/runs/20220210revised/base_audience.parquet')

dd.head()

dd.shape
 

q=""" select * from `gcp-wow-ent-im-wowx-cust-prod`.adp_wowx_dm_customer_view.loyalty_card_detail_v  where PrimaryCustomerRegistrationNumber= '3300000000001852256'
    or CustomerRegistrationNumber='3300000000001852256';"""


q="""  select * from score.cfv_instore_segments where crn='1000000000001133377' order by ref_dt desc;"""




q=""" select * from feature_build.base_cust where crn='3300000000001852256';"""

q="""  select * from feature_build.cfv_supers_txn_w8  where crn='3300000000001852256';"""

q="""  select * from score.cfv_instore_segments where crn='3300000000000544774' order by ref_dt desc;"""

q="""  select count(*) from score.cfv_instore_segments where adjusted_score_spend=-1 and ref_dt='2022-01-02';"""

pd.read_gbq(q, project_id=project_id)


pd.read_gbq('drop table if exists akelly.campaignanalysis', project_id=project_id)

query = """
create table akelly.campaignanalysis as 
with base as (
select cast(crn as string) as crn ,"control" as aud_type from `gcp-wow-rwds-ai-cfv-dev.asach.all_campaign_audience` 
where crn not in (select cast(crn as string) as crn 
                                    from wx-bq-poc.loyalty.et_targeted_customer_offer 
                                    where campaign_code= 'RTN-4879')
union DISTINCT
select cast(crn as string) as crn ,"test" as aud_type from `gcp-wow-rwds-ai-cfv-dev.asach.all_campaign_audience` 
where crn in (select cast(crn as string) as crn from wx-bq-poc.loyalty_campaign_analytics.rtn4879_final where pred_bin = 'hvcfv' )
and cast(crn as string) in (select cast(crn as string) as crn 
                                    from wx-bq-poc.loyalty.et_targeted_customer_offer 
                                    where campaign_code= 'RTN-4879' )


UNION DISTINCT 
 
SELECT b.crn, "test1" as aud_type 
    from `wx-bq-poc.wx_lty_digital_attribution_dev.tealium_events` as a 
    join wx-bq-poc.adh_own_data.redx_loyalty_customer_encrypted_detail as b on
    a.udo_user_profile_crn_hash=b.crn_enc
    where 
    (udo_campaign_offer_id='-218616' or udo_campaign_code='RTN-4879' )
     and udo_page_name like 'ww-rw:offers%'
     and udo_tealium_event = 'rw_offer_activation_success'
)

,sent as (
select distinct crn,1 as send_ind 
from wx-bq-poc.loyalty.et_targeted_customer_offer
where campaign_code= 'RTN-4879'
)

,activation as (
Select distinct(cecoa.crn),1 as act_ind
    --    ce.cmpgn_code as campaign_code, offer_id
FROM        wx-bq-poc.loyalty.campaign_exec ce
INNER JOIN  wx-bq-poc.loyalty.campaign_exec_cust_offer_allocation cecoa 
on          ce.cmpgn_exec_id = cecoa.cmpgn_exec_id
and         cecoa.offer_id IN (-218616,-216217) 
WHERE       cecoa.activation_ts IS NOT NULL 
AND         ce.cmpgn_code = 'RTN-4879'  
AND         ce.test_exec_ind != 'Y'
)


,temp_base_aud as (
       select bse.crn,aud_type,
       case when send_ind=1 then 1 else 0 end as send_ind
      ,case when act_ind=1 or aud_type='test2' then 1 else 0 end as act_ind
from base as bse
left join sent 
on cast(bse.crn as string)= sent.crn
left join activation as act 
on cast(bse.crn as string)=act.crn
)


,add_loyalty as ( 
select b.PrimaryCustomerRegistrationNumber as crn, b.LoyaltyCardNumber,min(LoyaltyCardRegistrationDate) as lylty_card_rgstr_date 
from `gcp-wow-ent-im-wowx-cust-prod`.adp_wowx_dm_customer_view.loyalty_card_detail_v b
WHERE b.PrimaryCustomerRegistrationNumber in (select crn from temp_base_aud)
group by 1,2)

select a.*,
case 
when a.act_ind  =1 then 'accepted'
when a.aud_type='test' and a.send_ind=1 then 'ignored'
else 'control' end as cust_type
,c.LoyaltyCardNumber
from temp_base_aud as a 
left join add_loyalty as c on a.crn=c.crn



"""
df=pd.read_gbq(query, project_id=project_id)


df[df['crn']=='1100000000130206552']




pd.read_gbq('drop table if exists akelly.campaignanalysis2', project_id=project_id)
query ="""
create table akelly.campaignanalysis2 as 
WITH booster AS (
SELECT a.udo_campaign_code,a.udo_user_profile_crn_hash,b.crn, 
min(udo_campaign_start) as activated_date,
count(*) as boosts
    from `wx-bq-poc.wx_lty_digital_attribution_dev.tealium_events` as a 
    join wx-bq-poc.adh_own_data.redx_loyalty_customer_encrypted_detail as b on
    a.udo_user_profile_crn_hash=b.crn_enc
    where 
    (udo_campaign_offer_id='-218616' or udo_campaign_code='RTN-4879' )
     and udo_page_name like 'ww-rw:offers%'
     and udo_tealium_event = 'rw_offer_activation_success'
    group by 1,2,3
),

total_boosts AS (
SELECT a.udo_campaign_code,a.udo_user_profile_crn_hash,b.crn, 
count(*) as total_boosts
    from `wx-bq-poc.wx_lty_digital_attribution_dev.tealium_events` as a 
    join wx-bq-poc.adh_own_data.redx_loyalty_customer_encrypted_detail as b on
    a.udo_user_profile_crn_hash=b.crn_enc
    where 
    (udo_campaign_offer_id='-218616' or udo_campaign_code='RTN-4879' )
     and udo_page_name like 'ww-rw:offers%'
     and udo_tealium_event = 'rw_offer_activation_success'
    group by 1,2,3
)

select a.*, b.activated_date, b.boosts ,c.total_boosts from akelly.campaignanalysis as a left join booster as b
on a.crn=b.crn left join total_boosts as c on a.crn=c.crn
"""


df2=pd.read_gbq(query, project_id=project_id)


pd.read_gbq('drop table if exists akelly.campaignanalysis3', project_id=project_id)
query ="""
create table akelly.campaignanalysis3 as 
WITH dateformat AS (
	select *, 
	case 
		when activated_date is not null then activated_date 
		else '2021-08-19' end as activation
	from akelly.campaignanalysis2
	)

select 
a.crn,
a.cust_type,
a.total_boosts,
EXTRACT(WEEK from b.TXNStartDate) as week,
case when b.TXNStartDate>= a.activation then 'After activation' else 'Before activation' end as spend_period,
min(b.TXNStartDate) as week_start,
sum(b.TotalAmountIncldTax) as weekly_spend
from gcp-wow-ent-im-wowx-cust-prod.adp_wowx_dm_integrated_sales_view.article_sales_summary_v as b
join dateformat as a on a.LoyaltyCardNumber=b.LoyaltyCardNumber
where 
b.TXNStartDate>="2021-06-24" and b.TXNStartDate<="2021-10-13" 
AND b.SalesOrg in(1030, 1005)
AND b.VoidFlag <> 'Y' 
AND b.TotalAmountIncldTax>0
AND b.POSNumber <> 100 
group by 1,2,3 ,4,5
"""


df3=pd.read_gbq('select * from akelly.campaignanalysis3', project_id=project_id)

#df3.to_feather('df3.feather')
#agg2.to_feather('agg2.feather')
df3 = pd.read_feather('df3.feather')
agg1 = pd.read_feather('agg1.feather')

df3.head()

df3.groupby('cust_type').count()
df3.groupby('cust_type').crn.nunique()
df.groupby('aud_type').crn.nunique()




df3[df3['cust_type']=='accepted']

df3[ df3.total_boosts>1]

df.groupby('crn').count().sort_values('cust_type',ascending=False)

df2[df2['crn']=='3300000000003806710']

def multiboost(x):
    if x==1:
        return 'single booster'
    if x>1:
        return 'multi booster'
    
    
df3['multiboost']=df3['total_boosts'].apply(multiboost)


df3.count()/len(df3)
df3=df3.fillna(0)
summary_by_cust=df3.groupby(['crn','cust_type','multiboost'	,'week_start' , 'week',	'spend_period'	], as_index=False)['weekly_spend'].agg('mean')


summary_by_cust2=summary_by_cust.groupby(['crn','cust_type','multiboost','spend_period'	], as_index=False)['weekly_spend'].agg('mean')

summary_by_cust3 = summary_by_cust2.pivot(index=['crn','cust_type'], columns='spend_period', values='weekly_spend')

summary_by_cust3 = summary_by_cust3.reset_index()           


summary_by_cust4 = summary_by_cust2[summary_by_cust2.cust_type=='accepted'].pivot(index=['crn','multiboost'], columns='spend_period', values='weekly_spend')

summary_by_cust4 = summary_by_cust4.reset_index()           



def moreless(x,y):
    if x==y:
        return 'Same'
    if x<y:
        return 'Higher weekly avgerage $'
    else:
        return 'Lower weekly average $'

    
    
    
summary_by_cust3['moreandless']= summary_by_cust3.apply(lambda x:moreless(x['Before activation'], x['After activation']) ,axis=1)
 
summary_by_cust4['moreandless']= summary_by_cust4.apply(lambda x:moreless(x['Before activation'], x['After activation']) ,axis=1)
forg=summary_by_cust3.groupby(['cust_type','moreandless'])['crn'].count()
    
    
    
df3[df3['crn']=='1100000000004605626']

summary_by_cust[summary_by_cust['crn']=='1100000000004605626']

df3.groupby('cust_type').crn.nunique()

df2.to_feather('df2.feather')
df2[df2['crn']=='1000000000000003861']
query ="""select 
*
from gcp-wow-ent-im-wowx-cust-prod.adp_wowx_dm_integrated_sales_view.article_sales_summary_v
where LoyaltyCardNumber in ('-5239615366835387440' )
and TXNStartDate>="2021-06-20" and TXNStartDate<="2021-10-10" 
AND SalesOrg in(1030, 1005)
AND VoidFlag <> 'Y' 
AND TotalAmountIncldTax>0
AND POSNumber <> 100 

"""
pd.read_gbq(query, project_id=project_id)

df['crn'].nunique()
df['LoyaltyCardNumber'].nunique()

noloyalty=df[df['LoyaltyCardNumber'].isna()]


#DROP TABLE IF EXISTS akelly.campaignanalysis3_long; 
query ="""

create table akelly.campaignanalysis3_long as 
WITH dateformat AS (
	select *, 
	case 
		when activated_date is not null then activated_date 
		else '2021-08-19' end as activation
	from akelly.campaignanalysis2
	)

select 
a.crn,
a.cust_type,
a.total_boosts,
EXTRACT(WEEK from b.TXNStartDate) as week,
case when b.TXNStartDate>= a.activation then 'After activation' else 'Before activation' end as spend_period,
BasketKey,
sum(b.TotalAmountIncldTax) as BasketKey_spend
from gcp-wow-ent-im-wowx-cust-prod.adp_wowx_dm_integrated_sales_view.article_sales_summary_v as b
join dateformat as a on a.LoyaltyCardNumber=b.LoyaltyCardNumber
where 
b.TXNStartDate>="2021-06-24" and b.TXNStartDate<="2021-10-13" 
AND b.SalesOrg in(1030, 1005)
AND b.VoidFlag <> 'Y' 
AND b.TotalAmountIncldTax>0
AND b.POSNumber <> 100 
group by 1,2,3 ,4,5,6;
"""
d3_2=pd.read_gbq(query, project_id=project_id)


pd.read_gbq('DROP TABLE IF EXISTS akelly.campaignanalysis4; ', project_id=project_id)
query ="""

create table akelly.campaignanalysis4 as 
WITH base_crn as 
(
        select crn

    from `gcp-wow-rwds-ai-cfv-dev.azhang.true_audience_4879` 
    where pred_bin = 'hvcfv' 
        and cast(crn as string) in (select cast(crn as string) as crn 
                                    from wx-bq-poc.loyalty.et_targeted_customer_offer 
                                    where campaign_code= 'RTN-4879')
    

),
Activation as 
(
Select distinct(cecoa.crn),1 as act_ind
FROM        wx-bq-poc.loyalty.campaign_exec ce
INNER JOIN  wx-bq-poc.loyalty.campaign_exec_cust_offer_allocation cecoa 
on          ce.cmpgn_exec_id = cecoa.cmpgn_exec_id
and         cecoa.offer_id IN (-218616,-216217) 
WHERE       cecoa.activation_ts IS NOT NULL 
AND         ce.cmpgn_code = 'RTN-4879'  
AND         ce.test_exec_ind != 'Y'
),
RANKED AS 
(
    SELECT 
          D.crn
          ,D.pw_end_date
          ,ROW_NUMBER() OVER (PARTITION BY D.crn ORDER BY D.pw_end_date) AS DateRank    
          ,CASE WHEN substr(D.segment_cell_curr,1,2) = 'HV' THEN 'high value'
                WHEN substr(D.segment_cell_curr,1,2) = 'MV' THEN 'medium value'
                WHEN substr(D.segment_cell_curr,1,2) = 'LV' THEN 'low value'
                ELSE D.segment_cell_curr
           END AS cvm
          ,CASE WHEN substr(D.segment_cell_curr,1,2) = 'HV' THEN 3
                WHEN substr(D.segment_cell_curr,1,2) = 'MV' THEN 2
                WHEN substr(D.segment_cell_curr,1,2) = 'LV' THEN 1
                ELSE 0
           END AS cvm_rank

    FROM `wx-bq-poc.loyalty.customer_value_model` D
       WHERE D.pw_end_date in ("2021-10-13","2021-06-24"))

,Transition AS
	(
    SELECT
        case when a.act_ind=1 then 1 else 0 end as act_ind
        ,bs.crn
        -- ,bs.aud_type
        ,FORMAT_DATE('%Y-%m-%d', src.pw_end_date) AS SourceDate

        ,src.cvm as SourceCVM
        ,src.cvm_rank as SourceCVMRank
        
        ,FORMAT_DATE('%Y-%m-%d', tgt.pw_end_date) AS TargetDate
        ,tgt.cvm as TargetCVM
        ,tgt.cvm_rank as TargetCVMRank

        ,CASE WHEN tgt.cvm_rank - src.cvm_rank = 0 THEN 'stable'
            WHEN tgt.cvm_rank - src.cvm_rank < 0 THEN 'down'
            WHEN tgt.cvm_rank - src.cvm_rank > 0 THEN 'up'
        END AS cvm_trend

FROM    
        base_crn bs
        left join Activation a
        on cast(bs.crn as string)=a.crn
        LEFT JOIN 
        RANKED AS src
        ON cast(bs.crn as string)=src.crn
    INNER JOIN 
        RANKED AS tgt
    ON src.crn = tgt.crn
        AND src.DateRank + 1 = tgt.DateRank)
, BothT AS
(
    select
    T.crn
    ,T.SourceDate
    ,T.TargetDate
    ,act_ind
    ,T.SourceCVM
    ,T.TargetCVM
from Transition T 
)

select a.SourceDate	,a.TargetDate	,a.act_ind	,a.SourceCVM	,a.TargetCVM	, b.* from akelly.campaignanalysis3 as b left join BothT as a on cast(a.crn as string)=cast(b.crn as string)

"""
d4=pd.read_gbq(query, project_id=project_id)
d4.head()
query ="""

select * from 
`gcp-wow-ent-im-wowx-cust-prod`.adp_wowx_dm_customer_view.loyalty_card_detail_v
where CustomerRegistrationNumber='1100000000096193737'

"""
d=pd.read_gbq(query, project_id=project_id)

d[['PrimaryCustomerRegistrationNumber',	'PrimaryLoyaltyCardNumber','LoyaltyCardStatusDescription','LoyaltyRewardAccountStatusDescription','LoyaltyCardRegistrationDate']]

query = """
create table akelly.campaignanalysis3_wtran as 
select 
b.crn,
b.cust_type,
b.before_instore_spend_w8 ,b.after_instore_spend_w8  ,b.LoyaltyCardNumber,
sum(case when c.TXNStartDate>="2021-08-15" then  c.TotalAmountIncldTax else 0 end) as tot_spend_after,
sum(case when c.TXNStartDate<"2021-08-15" then  c.TotalAmountIncldTax else 0 end) as tot_spend_before
from gcp-wow-ent-im-wowx-cust-prod.adp_wowx_dm_integrated_sales_view.article_sales_summary_v as c
join akelly.campaignanalysis3 as b on c.LoyaltyCardNumber=b.LoyaltyCardNumber
where 
c.TXNStartDate>="2021-06-20" and c.TXNStartDate<="2021-10-10" 
AND c.SalesOrg in(1030, 1005)
AND c.VoidFlag <> 'Y' 
AND c.TotalAmountIncldTax>0
AND c.POSNumber <> 100 
group by 1,2,3 ,4,5
"""

df2=pd.read_gbq(query, project_id=project_id)

query = """

select 
b.crn,
b.cust_type,
b.before_instore_spend_w8 ,b.after_instore_spend_w8  ,
sum(case when c.TXNStartDate>="2021-08-15" then  c.TotalAmountIncldTax else 0 end) as tot_spend_after,
sum(case when c.TXNStartDate<"2021-08-15" then  c.TotalAmountIncldTax else 0 end) as tot_spend_before
from gcp-wow-ent-im-wowx-cust-prod.adp_wowx_dm_integrated_sales_view.article_sales_summary_v as c
join akelly.campaignanalysis3 as b on c.LoyaltyCardNumber=b.LoyaltyCardNumber
where 
c.TXNStartDate>="2021-06-20" and c.TXNStartDate<="2021-10-10" 
AND c.SalesOrg in(1030, 1005)
AND c.VoidFlag <> 'Y' 
AND c.TotalAmountIncldTax>0
AND c.POSNumber <> 100 
group by 1,2,3 ,4
"""

df2=pd.read_gbq(query, project_id=project_id)
query = """drop table if exists akelly.campaignanalysis3_wtran"""
pd.read_gbq(query, project_id=project_id)
#accepted 1100000000093996430 , 	-2213106048285267709
#1100000000043910727 , 92840201938884970


query = """select * from wx-bq-poc.loyalty.campaign_exec_cust_offer_allocation
where Crn='1100000000093996430'"""
pd.read_gbq(query, project_id=project_id)

query = """Select cecoa.*,ce.* ,1 as act_ind
    --    ce.cmpgn_code as campaign_code, offer_id
FROM        wx-bq-poc.loyalty.campaign_exec ce
INNER JOIN  wx-bq-poc.loyalty.campaign_exec_cust_offer_allocation cecoa 
on          ce.cmpgn_exec_id = cecoa.cmpgn_exec_id
and         cecoa.offer_id IN (-218616,-216217) 
WHERE       cecoa.activation_ts IS NOT NULL 
AND         cecoa.crn='1100000000093996430'
AND         ce.cmpgn_code = 'RTN-4879'  
AND         ce.test_exec_ind != 'Y'"""
d=pd.read_gbq(query, project_id=project_id)
d.to_csv('check.csv')

query = """select * from wx-bq-poc.loyalty.et_resp_sendlog where campaign_code = 'RTN-4879' and crn='1100000000093996430'"""
d=pd.read_gbq(query, project_id=project_id)
d.columns

pd.read_gbq("""select * from akelly.campaignanalysis3_wtran where crn='1000000000000462379'""", project_id=project_id)
df2.head()

query = """select * from wx-bq-poc.loyalty.customer_activated_offers where  crn='1100000000093996430' and lms_offer_id='-218616'"""
pd.read_gbq(query, project_id=project_id)

query = """select * from wx-bq-poc.loyalty_bi_analytics.dim_campaign_offer where campaign_code = 'RTN-4879'"""
pd.read_gbq(query, project_id=project_id)


query = """select * from wx-bq-poc.loyalty.campaign_exec_cust_offer_allocation where crn = '1100000000093996430' and offer_nbr='-218616'"""
pd.read_gbq(query, project_id=project_id)

query = """select offer_alloc_start_ts,offer_alloc_end_ts,redemption_limit, count(*) from wx-bq-poc.loyalty.campaign_exec_cust_offer_allocation  where offer_nbr='-218616'
group by 1,2,3"""
pd.read_gbq(query, project_id=project_id)


loyalty.customer_activated_offers



df2.groupby('cust_type').count()
df2[df2['cust_type']=='accepted'].iloc[1:4]

df2['before_instore_spend_w8'].sum()
df2['tot_spend_before'].sum()

df2['after_instore_spend_w8'].sum()
df2['tot_spend_after'].sum()

df2['after_dif']=pd.to_numeric(df2['after_instore_spend_w8'])-pd.to_numeric(df2['tot_spend_after'])
df2['before_dif']=pd.to_numeric(df2['before_instore_spend_w8'])-pd.to_numeric(df2['tot_spend_before'])

df2.sort_values(by='after_dif',ascending =False)

df2[df2['crn']=='1100000000003007767']

query ="""

select c.* from 
`gcp-wow-ent-im-wowx-cust-prod`.adp_wowx_dm_customer_view.loyalty_card_detail_v c
where c.PrimaryCustomerRegistrationNumber='1000000000000462379' 

"""
pd.read_gbq(query, project_id=project_id)




query ="""select * from akelly.campaignanalysis3 where 
crn ='1100000000003007767'
"""
pd.read_gbq(query, project_id=project_id)





query2 ="""drop table if exists akelly.campaignanalysis3_wtran"""
pd.read_gbq(query2, project_id=project_id)
query ="""drop table if exists akelly.campaignanalysis2"""
pd.read_gbq(query, project_id=project_id)
query ="""drop table if exists akelly.campaignanalysis3"""
pd.read_gbq(query, project_id=project_id)




query = """select CustomerRegistrationNumber,OfferAllocationStartTS,OfferAllocationEndTS, count(*)

from akelly.campaignanalysis2 where CustomerRegistrationNumber in  ('1100000000003174344','1100000000001590357')
group by 1,2,3
"""
pd.read_gbq(query, project_id=project_id)

query = """
create table akelly.campaignanalysis3 as 
with base as (
    -- select crn
    -- from loyalty_campaign_analytics.rtn4879_final
    -- -- from `gcp-wow-rwds-ai-cfv-dev.azhang.true_audience_4879`
    -- where pred_bin = 'hvcfv'

select cast(crn as string) as crn ,"control" as aud_type from `gcp-wow-rwds-ai-cfv-dev.asach.all_campaign_audience` 
where crn not in (select cast(crn as string) as crn 
                                    from wx-bq-poc.loyalty.et_targeted_customer_offer 
                                    where campaign_code= 'RTN-4879')
union DISTINCT
select cast(crn as string) as crn ,"test" as aud_type from `gcp-wow-rwds-ai-cfv-dev.asach.all_campaign_audience` 
where crn in (select cast(crn as string) as crn from wx-bq-poc.loyalty_campaign_analytics.rtn4879_final where pred_bin = 'hvcfv' )
and cast(crn as string) in (select cast(crn as string) as crn 
                                    from wx-bq-poc.loyalty.et_targeted_customer_offer 
                                    where campaign_code= 'RTN-4879' )

)
,sent as (
select distinct crn,1 as send_ind 
from wx-bq-poc.loyalty.et_targeted_customer_offer
where campaign_code= 'RTN-4879'
)

,activation as (
Select distinct(cecoa.crn),1 as act_ind
    --    ce.cmpgn_code as campaign_code, offer_id
FROM        wx-bq-poc.loyalty.campaign_exec ce
INNER JOIN 	wx-bq-poc.loyalty.campaign_exec_cust_offer_allocation cecoa 
on 			ce.cmpgn_exec_id = cecoa.cmpgn_exec_id
and 		cecoa.offer_id IN (-218616,-216217) 
WHERE      	cecoa.activation_ts IS NOT NULL 
AND 		ce.cmpgn_code = 'RTN-4879'	
AND 		ce.test_exec_ind != 'Y'
)

,temp_base_aud as (
        select bse.crn,aud_type,
       case when send_ind=1 then 1 else 0 end as send_ind
      ,case when act_ind=1 then 1 else 0 end as act_ind
from base as bse
left join sent 
on cast(bse.crn as string)= sent.crn
left join activation as act 
on cast(bse.crn as string)=act.crn
)
,super_spend as (
    select crn
           ,sum(case when sp.ref_dt="2021-08-15" then sp.supers_udp_tot_amt_w8 else 0 end )as before_instore_spend_w8
           ,sum(case when sp.ref_dt="2021-10-10" then sp.supers_udp_tot_amt_w8 else 0 end) as after_instore_spend_w8
    from`gcp-wow-rwds-ai-cfv-dev.feature_build.cfv_supers_txn_w8` sp
    -- where ref_dt in("2021-08-15","2021-09-19"  )
    where ref_dt in("2021-08-15","2021-10-10"  )    
    	

    group by crn )
select * from temp_base_aud
"""
base=pd.read_gbq(query, project_id=project_id)

base[base['crn'] =='3300000000005244789']


base[base['aud_type'] !='test']

base.groupby(['aud_type','send_ind','act_ind']).count()




query = """
create table akelly.campaignanalysis3_test as 
with base as (
    -- select crn
    -- from loyalty_campaign_analytics.rtn4879_final
    -- -- from `gcp-wow-rwds-ai-cfv-dev.azhang.true_audience_4879`
    -- where pred_bin = 'hvcfv'

select cast(crn as string) as crn ,"control" as aud_type from `gcp-wow-rwds-ai-cfv-dev.asach.all_campaign_audience` 
where crn not in (select cast(crn as string) as crn 
                                    from wx-bq-poc.loyalty.et_targeted_customer_offer 
                                    where campaign_code= 'RTN-4879')
union DISTINCT
select cast(crn as string) as crn ,"test" as aud_type from `gcp-wow-rwds-ai-cfv-dev.asach.all_campaign_audience` 
where crn in (select cast(crn as string) as crn from wx-bq-poc.loyalty_campaign_analytics.rtn4879_final where pred_bin = 'hvcfv' )
and cast(crn as string) in (select cast(crn as string) as crn 
                                    from wx-bq-poc.loyalty.et_targeted_customer_offer 
                                    where campaign_code= 'RTN-4879' )

)
,sent as (
select distinct crn,1 as send_ind 
from wx-bq-poc.loyalty.et_targeted_customer_offer
where campaign_code= 'RTN-4879'
)

,activation as (
Select distinct(cecoa.crn),1 as act_ind
    --    ce.cmpgn_code as campaign_code, offer_id
FROM        wx-bq-poc.loyalty.campaign_exec ce
INNER JOIN 	wx-bq-poc.loyalty.campaign_exec_cust_offer_allocation cecoa 
on 			ce.cmpgn_exec_id = cecoa.cmpgn_exec_id
and 		cecoa.offer_id IN (-218616,-216217) 
WHERE      	cecoa.activation_ts IS NOT NULL 
AND 		ce.cmpgn_code = 'RTN-4879'	
AND 		ce.test_exec_ind != 'Y'
)

,temp_base_aud as (
        select bse.crn,aud_type,
       case when send_ind=1 then 1 else 0 end as send_ind
      ,case when act_ind=1 then 1 else 0 end as act_ind
from base as bse
left join sent 
on cast(bse.crn as string)= sent.crn
left join activation as act 
on cast(bse.crn as string)=act.crn
)
,super_spend as (
    select crn
           ,sum(case when sp.ref_dt="2021-08-15" then sp.supers_udp_tot_amt_w8 else 0 end )as before_instore_spend_w8
           ,sum(case when sp.ref_dt="2021-10-10" then sp.supers_udp_tot_amt_w8 else 0 end) as after_instore_spend_w8
    from`gcp-wow-rwds-ai-cfv-dev.feature_build.cfv_supers_txn_w8` sp
    -- where ref_dt in("2021-08-15","2021-09-19"  )
    where ref_dt in("2021-08-15","2021-10-10"  )    
    	

    group by crn )
select a.* , b.before_instore_spend_w8,b.after_instore_spend_w8 from temp_base_aud as a join super_spend as b on a.crn=b.crn
"""
withsuperspend=pd.read_gbq(query, project_id=project_id)
withsuperspend.head()
withsuperspend.shape
base3[base3['crn']=='1100000000103499441']


query = """
create table akelly.campaignanalysis4 as 
select a.*,
b.PrimaryCustomerRegistrationNumber as LoyaltyCardNumber,min(LoyaltyCardRegistrationDate) as lylty_card_rgstr_date 
from akelly.campaignanalysis3 as a 
LEFT JOIN `gcp-wow-ent-im-wowx-cust-prod`.adp_wowx_dm_customer_view.loyalty_card_detail_v b
ON a.crn = b.CustomerRegistrationNumber
where b.LoyaltyCardStatusDescription not in ('Cancelled', 'Closed', 'Deregistered')
 """
pd.read_gbq(query, project_id=project_id)


query = """
SELECT a.crn,
case 
when a.act_ind	=1 then 'accepted'
when a.aud_type='test' and a.send_ind=1 then 'ignored'
else 'control' end as cust_type
,a.LoyaltyCardNumber,
CAST(EXTRACT(WEEK from c.TXNStartDate) as string) as week ,
CAST(EXTRACT(MONTH from c.TXNStartDate) as string) as MONTH ,
case when c.TXNStartDate>="2021-08-19" then 'after offer' else 'before offer' end as offer_status,
sum(c.ProductQty) as tot_pro,
sum(c.TotalAmountIncldTax) as tot_spend
from akelly.campaignanalysis4 as a join 
gcp-wow-ent-im-wowx-cust-prod.adp_wowx_dm_integrated_sales_view.article_sales_summary_v as c
on a.LoyaltyCardNumber=c.LoyaltyCardNumber
where c.TXNStartDate>="2021-06-20" and c.TXNStartDate<="2021-10-10" and
c.SalesOrg in(1030, 1005)
AND c.VoidFlag <> 'Y'
group by 1,2,3,4,5,6
 """

#case when c.TXNStartDate>=EXTRACT(DATE from b.OfferAllocationStartTS) and c.TXNStartDate<=EXTRACT(DATE from b.OfferAllocationEndTS )  then 'in allocation' else 'outside allocation' end as allocation_status,

query = """ 
select c.LoyaltyCardNumber,EXTRACT(YEAR from c.TXNStartDate) as year,
EXTRACT(MONTH from c.TXNStartDate) as month, 
count(distinct(c.LoyaltyCardNumber)) as customers,
sum(c.ProductQty) as tot_pro,
sum(c.TotalAmountIncldTax) as tot_spend,
avg( c.TotalAmountIncldTax) as avg_spend,
min( c.TotalAmountIncldTax) as min_spend,
max( c.TotalAmountIncldTax) as max_spend
from gcp-wow-ent-im-wowx-cust-prod.adp_wowx_dm_integrated_sales_view.article_sales_summary_v as c
where 
c.LoyaltyCardNumber='-8482206883256382192'
AND c.TXNStartDate>="2021-01-01" 
AND c.SalesOrg in(1030, 1005)
AND c.VoidFlag <> 'Y' 
AND c.TotalAmountIncldTax>0
AND c.POSNumber <> 100 
group by 1,2,3
"""
seasonal_spend=pd.read_gbq(query, project_id=project_id)



query = """
SELECT c.TXNStartDate,
case 
    when a.act_ind	=1 then 'accepted'
    when a.aud_type='test' and a.send_ind=1 then 'ignored'
    else 'control' end as cust_type,
case when c.TXNStartDate>=EXTRACT(DATE from b.OfferActivationStartTs) and c.TXNStartDate<=EXTRACT(DATE from b.OfferActivationEndTs )  then 'in offer' else 'outside offer' end as offer_status,
case when SalesChannelCode=1 then 'In store' else 'Online' end as shop_type,
count(distinct(a.crn)) as crns,
sum(c.ProductQty) as tot_pro,
sum(c.TotalAmountIncldTax) as tot_spend,
avg( c.TotalAmountIncldTax) as avg_spend,
min( c.TotalAmountIncldTax) as min_spend,
max( c.TotalAmountIncldTax) as max_spend
from akelly.campaignanalysis4 as a join 
gcp-wow-ent-im-wowx-cust-prod.adp_wowx_dm_integrated_sales_view.article_sales_summary_v as c
on a.LoyaltyCardNumber=c.LoyaltyCardNumber left join 
 akelly.campaignanalysis as b on a.crn=b.CustomerRegistrationNumber
where c.TXNStartDate>="2021-06-20" and c.TXNStartDate<="2021-10-10" and
c.SalesOrg in(1030, 1005)
AND c.VoidFlag <> 'Y' and c.TotalAmountIncldTax>0
AND c.POSNumber <> 100 
group by 1,2,3,4
 """
agg2=pd.read_gbq(query, project_id=project_id)

agg2.groupby('shop_type').count()

agg2.count()

query = """
SELECT b.CustomerRegistrationNumber,count(distinct(b.OfferAllocationStartTS)) 
from akelly.campaignanalysis4 as a  
left join 
akelly.campaignanalysis as b on a.crn=b.CustomerRegistrationNumber
where 
a.act_ind	=1 
group by 1
 """
no_activations=pd.read_gbq(query, project_id=project_id)

query = """
SELECT * from akelly.campaignanalysis limit 3
 """
pd.read_gbq(query, project_id=project_id)


agg1


base3=pd.read_gbq(query, project_id=project_id)

base3.shape
base3.head()

#accepted crns ('1100000000003174344','1100000000001590357')

path='campaign analysis /'
train_path= 'base3.feather'
#base3.to_feather(train_path)
#agg2.to_feather('agg2.feather')
base3 = pd.read_feather('base3.feather')
agg1 = pd.read_feather('agg1.feather')



base3[base3['cust_type']=='accepted']

base3.groupby(['offer_status'	,'MONTH'	,'week']).count()

forgraph=base3.groupby(['cust_type','offer_status','MONTH','week'])['tot_pro'	,'tot_spend'].agg(['count', 'sum','mean'])

forgraph.to_csv(path+'forg.csv')

base3.groupby(['cust_type','offer_status'])['tot_pro'	,'tot_spend'].agg(['count', 'sum','mean'])

import matplotlib.pyplot as plt
import seaborn as sns


sns.set_style('darkgrid') # darkgrid, white grid, dark, white and ticks
plt.rc('axes', titlesize=18)     # fontsize of the axes title
plt.rc('axes', labelsize=14)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=13)    # fontsize of the tick labels
plt.rc('ytick', labelsize=13)    # fontsize of the tick labels
plt.rc('legend', fontsize=13)    # legend fontsize
plt.rc('font', size=13)          # controls default text sizes
green = sns.color_palette("Greens")


Finalslkdajflk
summary_by_cust3['moreandless'] = summary_by_cust3['moreandless'].astype('category').cat.reorder_categories([ 'Lower weekly average $', 'Higher weekly avgerage $'])

state_office = summary_by_cust3.groupby(['cust_type', 'moreandless'])['crn'].count()
state = summary_by_cust3.groupby(['cust_type'])['crn'].count()
barplot4=state_office.div(state, level='cust_type') * 100

barplot4=pd.DataFrame(barplot4).reset_index()
barplot4=barplot4[barplot4['moreandless'] !='Same']


plt.figure(figsize=(12, 6), tight_layout=True)
ax = sns.barplot(x=barplot4['cust_type'], y=barplot4['crn'], hue=barplot4['moreandless'], palette=green )#,order=barplot['offer_status'] ) #, palette='pastel'
ax.set(title='The majority of customers that accepted the offer increased thier avg. weekly spend' ,xlabel='Experiment group', ylabel='% of group')
ax.legend(title='Customer type', title_fontsize='13', loc='upper right')
ax.set_ylim([0, 70])
plt.show()

state_office = summary_by_cust4.groupby(['multiboost', 'moreandless'])['crn'].count()
state = summary_by_cust4.groupby(['multiboost'])['crn'].count()
barplot4=state_office.div(state, level='multiboost') * 100

barplot4=pd.DataFrame(barplot4).reset_index()
barplot4=barplot4[barplot4['moreandless'] !='Same']


plt.figure(figsize=(12, 6), tight_layout=True)
ax = sns.barplot(x=barplot4['multiboost'], y=barplot4['crn'], hue=barplot4['moreandless'], palette=green )#,order=barplot['offer_status'] ) #, palette='pastel'
ax.set(title='Customers that accepted the offer were more likely to spend more' ,xlabel='Experiment group', ylabel='% of group')
ax.legend(title='Customer type', title_fontsize='13', loc='upper right')
plt.show()

base3['offer_status'] = base3['offer_status'].astype('category').cat.reorder_categories(['before offer', 'after offer'])
base3['cust_type'] = base3['cust_type'].astype('category').cat.reorder_categories(['control', 'ignored','accepted'])


barplot = base3.groupby(['cust_type', 'offer_status'], as_index=False).count()

plt.figure(figsize=(12, 6), tight_layout=True)
ax = sns.barplot(x=barplot['cust_type'], y=barplot['crn'], hue=barplot['offer_status'], palette=green )#,order=barplot['offer_status'] ) #, palette='pastel'
ax.set(title='Customers that accepted the offer had a lower rate of decline in customer numbers' ,xlabel='Experiment group', ylabel='Customer numbers')
ax.legend(title='Campaign period', title_fontsize='13', loc='upper right')
plt.show()

barplot2 = base3.groupby(['cust_type', 'offer_status'], as_index=False)['tot_spend'].sum()

plt.figure(figsize=(12, 6), tight_layout=True)
ax = sns.barplot(x=barplot2['cust_type'], y=barplot2['tot_spend'], hue=barplot2['offer_status'], palette=green )#,order=barplot['offer_status'] ) #, palette='pastel'
ax.set(title='Customers that accepted the offer had a lower rate of decline in total spend' ,xlabel='Experiment group', ylabel='Total spend')
ax.legend(title='Campaign period', title_fontsize='13', loc='upper right')
plt.show()



base3.groupby('crn')['offer_status'].count()
before=base3[base3['offer_status']=='before offer']['crn'].drop_duplicates()
after=base3[base3['offer_status']!='before offer']['crn'].drop_duplicates()

base_onlyinboth=base3[base3['crn'].isin(after)]

barplot3 = base_onlyinboth.groupby(['cust_type', 'offer_status'], as_index=False)['tot_spend'].sum()

plt.figure(figsize=(12, 6), tight_layout=True)
ax = sns.barplot(x=barplot3['cust_type'], y=barplot3['tot_spend'], hue=barplot3['offer_status'], palette=green )#,order=barplot['offer_status'] ) #, palette='pastel'
ax.set(title='Customers that accepted the offer had a lower rate of decline in total spend' ,xlabel='Experiment group', ylabel='Total spend')
ax.legend(title='Campaign period', title_fontsize='13', loc='upper right')
plt.show()

base3.head()

from scipy import stats

 
withsuperspend.head()

base3[base3['crn']=='1000000000000000063']
#-8482206883256382192	

validate=pd.merge(res,withsuperspend, on='crn')

cust_clean=pd.DataFrame(base3.groupby(['crn','offer_status'], as_index=False)['tot_spend'].sum())
res = cust_clean.pivot(index='crn', columns='offer_status', values='tot_spend')

res=pd.merge(res, base3[['crn','cust_type']], on='crn').drop_duplicates()
res2=res[res['cust_type'] !='control']


plt.figure(figsize=(12, 6), tight_layout=True)
ax = sns.scatterplot(x=res2['before offer'], y=res2['after offer'], hue=res2['cust_type'], palette=sns.color_palette('Greens', n_colors=3))#,order=barplot['offer_status'] ) #, palette='pastel', palette=green
ax.set(title='Customer 8 week spend, before and after offer' ,xlabel='before offer spend', ylabel='after offer spend')
ax.legend(title='Campaign period', title_fontsize='13', loc='upper right')
plt.show()


def moreless(x,y):
    if x==y:
        return 'Same'
    if x<y:
        return 'More'
    else:
        return 'Less'
    
res['moreandless']= res.apply(lambda x:moreless(x['before offer'], x['after offer']) ,axis=1)


moreandless=pd.DataFrame(res.groupby(['cust_type','moreandless'], as_index=False).count())

state_office = res.groupby(['cust_type', 'moreandless']).count()
state = res.groupby(['cust_type']).count()
barplot4=state_office.div(state, level='cust_type') * 100

barplot4 = barplot4.drop(['moreandless','crn'],axis=1).reset_index().melt(id_vars=['cust_type'	,'moreandless']).drop_duplicates()
barplot4=barplot4[barplot4['moreandless'] !='Same']


plt.figure(figsize=(12, 6), tight_layout=True)
ax = sns.barplot(x=barplot4['moreandless'], y=barplot4['value'], hue=barplot4['cust_type'], palette=green )#,order=barplot['offer_status'] ) #, palette='pastel'
ax.set(title='Customers that accepted the offer were more likely to spend more' ,xlabel='Experiment group', ylabel='% of group')
ax.legend(title='Customer type', title_fontsize='13', loc='upper right')
plt.show()






agg2['groupbyit']=agg2['cust_type']+' '+ agg2['shop_type']

plt.figure(figsize=(12, 6), tight_layout=True)
ax = sns.lineplot(x=agg2['TXNStartDate'], y=agg2['avg_spend'], hue=agg2['cust_type'], palette=sns.color_palette('Greens', n_colors=3) )#,order=barplot['offer_status'] ) #, palette='pastel', palette=green
ax.set(title='Customer total spend, 8 weeks before and after offer' ,xlabel='Experiment group', ylabel='Count')
ax.legend(title='Campaign period', title_fontsize='13', loc='upper right')
plt.axvline(x='2021-06-23', ymin=0.10, ymax=0.70, color='b', ls='--', lw=1.5, label='axvline - % of full height')
plt.show()

 

(mdates.date2num(datetime.datetime(2021,08,15)), 50)
plt.axvline(x='2021-06-23')

accepted = agg1[(agg1['cust_type'] == 'accepted' & agg1['cust_type'] == 'accepted' )] #['TXNStartDate','cust_type','allocation_status','offer_status']
accepted['groupbyit']=accepted['offer_status']+' '+ accepted['allocation_status']

accepted2=accepted.groupby(['TXNStartDate','groupbyit'])['tot_spend'].agg('mean')

plt.figure(figsize=(12, 6), tight_layout=True)
ax = sns.lineplot(x=accepted['TXNStartDate'], y=accepted['avg_spend'], hue=accepted['groupbyit'], palette='pastel' )#,order=barplot['offer_status'] ) #, palette='pastel', palette=green
ax.set(title='Customer type, before and after offer' ,xlabel='Experiment group', ylabel='Count')
ax.legend(title='Campaign period', title_fontsize='13', loc='upper right')
plt.show()


import datetime
mydate="2021-10-10" 
mydate2="2021-08-15"
aDate ,aDate2= datetime.datetime.strptime(mydate,"%Y-%m-%d"), datetime.datetime.strptime(mydate2,"%Y-%m-%d")
_8Weeks = datetime.timedelta(weeks = 8)

print (aDate + _8Weeks)
print (aDate2 - _8Weeks)

query = """
SELECT c.LoyaltyCardNumber,
sum(case when c.TXNStartDate>="2021-06-20" and c.TXNStartDate<="2021-08-15" then ProductQty else 0 end ) as tot_products_before_offer_2021,
sum(case when c.TXNStartDate>="2020-06-20" and c.TXNStartDate<="2020-08-15" then ProductQty else 0 end ) as tot_products_before_offer_2020,
sum(case when c.TXNStartDate>="2021-06-20" and c.TXNStartDate<="2021-08-15" then TotalAmountIncldTax else 0 end ) as tot_spend_before_offer_2021,
sum(case when c.TXNStartDate>="2020-06-20" and c.TXNStartDate<="2020-08-15" then TotalAmountIncldTax else 0 end ) as tot_spend_before_offer_2020,
sum(case when  c.TXNStartDate>"2021-08-15" then ProductQty else 0 end ) as tot_products_after_offer_2021,
sum(case when c.TXNStartDate>"2020-08-15" and c.TXNStartDate<"2021-01-10" then ProductQty else 0 end ) as tot_products_after_offer_2020,
sum(case when c.TXNStartDate>"2021-08-15" then TotalAmountIncldTax else 0 end ) as tot_spend_after_offer_2021,
sum(case when c.TXNStartDate>"2020-08-15" and c.TXNStartDate<"2021-01-10" then TotalAmountIncldTax else 0 end ) as tot_spend_after_offer_2020,
FROM 
gcp-wow-ent-im-wowx-cust-prod.adp_wowx_dm_integrated_sales_view.article_sales_summary_v as c
WHERE c.LoyaltyCardNumber='-483621050623971481' AND
((c.TXNStartDate>="2020-06-20" AND
c.TXNStartDate<="2020-10-10") OR
(c.TXNStartDate>="2021-06-20" AND
c.TXNStartDate<="2021-10-10"))
group by 1
"""

query = """ drop table if exists akelly.campaignanalysis4"""
pd.read_gbq(query, project_id=project_id)

 

query = """
create table akelly.campaignanalysis4 AS 
SELECT 
c.LoyaltyCardNumber
,a.crn	
,a.aud_type	
,a.send_ind	
,a.act_ind
,a.ActivationType
,a.UserActivationInd
,a.EventmetadataEventtype	
,a.EventmetadataEventname,
sum(case when c.TXNStartDate>="2021-06-20" and c.TXNStartDate<="2021-08-15" then ProductQty else 0 end ) as tot_products_before_offer_2021,
sum(case when c.TXNStartDate>="2020-06-20" and c.TXNStartDate<="2020-08-15" then ProductQty else 0 end ) as tot_products_before_offer_2020,
sum(case when c.TXNStartDate>="2021-06-20" and c.TXNStartDate<="2021-08-15" then TotalAmountIncldTax else 0 end ) as tot_spend_before_offer_2021,
sum(case when c.TXNStartDate>="2020-06-20" and c.TXNStartDate<="2020-08-15" then TotalAmountIncldTax else 0 end ) as tot_spend_before_offer_2020,
sum(case when  c.TXNStartDate>"2021-08-15" then ProductQty else 0 end ) as tot_products_after_offer_2021,
sum(case when c.TXNStartDate>"2020-08-15" and c.TXNStartDate<"2021-01-10" then ProductQty else 0 end ) as tot_products_after_offer_2020,
sum(case when c.TXNStartDate>"2021-08-15" then TotalAmountIncldTax else 0 end ) as tot_spend_after_offer_2021,
sum(case when c.TXNStartDate>"2020-08-15" and c.TXNStartDate<"2021-01-10" then TotalAmountIncldTax else 0 end ) as tot_spend_after_offer_2020,
FROM 
gcp-wow-ent-im-wowx-cust-prod.adp_wowx_dm_integrated_sales_view.article_sales_summary_v as c
 join akelly.campaignanalysis3 as a
ON A.LoyaltyCardNumber=c.LoyaltyCardNumber
WHERE 

((c.TXNStartDate>="2020-06-20" AND
c.TXNStartDate<="2020-10-10") OR
(c.TXNStartDate>="2021-06-20" AND
c.TXNStartDate<="2021-10-10")) AND 
c.SalesOrg in(1030, 1005) AND c.VoidFlag <> 'Y' AND c.POSNumber <> 100
group by 1,2,3,4,5,6,7,8,9
"""
base3=pd.read_gbq(query, project_id=project_id)

base3=base3[base3['crn'] is not None]
base3=base3[base3['crn'] !='None']


base3.groupby(['aud_type', 'UserActivationInd']).count()
base.groupby(['aud_type', 'UserActivationInd']).count()


base3.shape
base3.head()
'1100000000072360650'
'-6824689176242724049'



path='train/'
train_path= 'train_2020_2021.feather'
#df.to_feather(train_path)
df = pd.read_feather(path+ train_path)