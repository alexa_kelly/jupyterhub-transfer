


import os
from google.cloud.bigquery.client import Client

os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = '/home/jovyan/secrets/service-ac-key.json'
bq_client = Client()
project_id = 'gcp-wow-rwds-ai-cfv-prod'


from feature_store import FeatureStore
from feature_store.model import Feature


def main():
    FEATURE_STORE_ENV = 'test'

    fs = FeatureStore(env=FEATURE_STORE_ENV)

    supers_instore_response = fs.management.define_feature_set(
        feature_group_name='cfv',
        feature_set_name='supers_instore_cfv_prod',
        key_columns=['crn'],
        description="Supers Instore CFV Segments",
        meta={"project": "cfv"},
        owners = ['akelly5@woolworths.com.au'],
        features=[
            Feature('cfv_ref_dt', 'Original CFV ref_dt',),
            Feature('cfv_instore_score_spend', 'Final Supers Instore spend score',),
            Feature('instore_cfv', 'Final Supers Instore CFV Segment', ),
        ],
    )   
   
   
     _ =fs.batch.import_features_from_bq(
         source_table='gcp-wow-rwds-ai-cfv-prod.score.fs_cfv_supers_instore_past_import',
         feature_group='cfv',
         feature_set='supers_instore_cfv_prod',
         grant_data_viewer_role_to_feature_store=False
     )
   
   
    supers_online_response = fs.management.define_feature_set(
        feature_group_name='cfv',
        feature_set_name='supers_online_cfv_prod',
        key_columns=['crn'],
        description="Supers Online CFV Segments",
        meta={"project": "cfv"},
        owners = ['akelly5@woolworths.com.au'],
        features=[
            Feature('cfv_ref_dt', 'Original CFV ref_dt',),
            Feature('cfv_ecom_score_spend', 'Final Supers online spend score',),
            Feature('ecom_cfv', 'Final Supers online CFV Segment', ),
        ],
    )

    
       _ =fs.batch.import_features_from_bq(
            source_table='gcp-wow-rwds-ai-cfv-prod.score.fs_cfv_supers_online_past_import',
            feature_group='cfv',
            feature_set='supers_online_cfv_prod',
            grant_data_viewer_role_to_feature_store=False
        )
     
     
     supers_online3m_response = fs.management.define_feature_set(
        feature_group_name='cfv',
        feature_set_name='supers_online3m_cfv_prod',
        key_columns=['crn'],
        description="Supers Online 3m CFV Segments",
        meta={"project": "cfv"},
        owners = ['akelly5@woolworths.com.au'],
        features=[
            Feature('cfv_ref_dt', 'Original CFV ref_dt',),
            Feature('cfv_ecom3m_score_spend', 'Final Supers online 3m spend score',),
            Feature('ecom3m_cfv', 'Final supers online 3m CFV Segment', ),
        ],
    )
     
       _ =fs.batch.import_features_from_bq(
            source_table='gcp-wow-rwds-ai-cfv-prod.score.fs_cfv_supers_online3m_past_import',
            feature_group='cfv',
            feature_set='supers_online3m_cfv_prod',
            grant_data_viewer_role_to_feature_store=False
        )

      
    bigw_instore_response = fs.management.define_feature_set(
        feature_group_name='cfv',
        feature_set_name='bigw_instore_cfv_prod',
        key_columns=['crn'],
        description="BigW Instore CFV Segments",
        meta={"project": "cfv"},
        owners = ['akelly5@woolworths.com.au'],
        features=[
            Feature('cfv_ref_dt', 'Original CFV ref_dt',),
            Feature('cfv_bigw_instore_score_spend', 'Final bigw Instore spend score',),
            Feature('bigw_instore_cfv', 'Final Supers bigw CFV Segment', ),
        ],
    )
       _ =fs.batch.import_features_from_bq(
            source_table='gcp-wow-rwds-ai-cfv-prod.score.fs_cfv_bigw_instore_past_import',
            feature_group='cfv',
            feature_set='bigw_instore_cfv_prod',
            grant_data_viewer_role_to_feature_store=False
        )
     
     
    bigw_online_response = fs.management.define_feature_set(
        feature_group_name='cfv',
        feature_set_name='bigw_online_cfv_prod',
        key_columns=['crn'],
        description="BigW Online CFV Segments",
        meta={"project": "cfv"},
        owners = ['akelly5@woolworths.com.au'],
        features=[
            Feature('cfv_ref_dt', 'Original CFV ref_dt',),
            Feature('cfv_bigw_online_score_spend', 'Final bigw online spend score',),
            Feature('bigw_online_cfv', 'Final bigw online CFV Segment', ),
        ],
    )
    
       _ =fs.batch.import_features_from_bq(
            source_table='gcp-wow-rwds-ai-cfv-prod.score.fs_cfv_bigw_online_past_import',
            feature_group='cfv',
            feature_set='bigw_online_cfv_prod',
            grant_data_viewer_role_to_feature_store=False
        )
     
    bws_instore_response = fs.management.define_feature_set(
        feature_group_name='cfv',
        feature_set_name='bws_instore_cfv_prod',
        key_columns=['crn'],
        description="BWS Instore CFV Segments",
        meta={"project": "cfv"},
        owners = ['akelly5@woolworths.com.au'],
        features=[
            Feature('cfv_ref_dt', 'Original CFV ref_dt',),
            Feature('cfv_bws_instore_score_spend', 'Final bws Instore spend score',),
            Feature('bws_instore_cfv', 'Final bws Instore CFV Segment', ),
        ],
    )
    
    
       _ =fs.batch.import_features_from_bq(
            source_table='gcp-wow-rwds-ai-cfv-prod.score.fs_cfv_bws_instore_past_import',
            feature_group='cfv',
            feature_set='bws_instore_cfv_prod',
            grant_data_viewer_role_to_feature_store=False
        )

    bws_online_response = fs.management.define_feature_set(
        feature_group_name='cfv',
        feature_set_name='bws_online_cfv_prod',
        key_columns=['crn'],
        description="BWS Online CFV Segments",
        meta={"project": "cfv"},
        owners = ['akelly5@woolworths.com.au'],
        features=[
            Feature('cfv_ref_dt', 'Original CFV ref_dt',),
            Feature('cfv_bws_online_score_spend', 'Final bws online spend score',),
            Feature('bws_online_cfv', 'Final bws online CFV Segment', ),
        ],
    )
    
    _ =fs.batch.import_features_from_bq(
         source_table='gcp-wow-rwds-ai-cfv-prod.score.fs_cfv_bws_online_past_import',
         feature_group='cfv',
         feature_set='bws_online_cfv_prod',
         grant_data_viewer_role_to_feature_store=False
     )
    
    
     _ =fs.delete_feature_set( 'supers_instore_cfv')
     _ =fs.delete_feature_set( 'supers_online_cfv')
     _ =fs.delete_feature_set( 'bigw_instore_cfv')
     _ =fs.delete_feature_set( 'bigw_online_cfv')
     _ =fs.delete_feature_set( 'bws_instore_cfv')
     _ =fs.delete_feature_set( 'bws_online_cfv')
