-- with base as (
-- select  bse.crn,
-- from loyalty_campaign_analytics.rtn4879_final as bse
-- where pred_bin = 'hvcfv' 
-- and crn not in (select crn from `gcp-wow-rwds-ai-cfv-dev.azhang.true_audience_4879` )),

with base as (
select cast(crn as string) as crn ,"control" as aud_type from `gcp-wow-rwds-ai-cfv-dev.asach.all_campaign_audience` 
where crn not in (select cast(crn as string) as crn 
                                    from loyalty.et_targeted_customer_offer 
                                    where campaign_code= 'RTN-4879')
union DISTINCT
select cast(crn as string) as crn ,"test" as aud_type from `gcp-wow-rwds-ai-cfv-dev.asach.all_campaign_audience` 
where crn in (select cast(crn as string) as crn from loyalty_campaign_analytics.rtn4879_final where pred_bin = 'hvcfv' )
and cast(crn as string) in (select cast(crn as string) as crn 
                                    from loyalty.et_targeted_customer_offer 
                                    where campaign_code= 'RTN-4879' )

)

,sent as (
select distinct crn,1 as send_ind 
from loyalty.et_targeted_customer_offer
where campaign_code= 'RTN-4879'
)

,activation as (
Select distinct(cecoa.crn),1 as act_ind
    --    ce.cmpgn_code as campaign_code, offer_id
FROM        loyalty.campaign_exec ce
INNER JOIN 	loyalty.campaign_exec_cust_offer_allocation cecoa 
on 			ce.cmpgn_exec_id = cecoa.cmpgn_exec_id
and 		cecoa.offer_id IN (-218616,-216217) 
-- /RTN-4879 campaign Offer Id's'/
--INNER JOIN  Audience wc
--on wc.crn = cecoa.crn 
WHERE      	cecoa.activation_ts IS NOT NULL 
AND 		ce.cmpgn_code = 'RTN-4879'	
AND 		ce.test_exec_ind != 'Y'
)

,temp_base_aud as (
        select bse.crn,aud_type,
       case when send_ind=1 then 1 else 0 end as send_ind
      ,case when act_ind=1 then 1 else 0 end as act_ind
from base as bse
left join sent 
on cast(bse.crn as string)= sent.crn
left join activation as act 
on cast(bse.crn as string)=act.crn
)

,super_spend as (
    select crn
           ,sum(case when sp.ref_dt="2021-08-15" then sp.supers_udp_tot_amt_w8 else 0 end )as before_instore_spend_w8
           ,sum(case when sp.ref_dt="2021-10-10" then sp.supers_udp_tot_amt_w8 else 0 end) as after_instore_spend_w8
    from`gcp-wow-rwds-ai-cfv-dev.feature_build.cfv_supers_txn_w8` sp
    -- where ref_dt in("2021-08-15","2021-09-19"  )
    where ref_dt in("2021-08-15","2021-10-10"  )    
    	

    group by crn )
,ecom_spend as (select crn,ref_dt ,onln_spend_catg_1516_w8	+
onln_spend_catg_1097_w8	+
onln_spend_catg_0571_w8	+
onln_spend_catg_1507_w8	+
onln_spend_catg_1099_w8	+
onln_spend_catg_4067_w8	+
onln_spend_catg_3069_w8	+
onln_spend_catg_5570_w8	+
onln_spend_catg_0574_w8	+
onln_spend_catg_0586_w8	+
onln_spend_catg_2855_w8	+
onln_spend_catg_0579_w8	+
onln_spend_catg_2051_w8	+
onln_spend_catg_1089_w8	+
onln_spend_catg_0544_w8	+
onln_spend_catg_0541_w8	+
onln_spend_catg_0576_w8	+
onln_spend_catg_2075_w8	+
onln_spend_catg_0038_w8	+
onln_spend_catg_1505_w8	+
onln_spend_catg_2054_w8	+
onln_spend_catg_1014_w8	+
onln_spend_catg_1066_w8	+
onln_spend_catg_0584_w8	+
onln_spend_catg_0552_w8	+
onln_spend_catg_0540_w8	+
onln_spend_catg_1532_w8	+
onln_spend_catg_1521_w8	+
onln_spend_catg_2830_w8	+
onln_spend_catg_1523_w8	+
onln_spend_catg_2811_w8	+
onln_spend_catg_1092_w8	+
onln_spend_catg_2568_w8	+
onln_spend_catg_0580_w8	+
onln_spend_catg_0553_w8	+
onln_spend_catg_1093_w8	+
onln_spend_catg_1091_w8	+
onln_spend_catg_2078_w8	+
onln_spend_catg_1028_w8	+
onln_spend_catg_0585_w8	+
onln_spend_catg_1510_w8	+
onln_spend_catg_0557_w8	+
onln_spend_catg_3063_w8	+
onln_spend_catg_1525_w8	+
onln_spend_catg_0581_w8	+
onln_spend_catg_1503_w8	+
onln_spend_catg_1535_w8	+
onln_spend_catg_0573_w8	+
onln_spend_catg_0558_w8	+
onln_spend_catg_0583_w8	+
onln_spend_catg_1094_w8	+
onln_spend_catg_0549_w8	+
onln_spend_catg_1090_w8	+
onln_spend_catg_1095_w8	+
onln_spend_catg_0546_w8	+
onln_spend_catg_3036_w8	+
onln_spend_catg_1512_w8	+
onln_spend_catg_0543_w8	+
onln_spend_catg_1017_w8	+
onln_spend_catg_0582_w8	+
onln_spend_catg_4556_w8	+
onln_spend_catg_1504_w8	+
onln_spend_catg_2787_w8	+
onln_spend_catg_0559_w8	+
onln_spend_catg_4760_w8	+
onln_spend_catg_0548_w8	+
onln_spend_catg_1062_w8	+
onln_spend_catg_0564_w8	+
onln_spend_catg_1065_w8	+
onln_spend_catg_0542_w8	+
onln_spend_catg_0572_w8	+
onln_spend_catg_1520_w8	+
onln_spend_catg_1502_w8	+
onln_spend_catg_2019_w8	+
onln_spend_catg_1526_w8	+
onln_spend_catg_1537_w8	+
onln_spend_catg_2050_w8	+
onln_spend_catg_1098_w8	+
onln_spend_catg_1096_w8 as ecom_udp_tot_amt_w8
from `gcp-wow-rwds-ai-cfv-dev.feature_build.cfv_ecom_onln_spend_catg_w8`
-- where  ref_dt in("2021-08-15","2021-09-19"  ))
where  ref_dt in("2021-08-15","2021-10-10"  ))


, new_ecom as (
    select crn
        ,sum(case when ep.ref_dt="2021-08-15" then ep.ecom_udp_tot_amt_w8 else 0 end )as before_ecom_spend_w8
        ,sum(case when ep.ref_dt="2021-10-10" then ep.ecom_udp_tot_amt_w8 else 0 end) as after_ecom_spend_w8
    from ecom_spend ep 
    group by crn
)
, Transition2 as (
       select cast(tbs.crn as string) as crn
       ,tbs.aud_type
       ,tbs.act_ind
    -- ,count(tbs.crn) as ncount
       ,case when before_instore_spend_w8 is null then 0 else before_instore_spend_w8 end as before_instore_spend_w8


       ,case when after_instore_spend_w8 is null then 0 else after_instore_spend_w8 end as after_instore_spend_w8

              ,case when before_ecom_spend_w8 is null then 0 else before_ecom_spend_w8 end as before_ecom_spend_w8
       

       ,case when after_ecom_spend_w8 is null then 0 else after_ecom_spend_w8 end as after_ecom_spend_w8

       

from temp_base_aud tbs 
left join super_spend sp
on cast(tbs.crn as string)=sp.crn 
left join new_ecom ep
on cast(tbs.crn as string)=ep.crn 
)

select 
aud_type
-- act_ind
,count(crn) as crn_count
,avg(before_instore_spend_w8) as before_instore_spend_w8
,avg(after_instore_spend_w8) as after_instore_spend_w8
,avg(before_ecom_spend_w8) as before_ecom_spend_w8
,avg(after_ecom_spend_w8) as after_ecom_spend_w8
,avg(before_instore_spend_w8+before_ecom_spend_w8) as before_combined_spend_w8
,avg(after_instore_spend_w8+after_ecom_spend_w8) as after_combined_spend_w8
from Transition2 
-- where aud_type ='test'
group by aud_type
    -- ,send_ind
    -- act_ind