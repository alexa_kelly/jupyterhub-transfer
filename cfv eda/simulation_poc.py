import numpy as np
import pandas as pd
import random
import matplotlib.pyplot as plt
import seaborn as sns
import os
import sys
from google.cloud import bigquery
from google.cloud import storage
import pickle
import matplotlib.ticker as mtick






os.environ['GOOGLE_APPLICATION_CREDENTIALS']=f"/home/jovyan/.config/gcloud/legacy_credentials/{os.getenv('JUPYTERHUB_USER')}/adc.json"
np.set_printoptions(threshold=sys.maxsize)
pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 500)
#from pandas_profiling import ProfileReport
pd.options.display.float_format = '{:.4f}'.format

#spreadsheet location https://docs.google.com/spreadsheets/d/17ZsLzPjtbOIQmqhaWn62roa-kcuG41FgnGBYfC_qJB4/edit#gid=0
project_id = 'gcp-wow-rwds-ai-cfv-dev'

##############################################################################################################################
##########################################TESTING CONTROL SAMPLEE###############################################################
##############################################################################################################################
q="""select count(*) from akelly.campaign_control_before where campaign_code is not null ; """
pd.read_gbq(q, project_id=project_id)


q="""select count(*) from akelly.campaign_control_before  ; """
pd.read_gbq(q, project_id=project_id)



q="""select * from akelly.campaign_control_before limit 20 ; """
control_sample=pd.read_gbq(q, project_id=project_id)

q="""select * from akelly.campaign_control_before_withcamp limit 20 ; """
control_sample=pd.read_gbq(q, project_id=project_id)

q="""select count(*) from akelly.campaign_control_before_withcamp ; """
pd.read_gbq(q, project_id=project_id)


q="""select crn,ref_dt,count(*) from akelly.campaign_control_before_withcamp group by 1,2 order by 3 desc limit 10; """
pd.read_gbq(q, project_id=project_id)



q="""select * from akelly.campaign_control_before_withcamp where crn='3300000000004297500' """
pd.read_gbq(q, project_id=project_id)


q="""select count(*) from akelly.campaign_control_after where campaign_code is not null ; """
pd.read_gbq(q, project_id=project_id)

q="""select * from akelly.campaign_control_after limit 20 ; """
control_sample=pd.read_gbq(q, project_id=project_id)

q="""select crn,ref_dt,count(*) from akelly.campaign_control_after group by 1,2 order by 3 desc limit 10; """
pd.read_gbq(q, project_id=project_id)

q="""select * from akelly.campaign_control_after where crn='1000000000002549359' """
pd.read_gbq(q, project_id=project_id)


q="""select count(*) from akelly.campaign_control_before ; """
pd.read_gbq(q, project_id=project_id)

q="""select * from akelly.campaign_test2 where crn in('1100000000124507669') and campaign_code='CNA-0005B' ; """
control_sample_in_target=pd.read_gbq(q, project_id=project_id)

q="""select count(*) from akelly.campaign_ref_dt  where campaign_code is null;"""
pd.read_gbq(q, project_id=project_id)



q="""select pred_bin_after,cav_conversion, count(*) from akelly.campaign_newbin group by 1,2"""
pd.read_gbq(q, project_id=project_id)
 


q="""select * from akelly.campaign_test5 limit 4"""
pd.read_gbq(q, project_id=project_id)

q="""select count(*) , count(distinct(crn)) from akelly.campaign_test5 where offer_desc='10x points on the entire Macro range'"""
pd.read_gbq(q, project_id=project_id)
##############################################################################################################################
##########################################TESTING CONTROL SAMPLEE###############################################################
##############################################################################################################################

 
#d.head()
#chuurn.to_feather('supers_instore_churn.feather') campaigns['customers'].min()
d=pd.read_feather('supers_instore_churn.feather')
other=d.iloc[0,1]
same_CAV=d.iloc[1,1]
churn_to_lower_CAV=d.iloc[2,1]
churned=d.iloc[3,1]

campaigns_ecom=pd.read_feather('campaigns_ecom.feather')
campaigns=pd.read_feather('campaigns.feather')
campaigns_control=pd.read_feather('campaigns_control.feather')
campaigns.sort_values(by='campaigns',ascending=False)

campaigns.sort_values(by='customers',ascending=True)
#campaigns_control=campaigns_control.fillna(0)
#campaigns =pd.read_gbq('select * from akelly.campaign_instore_agg order by 1', project_id=project_id)
#campaigns_ecom =pd.read_gbq('select * from akelly.campaign_ecom_aggregate order by 1', project_id=project_id)
#campaigns_control =pd.read_gbq('select * from akelly.campaign_control_agg order by 1', project_id=project_id)
#campaigns.to_csv('campaigns.csv')
#campaigns_control.to_feather('campaigns_control.feather')
#campaigns_ecom.to_feather('campaigns_ecom.feather')
#campaigns.to_feather('campaigns.feather')
#campaigns[campaigns.offer_desc=='Spend and Get Points']

#campaigns.groupby('offer_desc').count()           
                     

campaigns[campaigns.campaigns>=3]

# From the expected conversion rate and stdev of it, get the
# realized conversion rate

 
 
# Gather marketing campaign inputs


#calculate conversion rate
#calculate conversion rate

# Budget


 
def get_conversion_rate(expected, stdev):
    conversion_rate = max(expected + np.random.normal()*(stdev), #stdev, 
                          0.01)
    if conversion_rate>=1:
        return 0.99
    else:
        return conversion_rate

# Function for calculating the results of a marketing campaign
def run_campaign(conversion_rate, targeted_customers): #=None,spend=None, activated_cost=None):
    return np.random.binomial(targeted_customers, conversion_rate)


    
# Function that models the progression of a cohort over time
def simulate_cohort(cohort_size, churn_rate, avg_yr_pred, yrs=1,before=False):
    customers_left = []
    spending = []
    profit = []
    for i in range(yrs):
        if before==True:
            spending.append(cohort_size*avg_yr_pred)
            return cohort_size, spending
        else:
      
            for customer in range(cohort_size):
                # Assume cancels happen at the start of the year 
                # (for simplicity)
                
                churn_random_num = np.random.random()
                # Generate a random number between 0 and 1, if less 
                # than churn_rate then customer has churned and we 
                # subtract 1 from cohort_size
                if churn_random_num <= churn_rate:
                    cohort_size += -1
                # Calculate and record cohort's data
            customers_left.append(cohort_size)
            spending.append(cohort_size*avg_yr_pred)

            return cohort_size, spending


def simiulation_step1(campaigns,activated_cost,offer,runs=1000,control=False):
    test=campaigns_ecom[campaigns_ecom.offer_desc==offer]

    chosen_campaign=campaigns[campaigns.offer_desc==offer]
    targeted_customers=int(chosen_campaign.customers/chosen_campaign.campaigns)
    conversion_rate_activated_expected =chosen_campaign.cav_conversion_rate.iloc[0]
    conversion_rate_activated_std=chosen_campaign.activated_std.iloc[0]
    conversion_rate_uplifted_expected =chosen_campaign.cav_conversion_activated_rate_from_activated.iloc[0]
    conversion_rate_uplifted_std=chosen_campaign.activated_cav_conversion_std.iloc[0]



    simulate=pd.DataFrame(columns=['CAC','cohort_size_activated','cohort_size_uplift','Pcent_uplift','Pcent_uplift_oftotal','Pcent_activated','customers_not_activated_before','customers_not_activated_after','customers_activated_before','customers_activated_after','customers_uplifted_before','customers_uplifted_after','spending_not_activated_before', 'spending_not_activated_after','spending_before_activated', 'spending_after_activated', 'spending_before_uplifted', 'spending_after_uplifted','cohort_uplifted_value','spread' ,'activated_conversion_rate','activated_uplift_conversion_rate'])


    for i in range(runs):
        if i==1:
            print('chosen_campaign: ',offer )
            print('targeted_customers: ',targeted_customers )
            print('conversion_rate_activated_expected: ',conversion_rate_activated_expected )
            print('conversion_rate_activated_std: ',conversion_rate_activated_std )
            print('conversion_rate_uplifted_expected: ',conversion_rate_uplifted_expected )
            print('conversion_rate_uplifted_std: ',conversion_rate_uplifted_std )
        # Run marketing campaign sim
       	
       
        activated_conversion_rate = get_conversion_rate(conversion_rate_activated_expected,conversion_rate_activated_std*0.01)
        activated_uplift_conversion_rate = get_conversion_rate(conversion_rate_uplifted_expected, conversion_rate_uplifted_std*0.01)
        cohort_size_activated = max(run_campaign( activated_conversion_rate,targeted_customers=targeted_customers),1)
        if cohort_size_activated>=targeted_customers:
             cohort_size_activated=targeted_customers-1
        else:
            cohort_size_activated=cohort_size_activated
        cohort_size_uplift = max(run_campaign( activated_uplift_conversion_rate,targeted_customers=cohort_size_activated),1)
        #CAC = spend/cohort_size
        #cost of campaign
        CAC = int(activated_cost*cohort_size_activated)
        Pcent_uplift_oftotal=cohort_size_uplift/targeted_customers
        Pcent_uplift=cohort_size_uplift/cohort_size_activated
        Pcent_activated=cohort_size_activated/targeted_customers
        cohort_size_not_activated=targeted_customers-cohort_size_activated
        # Run the function
        
        
        customers_not_activated_before, spending_not_activated_before =simulate_cohort(cohort_size_not_activated, chosen_campaign.campaign_churn.iloc[0],chosen_campaign.avg_spend_before_not_activated.iloc[0], yrs=1,before=True)

        customers_not_activated_after, spending_not_activated_after =simulate_cohort(cohort_size_not_activated, chosen_campaign.campaign_churn.iloc[0],chosen_campaign.avg_spend_after_not_activated.iloc[0], yrs=1)
        
        
        customers_activated_before, spending_before_activated =simulate_cohort(cohort_size_activated, chosen_campaign.campaign_churn.iloc[0],chosen_campaign.avg_spend_before_activated.iloc[0], yrs=1,before=True)

        customers_activated_after, spending_after_activated =simulate_cohort(cohort_size_activated, chosen_campaign.campaign_churn.iloc[0],chosen_campaign.avg_spend_after_activated.iloc[0], yrs=1)


        customers_uplifted_before, spending_before_uplifted =simulate_cohort(cohort_size_uplift, chosen_campaign.campaign_churn.iloc[0],chosen_campaign.avg_spend_before_uplifted.iloc[0], yrs=1,before=True)

        customers_uplifted_after, spending_after_uplifted =simulate_cohort(cohort_size_uplift, chosen_campaign.campaign_churn.iloc[0],chosen_campaign.avg_spend_after_uplifted.iloc[0], yrs=1)

        cohort_uplifted_value = sum(spending_after_uplifted)-sum(spending_before_uplifted)
        spread=int(cohort_uplifted_value - CAC)
        temp=pd.DataFrame([[CAC,cohort_size_activated,cohort_size_uplift,Pcent_uplift,Pcent_uplift_oftotal,\
                                   float(Pcent_activated),customers_not_activated_before,customers_not_activated_after,customers_activated_before,customers_activated_after,\
                                       customers_uplifted_before,customers_uplifted_after,spending_not_activated_before[0],\
                                       spending_not_activated_after[0],spending_before_activated[0],\
                                       spending_after_activated[0], spending_before_uplifted[0],spending_after_uplifted[0],\
                                       cohort_uplifted_value,spread,activated_conversion_rate,activated_uplift_conversion_rate]],columns=['CAC','cohort_size_activated','cohort_size_uplift','Pcent_uplift','Pcent_uplift_oftotal','Pcent_activated','customers_not_activated_before','customers_not_activated_after','customers_activated_before','customers_activated_after','customers_uplifted_before','customers_uplifted_after','spending_not_activated_before', 'spending_not_activated_after','spending_before_activated', 'spending_after_activated', 'spending_before_uplifted', 'spending_after_uplifted','cohort_uplifted_value','spread','activated_conversion_rate','activated_uplift_conversion_rate'])


        simulate=simulate.append(temp)
    simulate=simulate.reset_index()
    return simulate
   
activated_cost=5
offer='SPEND $60 GET 1500 PTS'
runs=1500
simulate=simiulation_step1(campaigns,activated_cost,offer,runs)
simulate_ecom=simiulation_step1(campaigns_ecom,activated_cost,offer,runs)   
campaigns_control=campaigns_control.fillna(0)
simulate_control=simiulation_step1(campaigns_control,activated_cost,offer,runs,control=True)   
 


simulate.to_feather('simulate2.feather')
simulate_ecom.to_feather('simulate_ecom2.feather')
simulate_control.to_feather('simulate_control2.feather')

simulate=pd.read_feather('simulate2.feather')
simulate_ecom=pd.read_feather('simulate_ecom2.feather')
simulate_control=pd.read_feather('simulate_control2.feather')


plot_df = simulate #[results_df['CAC']<=1000]?
 

simulate.columns

greens=['forestgreen','limegreen','darkgreen','green','lime','seagreen', 'mediumseagreen','springgreen','lightgreen','palegreen']


print('The mean instore activation rate is: ',  "{0:.2%}".format(simulate.activated_conversion_rate.mean()) 	)
print('The mean instore uplifted CAV rate (from activated samples) is: ',  "{0:.2%}".format(simulate.activated_uplift_conversion_rate.mean()) 	)
print('The mean instore activation rate is: ',  "{0:.2%}".format(simulate_ecom.activated_conversion_rate.mean()) 	)
print('The mean instore uplifted CAV rate (from activated samples) is: ',  "{0:.2%}".format(simulate_ecom.activated_uplift_conversion_rate.mean()) 	)


 
fig, ax = plt.subplots(1,2,figsize=(10,5)) 
 
sns.barplot(
    x=["Instore","Ecom"], 
    y=[simulate.activated_conversion_rate.mean(),simulate_ecom.activated_conversion_rate.mean() ], color=greens[2],ax=ax[0]);
 
sns.barplot(
    x=["Instore","Ecom"], 
    y=[simulate.activated_uplift_conversion_rate.mean(), simulate_ecom.activated_uplift_conversion_rate.mean()], color=greens[2],ax=ax[1]);
  

ax[0].set_ylabel('Density', fontsize=10)
ax[0].set_xlabel('Activation rate', fontsize=10)
ax[0].yaxis.set_major_formatter(mtick.PercentFormatter())


ax[1].set_ylabel('Density', fontsize=10)
ax[1].set_xlabel('Uplift rate', fontsize=10)
ax[1].yaxis.set_major_formatter(mtick.PercentFormatter())

fig.suptitle("Instore vs. Ecom activation rates", fontsize=16)

fig.tight_layout()

fig.show()

fig, ax = plt.subplots(figsize=(9,6))

sns.distplot(simulate['customers_uplifted_after'],color= greens[2], kde=False, bins=20)
sns.distplot(simulate_ecom['customers_uplifted_after'],color= greens[3], kde=False, bins=20)
# Setting the X and Y Label
plt.xlabel('CAV simulation')
plt.ylabel('Density')
plt.ticklabel_format(style='plain', axis='y')
plt.ticklabel_format(style='plain', axis='x')
plt.title('Instore activated and CAV uplifed (subset of activated) - Instore and Ecom', fontsize=20)
fig.legend(labels=['Instore','Ecom'], loc='lower center', borderaxespad=-0.7)

plt.tight_layout()

plt.show()


fig, ax = plt.subplots(figsize=(9,6))

sns.distplot(simulate['customers_activated_after'],color= greens[2], kde=False, bins=20)
sns.distplot(simulate_ecom['customers_activated_after'],color= greens[3], kde=False, bins=20)
# Setting the X and Y Label
plt.xlabel('CAV simulation')
plt.ylabel('Density')
plt.ticklabel_format(style='plain', axis='y')
plt.ticklabel_format(style='plain', axis='x')
plt.title('Activated Campaign- Instore and Ecom', fontsize=20)
fig.legend(labels=['Instore','Ecom'], loc='lower center', borderaxespad=-0.7)

plt.tight_layout()

plt.show()
 

fig, ax = plt.subplots(1,2,figsize=(10,5))
#sns.kdeplot(plot_df['spending_before_activated'], color= greens[0], shade=True, Label='before_activated')
#sns.kdeplot(plot_df['spending_after_activated'], color=greens[1], shade=True, Label='after_activated')
sns.kdeplot(simulate['spending_before_uplifted'], color= greens[2], shade=True, Label='online',ax=ax[0])
sns.kdeplot(simulate['spending_after_uplifted'], color=greens[3], shade=True, Label='instore',ax=ax[0])

sns.kdeplot(simulate_ecom['spending_before_uplifted'], color= greens[2], shade=True, Label='online',ax=ax[1])
sns.kdeplot(simulate_ecom['spending_after_uplifted'], color=greens[3], shade=True, Label='instore',ax=ax[1])
# Setting the X and Y Label

ax[0].set_ylabel('Density', fontsize=10)
ax[0].set_xlabel('Instore', fontsize=10)
ax[0].ticklabel_format(style='plain', axis='y')
ax[0].ticklabel_format(style='plain', axis='x')


ax[1].set_ylabel('Density', fontsize=10)
ax[1].set_xlabel('Ecom', fontsize=10)
ax[1].ticklabel_format(style='plain', axis='y')
ax[1].ticklabel_format(style='plain', axis='x')
fig.suptitle("Uplifted cohort - spending before vs. after campaign", fontsize=16)
ax[0].legend(labels=['before campaign','after campaign'], loc='upper right', borderaxespad=-0.3)
ax[1].legend(labels=['before campaign','after campaign'], loc='upper right', borderaxespad=-0.3)

fig.tight_layout()

fig.show()


fig, ax = plt.subplots(1,2,figsize=(10,5))
#sns.kdeplot(plot_df['spending_before_activated'], color= greens[0], shade=True, Label='before_activated')
#sns.kdeplot(plot_df['spending_after_activated'], color=greens[1], shade=True, Label='after_activated')
sns.kdeplot(simulate['spending_before_activated'], color= greens[2], shade=True, Label='online',ax=ax[0])
sns.kdeplot(simulate['spending_after_activated'], color=greens[3], shade=True, Label='instore',ax=ax[0])

sns.kdeplot(simulate_ecom['spending_before_activated'], color= greens[2], shade=True, Label='online',ax=ax[1])
sns.kdeplot(simulate_ecom['spending_after_activated'], color=greens[3], shade=True, Label='instore',ax=ax[1])
# Setting the X and Y Label

ax[0].set_ylabel('Density', fontsize=10)
ax[0].set_xlabel('Instore', fontsize=10)
ax[0].ticklabel_format(style='plain', axis='y')
ax[0].ticklabel_format(style='plain', axis='x')


ax[1].set_ylabel('Density', fontsize=10)
ax[1].set_xlabel('Ecom', fontsize=10)
ax[1].ticklabel_format(style='plain', axis='y')
ax[1].ticklabel_format(style='plain', axis='x')
fig.suptitle("Activated cohort - spending before vs. after campaign", fontsize=16)
ax[0].legend(labels=['before campaign','after campaign'], loc='upper right', borderaxespad=-0.3)
ax[1].legend(labels=['before campaign','after campaign'], loc='upper right', borderaxespad=-0.3)

fig.tight_layout()

fig.show()

fig, ax = plt.subplots(1,2,figsize=(10,5))
#sns.kdeplot(simulate['cohort_uplifted_value'], color=greens[2], shade=True, Label='instore',ax=ax[0])
#sns.kdeplot(simulate_ecom['cohort_uplifted_value'], color=greens[3], shade=True, Label='instore',ax=ax[0])

sns.distplot(simulate['cohort_uplifted_value'],color= greens[2], kde=False, bins=20,ax=ax[0])
sns.distplot(simulate_ecom['cohort_uplifted_value'],color= greens[3], kde=False, bins=20,ax=ax[0])

sns.distplot(simulate['CAC'],color= greens[2], kde=False, bins=20,ax=ax[1])
sns.distplot(simulate_ecom['CAC'],color= greens[3], kde=False, bins=20,ax=ax[1])
ax[0].set_ylabel('Frequency', fontsize=10)
ax[0].set_xlabel('Uplifted spend (after - before)', fontsize=10)
ax[0].ticklabel_format(style='plain', axis='y')
ax[0].ticklabel_format(style='plain', axis='x')


ax[1].set_ylabel('Frequency', fontsize=10)
ax[1].set_xlabel('Cost of campaign', fontsize=10)
ax[1].ticklabel_format(style='plain', axis='y')
ax[1].ticklabel_format(style='plain', axis='x')
fig.suptitle("Expected revenue and cost of campaign", fontsize=16)
ax[0].legend(labels=['before campaign','after campaign'], loc='upper right', borderaxespad=-0.3)
ax[1].legend(labels=['before campaign','after campaign'], loc='upper right', borderaxespad=-0.3)

fig.tight_layout()

fig.show()
	


 


fig, ax = plt.subplots(figsize=(9,6))
sns.kdeplot(simulate['spread'], color= greens[2], shade=True, Label='online')
sns.kdeplot(simulate_ecom['spread'], color=greens[3], shade=True, Label='instore')
 
# Setting the X and Y Label
plt.xlabel('CAV simulation')
plt.ylabel('Density')
plt.axvline(x=0, color='red')
plt.ticklabel_format(style='plain', axis='y')
plt.ticklabel_format(style='plain', axis='x')
plt.title('Uplifted value - cost of campaign', fontsize=20)
fig.legend(labels=['zero','Instore','Ecom'], loc='upper right', borderaxespad=-0.5)

plt.tight_layout()

plt.show()



 
fig, ax = plt.subplots(figsize=(9,6))
sns.distplot(plot_df['cohort_size_activated'],color= greens[2], kde=False, bins=100)
sns.distplot(plot_df['cohort_size_uplift'],color= greens[2], kde=False, bins=100)

#sns.kdeplot(plot_df['cohort_size_activated'], color= greens[2], shade=True, Label='online')

ax.set_xlabel("cohort_size_activated",fontsize=16)
ax.set_ylabel("Frequency",fontsize=16)
plt.tight_layout()
plt.ticklabel_format(style='plain', axis='y')
plt.ticklabel_format(style='plain', axis='x')
plt.show()




# Histogram for distribution of CLTV-CAC Spread
fig, ax = plt.subplots(figsize=(9,6))
sns.distplot(plot_df['spread'], kde=False, bins=150)
plt.axvline(x=0, color='red')
#plt.xlim(-200, 80)

ax.set_xlabel("CLTV - CAC Spread",fontsize=16)
ax.set_ylabel("Frequency",fontsize=16)
plt.tight_layout()

plt.savefig(fname='spread_hist', dpi=150)
plt.show()











 #Value of the cohort in today's dollars is sum of PVs
cohort_uplifted_value_tot = sum(spending_after_uplifted)
cohort_uplifted_value = sum(spending_after_uplifted)-sum(spending_before_uplifted)
spread=int(cohort_uplifted_value - CAC)
print('Total Cohort Value for CAV converted: ', int(cohort_uplifted_value_tot))
print('Total additional spend for CAV converted: ', int(cohort_uplifted_value))
print('CLTV-CAC Spread: ', int(cohort_uplifted_value - CAC))



simulate=pd.DataFrame(columns=['CAC','cohort_size_activated','cohort_size_uplift','Pcent_uplift','Pcent_activated','customers_activated_before','customers_activated_after','customers_uplifted_before','customers_uplifted_after','spending_before_activated', 'spending_after_activated', 'spending_before_uplifted', 'spending_after_uplifted','cohort_uplifted_value','spread' ])



# Simulate 1000 times and look at the distributions
activated_cost=5
# Conversion rate

chosen_campaign=campaigns[campaigns.offer_desc=='10x points on the entire Macro range']


targeted_customers=chosen_campaign.customers/chosen_campaign.campaigns


conversion_rate_activated_expected =chosen_campaign.cav_conversion_rate.iloc[0]
conversion_rate_activated_std=chosen_campaign.activated_std.iloc[0]
conversion_rate_uplifted_expected =chosen_campaign.cav_conversion_activated_rate_from_activated.iloc[0]
conversion_rate_uplifted_std=chosen_campaign.activated_cav_conversion_std.iloc[0]




for i in range(1000):
    
    # Run marketing campaign sim
    conversion_rate = get_conversion_rate(conversion_rate_expected, 
                                          conversion_rate_stdev)
    cohort_size = run_campaign( conversion_rate,targeted_customers=targeted_customers)
    if cohort_size<1:
     cohort_size=1
    else:
     cohort_size=cohort_size
    CAC = activated_cost*cohort_size
    
    # Simulate the resulting cohort
    customers_left_before, spending_before =simulate_cohort(cohort_size[0], churn_rate,avg_yr_pred_before, yrs=1)
    customers_left_after, spending_after =simulate_cohort(cohort_size[0], churn_rate,avg_yr_pred_after, yrs=1)
    customers_left_activated_before, spending_activated_before =simulate_cohort(cohort_size[0], churn_rate,avg_yr_pred_activated_before, yrs=1)
    customers_left_activated_after, spending_activated_after =simulate_cohort(cohort_size[0], churn_rate,avg_yr_pred_activated_after, yrs=1)

    
    #cohort_value = sum(spending)
    
    cohort_size_list.append(cohort_size)
    CAC_list.append(CAC)
    #CLTV_list.append(cohort_value/cohort_size)
    CLTV_list_before.append(sum(spending_before)/cohort_size)
    CLTV_list_after.append(sum(spending_after)/cohort_size)
    CLTV_list_activated_before.append(sum(spending_activated_before)/cohort_size)
    CLTV_list_activated_after.append(sum(spending_activated_after)/cohort_size)
    
        temp=pd.DataFrame([[CAC,cohort_size_activated,cohort_size_uplift,Pcent_uplift[0],float(Pcent_activated),customers_activated_before,customers_activated_after,customers_uplifted_before,customers_uplifted_after,spending_before_activated[0], spending_after_activated[0], spending_before_uplifted[0], spending_after_uplifted[0],cohort_uplifted_value,spread]],columns=['CAC','cohort_size_activated','cohort_size_uplift','Pcent_uplift','Pcent_activated','customers_activated_before','customers_activated_after','customers_uplifted_before','customers_uplifted_after','spending_before_activated', 'spending_after_activated', 'spending_before_uplifted', 'spending_after_uplifted','cohort_uplifted_value','spread' ])


simulate=simulate.append(temp)
    
# Store simulation results in a dataframe

cohort_size_list=np.concatenate(cohort_size_list).tolist()
CAC_list=np.concatenate(CAC_list).tolist()
CLTV_list_before=np.concatenate(CLTV_list_before).tolist()
CLTV_list_after=np.concatenate(CLTV_list_after).tolist()
CLTV_list_activated_before=np.concatenate(CLTV_list_activated_before).tolist()
CLTV_list_activated_after=np.concatenate(CLTV_list).tolist()



results_df = pd.DataFrame()
results_df['initial_cohort_size'] = cohort_size_list
results_df['CLTV_before'] = CLTV_list_before
results_df['CLTV_after'] = CLTV_list_after
results_df['CLTV_activated_before'] = CLTV_list_activated_before
results_df['CLTV_activated_after'] = CLTV_list_activated_after

results_df['CAC'] = CAC_list
results_df['Spread'] = results_df['CLTV'] - results_df['CAC']

results_df.to_feather('results_df.feather')
results_df=pd.read_feather('results_df.feather')
plot_df = results_df #[results_df['CAC']<=1000]


# Histogram for distribution of initial cohort size
fig, ax = plt.subplots(figsize=(9,6))
sns.distplot(plot_df['initial_cohort_size'],color='green' ,kde=False, bins=30)

ax.set_xlabel("Initial Cohort Size",fontsize=16)
ax.set_ylabel("Frequency",fontsize=16)
plt.title('Estimated customers with increased CAV (10%)', fontsize=20)
plt.tight_layout()
plt.ticklabel_format(style='plain', axis='x')
plt.savefig(fname='cohort_hist', dpi=150)
plt.show()

# Histogram for distribution of initial cohort size
fig, ax = plt.subplots(figsize=(9,6))
sns.distplot(plot_df['CLTV'],color='green', kde=False, bins=30)
plt.title('Customer Future Value', fontsize=20)
ax.set_xlabel("Customer Lifetime Value",fontsize=16)
ax.set_ylabel("Frequency",fontsize=16)
plt.tight_layout()

plt.savefig(fname='CLTV_hist', dpi=150)
plt.show()


greens=['forestgreen','limegreen','darkgreen','green','lime','seagreen', 'mediumseagreen','springgreen','lightgreen','palegreen']

fig, ax = plt.subplots(figsize=(9,6))
sns.kdeplot(plot_df['CLTV_after'], color= greens[0], shade=True, Label='control')
sns.kdeplot(plot_df['CLTV_activated_after'], color=greens[1], shade=True, Label='cohort')

 
# Setting the X and Y Label
plt.xlabel('CAV simulation')
plt.ylabel('Density')
plt.title('Expected cohort CAV vs. control', fontsize=20)
fig.legend(labels=['Control avg. CAV','Cohort avg. CAV'])

plt.tight_layout()

plt.savefig(fname='CLTV_hist', dpi=150)
plt.show()






fig, ax = plt.subplots(figsize=(9,6))
sns.kdeplot(plot_df['CLTV_activated_before'], color= greens[0], shade=True, Label='online')
sns.kdeplot(plot_df['CLTV_activated_after'], color=greens[1], shade=True, Label='instore')

 
# Setting the X and Y Label
plt.xlabel('CAV simulation')
plt.ylabel('Density')
plt.title('CAV simulation', fontsize=20)
fig.legend(labels=['before campaign','after campaign'])

plt.tight_layout()

plt.savefig(fname='CLTV_hist', dpi=150)
plt.show()




fig, ax = plt.subplots(figsize=(9,6))
sns.kdeplot(plot_df['CLTV_before'], color= greens[2], shade=True, Label='online')
sns.kdeplot(plot_df['CLTV_after'], color=greens[3], shade=True, Label='instore')
 
# Setting the X and Y Label
plt.xlabel('CAV simulation')
plt.ylabel('Density')
plt.title('CAV simulation (control- not activated)', fontsize=20)
fig.legend(labels=['before campaign (not activated)','after campaign (not activated)'], loc='lower center', borderaxespad=-0.5)

plt.tight_layout()

plt.show()



 
fig, ax = plt.subplots(figsize=(9,6))
sns.distplot(plot_df['CAC'], kde=False, bins=60)

ax.set_xlabel("Customer Activation Cost",fontsize=16)
ax.set_ylabel("Frequency",fontsize=16)
plt.tight_layout()

plt.savefig(fname='CAC_hist', dpi=150)
plt.show()




# Histogram for distribution of CLTV-CAC Spread
fig, ax = plt.subplots(figsize=(9,6))
sns.distplot(plot_df['Spread'], kde=False, bins=150)
plt.axvline(x=0, color='red')
plt.xlim(-200, 80)

ax.set_xlabel("CLTV - CAC Spread",fontsize=16)
ax.set_ylabel("Frequency",fontsize=16)
plt.tight_layout()

plt.savefig(fname='spread_hist', dpi=150)
plt.show()