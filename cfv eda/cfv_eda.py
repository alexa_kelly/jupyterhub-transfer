
import pandas as pd
import os
from google.cloud import bigquery
from google.cloud import storage
import pickle
os.environ['GOOGLE_APPLICATION_CREDENTIALS']=f"/home/jovyan/.config/gcloud/legacy_credentials/{os.getenv('JUPYTERHUB_USER')}/adc.json"
import numpy as np
import sys
np.set_printoptions(threshold=sys.maxsize)
pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 500)
from pandas_profiling import ProfileReport
import matplotlib.pyplot as plt
import matplotlib
import seaborn as sns
from matplotlib.ticker import ScalarFormatter
from sklearn.metrics import mean_squared_error
import statsmodels.api as sm
import subprocess
project_id = 'gcp-wow-rwds-ai-cfv-prod'


os.system('gsutil cp -r "/home/jovyan/campaign analysis /" ~/jupyterhub-transfer/')

 
os.system('gsutil cp -r "/home/jovyan/cfv1/" ~/jupyterhub-transfer/')

from_path='~/auto_ml/'


os.system('gsutil cp -r '+from_path+' ~/jupyterhub-transfer/')



import shutil
from shutil import copytree


from distutils.dir_util import copy_tree
import distutils.dir_util
 
sourcedir='/home/jovyan/campaign analysis '
destination='/home/jovyan/jupyterhub-transfer/'
distutils.dir_util.copytree(sourcedir, destination)


    BASE_CUST_START_DATE: 2021-12-30
    RUN_DATE: 
    BASE_CUST_END_DATE: 2022-01-06

from datetime import datetime,timedelta
RUN_DATE='20220106'
BASE_CUST_START_DATE = datetime.strptime(RUN_DATE, '%Y%m%d').strftime('%Y-%m-%d')
BASE_CUST_END_DATE=(datetime.strptime(BASE_CUST_START_DATE, '%Y-%m-%d')- timedelta(days=7)).strftime('%Y-%m-%d')

print('RUN_DATE: ',RUN_DATE,', BASE_CUST_START_DATE: ' ,BASE_CUST_START_DATE,', BASE_CUST_END_DATE:', BASE_CUST_END_DATE)



M=pd.read_parquet('gs://wx-auto-ml/alexa/cfv/ecom3m-bernoulli/alexae3mb20210202/feature_selection/feature_selection_output_M.parquet')
T=pd.read_parquet('gs://wx-auto-ml/alexa/cfv/ecom3m-bernoulli/alexae3mb20210202/feature_selection/feature_selection_output_T.parquet')


M=pd.read_parquet('gs://wx-321b539d-04ab-4be0-a707-524efb7a98d8/scoring/dev/runs/20220106testprod/base_audience.parquet')
M.head()


M.groupby('target').count()
T.groupby('target').count()

query= "select distinct(ref_dt) from feature_build.base_cust;"
pd.read_gbq(query, project_id=project_id)

query= "select * from akelly.test;"
d=pd.read_gbq(query, project_id=project_id)

query= "select ref_dt, count(distinct(crn)) from score.cfv_bigw_instore_segments group by 1 order by 1"
pd.read_gbq(query, project_id=project_id)



query= "select spend_range, count(*) as cnt from akelly.sample_base_cust_full_model_2020_to_2021_ecom3mooo group by 1;"
pd.read_gbq(query, project_id=project_id)

query= "select ref_dt, count(*) as cnt from akelly.sample_base_cust_full_model_2020_to_2021_ecom3mooo group by 1;"
pd.read_gbq(query, project_id=project_id)

query= "SELECT count(*),count(distinct (crn)) from akelly.sample_base_cust_full_model_2020_to_2021_ecom3mooo ;"
pd.read_gbq(query, project_id=project_id)
#2245619


query= "select spend_range, count(*) as cnt from akelly.temp_base_cust_2020_to_2021_ecom3m group by 1;"
pd.read_gbq(query, project_id=project_id)


query= "SELECT count(*),count(distinct (crn)) from akelly.sample_base_cust_full_model_2020_to_2021_ecom3m ;"
pd.read_gbq(query, project_id=project_id)

query= "SELECT * from akelly.sample_base_cust_full_model_2020_to_2021_ecom3m limit 6 ;"
d=pd.read_gbq(query, project_id=project_id)



query= "SELECT * from gcp-wow-rwds-ai-cfv-dev.ecom.ecom_target_3m where crn='3300000000001129783' ;"
dd=pd.read_gbq(query, project_id=project_id)
dd.sort_values(by='ref_dt',ascending=False)


d_bands_o='gs://wx-lty-cfv-dev/scoring/dev/models/bws/instore/spend/dollar_bands.parquet'    
pd.read_parquet(d_bands_o)



dd=pd.read_parquet('gs://wx-lty-cfv-dev/scoring/dev/runs/20220106/extract/partition_0/extra_feature.parquet')
dd.head()

dd.columns
q='select count(*) from akelly.bws_online_base_2020_to_2021_with_kimchi_with_cust_seg_offer'
pd.read_gbq(q, project_id=project_id)
#367057


q='select count(*) from akelly.sample_base_cust_full_model_2020_to_2021_ecom_bws'
pd.read_gbq(q, project_id=project_id)
#367057

temp_base_cust_2020_to_2021_ecom_bws
q='select count(*) from akelly.bigw_instore_base_2020_to_2021_with_kimchi_with_cust_seg_offer'
pd.read_gbq(q, project_id=project_id)
#2824226
#5033786
#1829567
q='select count(*) from akelly.temp_base_cust_2020_to_2021_instore_bws'
pd.read_gbq(q, project_id=project_id)
#366460


q='select count(*) from akelly.bws_instore_2020_train_model_refresh'
pd.read_gbq(q, project_id=project_id)
#366460


q='select count(*) from akelly.bws_instore_base_2020_to_2021_with_kimchi_with_cust_seg'
pd.read_gbq(q, project_id=project_id)
#366460

q='select *, from akelly.temp_base_cust_2020_to_2021_instore_bws'
bwsspend=pd.read_gbq(q, project_id=project_id)
 
spend=bwsspend[bwsspend['bws_tot_spend']<20000]['bws_tot_spend']
fig = plt.figure(figsize=(10,6))

sns.kdeplot(spend, color='green', shade=True, Label='instore')
#sns.kdeplot(bigw_instore['bigw_instore_tot_spend'], color='green', shade=True, Label='instore')
 
# Setting the X and Y Label
plt.xlabel('BWS Total Spend')
plt.ylabel('Probability Density')
plt.title('Total distribution', fontsize=20)
fig.legend(labels=['instore'])

plt.show()



query= "SELECT date_sub('2021-10-13', interval 1 week);"
pd.read_gbq(query, project_id=project_id)
print(backfill.iloc[0].astype(str))

backfill.to_string()

query= """
DECLARE backfill DATE;

SET backfill = '2021-10-13';
BEGIN
    CALL feature_build.sp_feature_build_base_cust (date_sub(backfill, interval 1 week), backfill);
    CALL ecom.sp_feature_build_ecom_target_3m();
END
"""



query = """
select spend_range, count(*) as cnt, count(distinct crn) as crns from 
 `gcp-wow-rwds-ai-cfv-dev.akelly.sample_base_cust_full_model_2020_to_2021_instore_bigw`
group by 1
order by spend_range desc; 
"""
d=pd.read_gbq(query, project_id=project_id)
 

d.sort_values(by='spend_range').reset_index()
order_agebin = pd.value_counts(d['spend_range']).sort_index().index




plt.figure(figsize=(12, 6), tight_layout=True)
ax = sns.barplot(x=d['spend_range'], y=d['crns'], palette='Greens' ,order= order_agebin)#,order=barplot['offer_status'] ) #, palette='pastel'
#ax.set(title='Distribution of supers total (Nov20-Nov21)' ,xlabel='Spend Range', ylabel='Customers')
ax.set(title='Stratified ecom sample distribution (Nov20-Nov21)' ,xlabel='Spend Range', ylabel='Customers')

plt.xticks(rotation=45)
ax.get_yaxis().set_major_formatter(
    matplotlib.ticker.FuncFormatter(lambda x, p: format(int(x), ',')))
plt.show()



sql = """
create or replace table akelly.supers_instore_scored_sow as select 
a.ref_dt ,a.pred_bin,count(distinct(a.crn)) as crns
,min(b.sow) as min_sow,avg(b.sow) as mean_sow,max(b.sow) as max_sow from
score.cfv_instore_segments as a 
join feature_build.cust_detail as b on a.crn=b.crn and 
CONCAT(EXTRACT( YEAR FROM a.ref_dt),EXTRACT( MONTH FROM a.ref_dt))  =CONCAT(EXTRACT( YEAR FROM b.ref_dt),EXTRACT( MONTH FROM b.ref_dt)) and EXTRACT( day FROM a.ref_dt)>=EXTRACT( day FROM b.ref_dt)
group by 1,2; 
"""


sql = """
create or replace table akelly.supers_ecom_scored_sow as select 
a.ref_dt ,a.pred_bin,count(distinct(a.crn)) as crns
,min(b.sow) as min_sow,avg(b.sow) as mean_sow,max(b.sow) as max_sow from
score.cfv_ecom_segments as a 
join feature_build.cust_detail as b on a.crn=b.crn and 
CONCAT(EXTRACT( YEAR FROM a.ref_dt),EXTRACT( MONTH FROM a.ref_dt))  =CONCAT(EXTRACT( YEAR FROM b.ref_dt),EXTRACT( MONTH FROM b.ref_dt)) and EXTRACT( day FROM a.ref_dt)>=EXTRACT( day FROM b.ref_dt)
group by 1,2; 
"""
 # and EXTRACT( day FROM a.ref_dt)>=EXTRACT( day FROM b.ref_dt)
ecom_sow=pd.read_gbq(sql, project_id=project_id)

instore_sow=pd.read_gbq(sql, project_id=project_id)

instore_sow.sort_values(by='ref_dt',acsending=False)

 
instore_sow=instore_sow[instore_sow['mean_sow'].notna()]
df=ecom_sow.copy()
df=df[df.ref_dt<='2021-12-15']
df.sort_values(by='ref_dt',ascending=False)

df.index = df.ref_dt 
del df['ref_dt'] 

cat_type = CategoricalDtype(categories=["zero", "low", "lowmed","medium","highmed","high"], ordered=True)
df.pred_bin = df.pred_bin.astype(cat_type)

grouped = df.groupby('pred_bin') 


colors = sns.color_palette('Greens',7)



fig = plt.figure()
ax = plt.subplot(111)
for i, (k, g) in enumerate(grouped):
    ax.plot_date(g['mean_sow'].index, g['mean_sow'],         marker='o',
         fillstyle='full', color=colors[i+1], label=k)
box = ax.get_position()
ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])
ax.legend(title='CAV Prediction', title_fontsize='13',loc='center left', bbox_to_anchor=(1, 0.5))
plt.title('SOW by supers ecoom CAV prediction', fontsize=15)
#ax.title('In store spend performance: True vs. Predicted spend')
plt.xlabel('Month', fontsize=10)
plt.ylabel('SOW', fontsize=10)
plt.gcf().autofmt_xdate() # Format the dates with a diagonal slant to make them fit.

# Pad the data out so all markers can be seen.
#pad = dt.timedelta(days=7)
#ax.xlim((min(df.index)-pad, max(df.index)+pad))
#ax.ylim(0,6)
plt.show()




q='select * from gcp-wow-rwds-ai-cfv-prod.score.cfv_instore_segments where crn="1000000000002577642"'

q='select * from gcp-wow-rwds-ai-cfv-prod.score.cfv_group_scores where crn="1000000000002568043"'
d1=pd.read_gbq(q, project_id=project_id)
 
q='select * from gcp-wow-rwds-ai-cfv-prod.score.cfv_group_scores where shopper_id="3040970"'
d1=pd.read_gbq(q, project_id=project_id)
 

q='select  crn,shopper_id,ref_dt,adjusted_score_spend,pred_bin from gcp-wow-rwds-ai-cfv-prod.score.cfv_bigw_online_segments where crn="3300000000001290723" order by ref_dt desc'
bigw_online=pd.read_gbq(q, project_id=project_id)
 

 
q='select  crn,shopper_id,ref_dt,adjusted_score_spend,pred_bin from gcp-wow-rwds-ai-cfv-prod.score.cfv_bws_online_segments where crn="3300000000001290723" order by ref_dt desc'
bws_online=pd.read_gbq(q, project_id=project_id)
  
q='select  crn,shopper_id,ref_dt,adjusted_score_spend,pred_bin from gcp-wow-rwds-ai-cfv-prod.score.cfv_bigw_instore_segments where crn="3300000000001290723" order by ref_dt desc'
bigw_instore=pd.read_gbq(q, project_id=project_id)
 

 
q='select  crn,shopper_id,ref_dt,adjusted_score_spend,pred_bin from gcp-wow-rwds-ai-cfv-prod.score.cfv_bws_instore_segments where crn="3300000000001290723" order by ref_dt desc'
bws_instore=pd.read_gbq(q, project_id=project_id)


writer = pd.ExcelWriter('crn3300000000001290723.xlsx', engine='xlsxwriter')
bigw_online.to_excel(writer, sheet_name='bigw_online')
bws_online.to_excel(writer, sheet_name='bws_online')
bigw_instore.to_excel(writer, sheet_name='bigw_instore')
bws_instore.to_excel(writer, sheet_name='bws_instore')
writer.save()


"1000000000002568043"
 
q='select * from  `gcp-wow-ent-im-wowx-cust-prod`.adp_wowx_dm_customer_view.loyalty_card_detail_v  where CustomerRegistrationNumber="1000000000002577642"'
d=pd.read_gbq(q, project_id=project_id)
 
q='select * from `gcp-wow-ent-im-wowx-cust-prod.adp_wowx_dm_customer_view.customer_online_shopper_v` where ShopperID="3040970"'
ddd=pd.read_gbq(q, project_id=project_id)
 
 
  (SELECT 
              PrimaryCustomerRegistrationNumber as crn
             ,min(LoyaltyCardRegistrationDate) as lylty_card_rgstr_date 
         FROM `gcp-wow-ent-im-wowx-cust-prod`.adp_wowx_dm_customer_view.loyalty_card_detail_v 
         WHERE LoyaltyCardStatusDescription not in ('Cancelled', 'Closed', 'Deregistered')
         AND PrimaryCustomerRegistrationNumber != '0'
         GROUP BY PrimaryCustomerRegistrationNumber) lc
    FULL JOIN 


q='select count(*) from akelly.sample_base_cust_full_model_2020_to_2021_ecom_bigw'
pd.read_gbq(q, project_id=project_id)
#667746

q='select count(*) from akelly.bigw_online_base_2020_to_2021_with_kimchi_with_cust'
pd.read_gbq(q, project_id=project_id)
#667746

q='select count(*) from akelly.bigw_online_base_2020_to_2021_with_kimchi_with_cust_seg'
pd.read_gbq(q, project_id=project_id)
#667746


q='select count(*) from akelly.bigw_online_base_2020_to_2021_with_kimchi_with_cust_seg_offer'
pd.read_gbq(q, project_id=project_id)
#667746
#667746
q='select count(*) from akelly.sample_base_cust_full_model_2020_to_2021_instore_bigw'
pd.read_gbq(q, project_id=project_id)
#5033786

q='select count(*) from akelly.sample_base_cust_full_model_2020_to_2021_instore_bigw'
pd.read_gbq(q, project_id=project_id)
#5033786

q='select count(*) from akelly.bigw_instore_base_2020_to_2021_with_kimchi_with_cust_seg_offer'
pd.read_gbq(q, project_id=project_id)
#667746

q="""select count(*),  max(bigw_online_tot_spend),min(bigw_online_tot_spend)
from akelly.bigw_online_base_2020_to_2021_with_kimchi_with_cust_seg_offer"""
pd.read_gbq(q, project_id=project_id)



	f0_	f1_	f2_
0	667746	14901.74	0.25


q="""select count(*),  max(bigw_instore_tot_spend),min(bigw_instore_tot_spend)
from akelly.bigw_instore_base_2020_to_2021_with_kimchi_with_cust_seg_offer"""
pd.read_gbq(q, project_id=project_id)


f0_	f1_	f2_
0	5033786	14946.27	0.01


q="""select ref_dt,crn,bigw_online_tot_spend from akelly.bigw_online_base_2020_to_2021_with_kimchi_with_cust_seg_offer"""

bigw_ecom=pd.read_gbq(q, project_id=project_id)


q="""select spend_range, count(distinct(crn)) as crns from akelly.bigw_review_spend_range_online
group by 1"""

spendrange=pd.read_gbq(q, project_id=project_id)

spendrange['crns'].sum()
q="""SELECT column_name, COUNT(1) AS nulls_count
FROM akelly.bws_online_base_2020_to_2021_with_kimchi_with_cust_seg_offer,
UNNEST(REGEXP_EXTRACT_ALL(TO_JSON_STRING(bws_online_base_2020_to_2021_with_kimchi_with_cust_seg_offer), r'"(\w+)":null')) column_name
GROUP BY column_name
ORDER BY nulls_count DESC"""

nulls=pd.read_gbq(q, project_id=project_id)

bws_online_len=pd.read_gbq("""select count(*) from akelly.bws_online_base_2020_to_2021_with_kimchi_with_cust_seg_offer""", project_id=project_id)
 

for i in range(len(nulls)):
 if nulls.iloc[i,1]/ 367057>=.8:
  print(nulls.iloc[i,0])



q="""select ref_dt,crn,bigw_instore_tot_spend from akelly.bigw_instore_base_2020_to_2021_with_kimchi_with_cust_seg_offer"""

bigw_instore=pd.read_gbq(q, project_id=project_id)

fig = plt.figure(figsize=(10,6))

sns.kdeplot(bigw_ecom['bigw_online_tot_spend'], color='green', shade=True, Label='online')
sns.kdeplot(bigw_instore['bigw_instore_tot_spend'], color='green', shade=True, Label='instore')
 
# Setting the X and Y Label
plt.xlabel('BIGW Total Spend')
plt.ylabel('Probability Density')
plt.title('Total distribution', fontsize=20)
fig.legend(labels=['online','instore'])

plt.show()


#1100000000094231533 tot_sales for supers is 853 but my calculations show $2111
#Here’s my code:
#Check base table
select supers_tot_spend,supers_order_frequency
from akelly.instore_base_2020_to_2021
where crn='1100000000094231533'
#Check supers table
q="""select * from gcp-wow-rwds-ai-cfv-dev.supers.supers_target where crn='1100000000009758276' and ref_dt = '2020-12-27';"""



d1=pd.read_gbq(q, project_id=project_id)
#Manual calculation

q="""select * from gcp-wow-ent-im-wowx-cust-prod.adp_wowx_dm_customer_view.loyalty_card_detail_v
where CustomerRegistrationNumber in ('1100000000009758276') or PrimaryCustomerRegistrationNumber in('1100000000009758276')"""
d3=pd.read_gbq(q, project_id=project_id)




q="""
 WITH article_sales_summary AS ( SELECT
       ass.LoyaltyCardNumber as lylty_card_nbr
      ,ass.SalesOrg as division_nbr
      ,ass.ArticleWithUOM as prod_nbr
      ,ass.TXNStartDate as start_txn_date
      ,ass.BasketKey as basket_key
      ,ass.TotalAmountIncldTax as tot_amt_incld_gst
      ,ass.BaseUnitQty as base_unit_qty
      ,ass.TotalMeasuredQty as tot_measured_qty
      ,ass.TotalOfferDiscountIncldTax as tot_offer_dscnt_incld_gst
      ,ass.TotalOfferDiscountIncldTax as tot_staff_dscnt_incld_gst
      ,ass.TotalUnknownDiscountIncldTax as tot_unknown_dscnt_incld_gst
      ,(ass.TotalOfferDiscountIncldTax + ass.TotalOfferDiscountIncldTax + ass.TotalUnknownDiscountIncldTax) as tot_dscnt
    FROM
      `gcp-wow-ent-im-wowx-cust-prod.adp_wowx_dm_integrated_sales_view.article_sales_summary_v` ass
    join gcp-wow-ent-im-wowx-cust-prod.adp_wowx_dm_customer_view.loyalty_card_detail_v c on 
    ass.LoyaltyCardNumber=c.LoyaltyCardNumber
    WHERE c.CustomerRegistrationNumber in ('1100000000094231533'
    )
        AND ass.TXNStartDate >= '2020-12-27' and ass.TXNStartDate <='2021-12-27'
        AND ass.VoidFlag <> 'Y'
        AND ass.SalesOrg IN (1005, 1030)
        AND ass.TotalAmountIncldTax > 0
        AND ass.POSNumber <> 100
  )
  SELECT
    ass.lylty_card_nbr,
    min(ass.start_txn_date) as min_date,
    max(ass.start_txn_date) as max_date,
    count(distinct(basket_key)) as frequency,
    sum(tot_amt_incld_gst) as tot_sales,
    sum(tot_dscnt) as discount
  FROM
    article_sales_summary ass
  LEFT JOIN
    `gcp-wow-ent-im-wowx-cust-prod.adp_wowx_dm_masterdata_view.dim_article_master_v` am
  ON ass.division_nbr = am.SalesOrg
    AND ass.prod_nbr = am.ArticleWithUOM
  WHERE am.DepartmentWithoutCompanyCode <> '20'
      AND am.MerchandiseCategoryLevel2Code <> 40801
      AND am.MerchandiseCategoryLevel4Code NOT IN (208030301, 208030201)
      group by 1;
"""

d2=pd.read_gbq(q, project_id=project_id)


def copy_file(src_dir,dest_dir,filename,do_parallel=True,do_recursive=False, logger=None):
    """
    Copies a file from one location to another using the
    gsutil cp command
    :param src_dir: full source directory path for file
    :param dest_dir: full destination directory path for file
    :param filename: specific file to copy

    :return: None
    """
    try:
        parallel = '-m' if do_parallel else ''
        recursive = '-r' if do_recursive else ''
        if logger:
            logger.info(f'Copying {filename} from: {src_dir} to: {dest_dir}...')
        subprocess.call(f'gsutil {parallel} cp {recursive} {src_dir}/{filename} {dest_dir}',shell=True)
    except Exception as e:
        raise

_model_path= "gs://wx-lty-cfv-dev/scoring/dev/models/bigw/instore/spend"
_model_object_name="model_object.pickle"

copy_file(_model_path, '.', _model_object_name)



###################### HOW TO ACCESS CURRENT PROD WORKFLOW 

scaling_factor_path='gs://wx-lty-cfv-dev/scoring/dev/models/bigw/instore/spend/scaling_factor.parquet'

scaling_factor_path='gs://wx-lty-cfv-dev/scoring/dev/models/ecom_spend/scaling_factor.parquet'
scaling_factor = pd.read_parquet(scaling_factor_path, engine='pyarrow')


gcloud container clusters get-credentials cfv-prod --region us-central1 --project gcp-wow-rwds-ai-cfv-dev
and then
kubectl port-forward service/argo-server 2746:2746
and then go to
http://localhost:2746/workflows/



#CONFIG FILES
#https://console.cloud.google.com/storage/browser/wx-personal/Akshay/testrun_config_files?pageState=(%22StorageObjectListTable%22:(%22f%22:%22%255B%255D%22))&project=wx-bq-poc&prefix=&forceOnObjectsSortingFiltering=false

# model information 
# wx-lty-cfv-dev/scoring/prod/models 
#Filter on the latest fw_end_date or pw_end_date will give you the latest marketable crns.
#https://console.cloud.google.com/storage/browser/_details/wx-lty-cfv-dev/scoring/prod/models/instore_spend/scoring/scored_M.parquet;tab=live_object?pageState=(%22StorageObjectListTable%22:(%22f%22:%22%255B%255D%22))&project=gcp-wow-rwds-ai-cfv-dev


#view in bitbucket
#https://console.cloud.google.com/storage/browser?project=gcp-wow-rwds-ai-cfv-dev&prefix=
#'gcp-wow-rwds-ai-cfv-dev' 

#gcp-wow-rwds-ai-cfv-dev.feature_build.camp_flags_w12
import seaborn as sns
import matplotlib.pyplot as plt
import matplotlib
import seaborn as sns
from matplotlib.ticker import ScalarFormatter
from sklearn.metrics import mean_squared_error



sns.set_style('darkgrid') # darkgrid, white grid, dark, white and ticks
plt.rc('axes', titlesize=18)     # fontsize of the axes title
plt.rc('axes', labelsize=14)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=13)    # fontsize of the tick labels
plt.rc('ytick', labelsize=13)    # fontsize of the tick labels
plt.rc('legend', fontsize=13)    # legend fontsize
plt.rc('font', size=13)          # controls default text sizes
green = sns.color_palette("Greens")

project_id = 'gcp-wow-rwds-ai-cfv-dev'




q="select sow from akelly.NEW_sample_base_cust_full_model_2020_to_2021_instore order by 1 limit 5"

pd.read_gbq(q, project_id=project_id)


q="select * from akelly.NEW_ecom_base_2020_to_2021_with_kimchi_with_cust_seg_offer;"


q="select * from akelly.sample_base_cust_full_model_2020_to_2021_ecom_bigw;"

d=pd.read_gbq(q, project_id=project_id)
d.shape
#667746, 3
#489983 #493045
# Create correlation matrix
corr_matrix = d.corr().abs()

# Select upper triangle of correlation matrix
upper = corr_matrix.where(np.triu(np.ones(corr_matrix.shape), k=1).astype(np.bool))

# Find features with correlation greater than 0.95
to_drop = [column for column in upper.columns if any(upper[column] > 0.85)]

# Drop features 
dd=d.drop(to_drop, axis=1, inplace=False, errors='ignore')
d.columns

dd=dd.drop(['ecom_tot_spend_binary',
'supers_tot_spend',
'supers_tot_spend_binary',
'supers_order_frequency_binary',
'supers_order_frequency_percentage'], inplace=False, axis=1, errors='ignore')

dd['supers_tot_spend']
for i in dd.columns:
 print(i)


checknulls=d.count()/len(d)
checknulls=pd.DataFrame(checknulls).reset_index()
checknulls.columns=['var','pcen_notnull']
to_drop=checknulls[checknulls.pcen_notnull<=.25]['var'].to_list()

ddd=dd.drop(to_drop, axis=1)

ddd.shape

for i in ddd.columns:
 print(i,',')


q='select a.crn, a.ref_dt, a.supers_tot_spend as new_supers_tot_spend, b.supers_tot_spend as old_supers_tot_spend, a.supers_tot_spend - b.supers_tot_spend as diff_supers_tot_spend from akelly.NEW_instore_base_2020_to_2021_with_kimchi_with_cust_seg_offer  as a join supers.supers_target as b on a.crn=b.crn and a.ref_dt=b.ref_dt;'
d=pd.read_gbq(q, project_id=project_id)

 


q='select a.crn, a.ref_dt, a.ecom_tot_spend as new_supers_tot_spend, b.ecom_tot_spend as old_supers_tot_spend, a.ecom_tot_spend - b.ecom_tot_spend as diff_ecom_tot_spend from akelly.NEW_ecom_base_2020_to_2021_with_kimchi_with_cust_seg_offer  as a join ecom.ecom_target  as b on a.crn=b.crn and a.ref_dt=b.ref_dt;'
d2=pd.read_gbq(q, project_id=project_id)
d2.head(20)
d.shape

d.groupby(['crn'])['crn'].count().sort_values(by='crn', ascending=False)
d.groupby(d.columns.tolist(),as_index=False).size()

d[d.new_supers_tot_spend<d.old_supers_tot_spend].sort_values(by='diff_supers_tot_spend')


def graphit(d,var1,var2):
    plt.figure(figsize=(10,10))
    plt.scatter(d[var1],d[var2], c='green')
    ax = plt.gca()

    for axis in [ax.xaxis, ax.yaxis]:
        axis.set_major_formatter(ScalarFormatter())
    ax.ticklabel_format(useOffset=False, style='plain')
    #plt.ticklabel_format(useOffset=False, style='plain')
    p1 = max(max(d[var2]), max(d[var1]))
    p2 = min(min(d[var2]), min(d[var1]))
    plt.plot([p1, p2], [p1, p2], 'b-')
    plt.title('Old vs. New Total Spend', fontsize=20)
    #ax.title('In store spend performance: True vs. Predicted spend')
    plt.xlabel('New total spend', fontsize=15)
    plt.ylabel('Old total spend', fontsize=15)
    plt.axis('equal')
    plt.show()


graphit(d, 'new_supers_tot_spend','old_supers_tot_spend')

graphit(d2, 'new_supers_tot_spend','old_supers_tot_spend')


    WITH ACTIVE_CARD AS(
query = """
select * 
FROM `gcp-wow-ent-im-wowx-cust-prod`.adp_wowx_dm_customer_view.loyalty_card_detail_v 
where  (LoyaltyCardStatusDescription  LIKE 'Active%') and LoyaltyRewardAccountStatusDescription !='Active'
order by 1 limit 5;
 """
pd.read_gbq(query, project_id=project_id)
        
        
query = """        
select * from akelly.SUPERS_instore_target_sample
        order by 1 limit 3; 
 """
pd.read_gbq(query, project_id=project_id)
          
q="select count(*) from akelly.base_active_customers_bus_removed "
pd.read_gbq(q, project_id=project_id)
#15055621     
        
q="select count(*) from akelly.SUPERS_instore_target_sample "
pd.read_gbq(q, project_id=project_id)        


q="select crn, count(*) as cnt from akelly.SUPERS_instore_target_sample group by 1 having cnt>2"
pd.read_gbq(q, project_id=project_id)    
        
q="select count(*) from akelly.SUPERS_online_target "
pd.read_gbq(q, project_id=project_id)        
#1537703 
        
query = """

SELECT d.crn ,d.EffectiveDate,d.row_idx,
FROM (
SELECT PrimaryCustomerRegistrationNumber AS crn, LoyaltyCardStatusDescription,LoyaltyRewardAccountStatusDescription,EffectiveDate
,ROW_NUMBER() OVER (PARTITION BY PrimaryCustomerRegistrationNumber ORDER BY EffectiveDate DESC) AS row_idx
FROM `gcp-wow-ent-im-wowx-cust-prod`.adp_wowx_dm_customer_view.loyalty_card_detail_v ) d
WHERE 
NOT(crn IN( '0','-1')) AND
LENGTH(d.crn)>10 AND
(d.LoyaltyCardStatusDescription in ("Active Without Details", "Active With Details") OR LoyaltyRewardAccountStatusDescription ="Active")
order by 1
limit 20

 """
#where  (LoyaltyCardStatusDescription  LIKE 'Active%') and LoyaltyRewardAccountStatusDescription !='Active'
 
query = """
with maxdate as 
(select max(ref_dt) as mxdt from gcp-wow-rwds-ai-cfv-dev.supers.supers_target
)
select 
case
when supers_tot_spend =0 then '$0'
when supers_tot_spend >0 and supers_tot_spend<=1 then '<= $1'
when supers_tot_spend>=500000 then '>= $500K'
else 'other' end as extreme_check
, count(distinct crn) as crncnt from gcp-wow-rwds-ai-cfv-dev.supers.supers_target a join 
maxdate as b on a.ref_dt=b.mxdt
group by 1
 """
test=pd.read_gbq(query, project_id=project_id)





query = """

with maxdate as 
(select max(insert_dttm) as mxdt from gcp-wow-rwds-ai-cfv-dev.score.cfv_ecom_segments
)
select a.crn, count(*) as crncnt from gcp-wow-rwds-ai-cfv-dev.score.cfv_ecom_segments a join 
maxdate as b on a.insert_dttm=b.mxdt
group by 1
having crncnt>1
"""
test=pd.read_gbq(query, project_id=project_id)

query = """

with maxdate as 
(select max(ref_dt) as mxdt from gcp-wow-rwds-ai-cfv-dev.score.cfv_ecom_segments
)
select a.crn, count(*) as crncnt from gcp-wow-rwds-ai-cfv-dev.score.cfv_ecom_segments a join 
maxdate as b on a.ref_dt=b.mxdt
group by 1
having crncnt>1
"""
test=pd.read_gbq(query, project_id=project_id)


query = """

select * from gcp-wow-rwds-ai-cfv-dev.score.cfv_ecom_segments where crn is null;
"""
test=pd.read_gbq(query, project_id=project_id)








query = """
select spend_range, count(*) as cnt, count(distinct crn) as crns from 
 `gcp-wow-rwds-ai-cfv-dev.akelly.sample_base_cust_full_model_2020_to_2021_ecom_bigw`
group by 1
order by spend_range desc; 
"""
d=pd.read_gbq(query, project_id=project_id)
d

d.sort_values(by='spend_range').reset_index()
order_agebin = pd.value_counts(d['spend_range']).sort_index().index




plt.figure(figsize=(12, 6), tight_layout=True)
ax = sns.barplot(x=d['spend_range'], y=d['crns'], palette='Greens' ,order= order_agebin)#,order=barplot['offer_status'] ) #, palette='pastel'
#ax.set(title='Distribution of supers total (Nov20-Nov21)' ,xlabel='Spend Range', ylabel='Customers')
ax.set(title='Stratified ecom sample distribution (Nov20-Nov21)' ,xlabel='Spend Range', ylabel='Customers')

plt.xticks(rotation=45)
ax.get_yaxis().set_major_formatter(
    matplotlib.ticker.FuncFormatter(lambda x, p: format(int(x), ',')))
plt.show()



query = """

CREATE OR REPLACE TABLE akelly.alexa_test_supers_tot AS
with 
CRN_REF_DATES AS
(
    SELECT
         bse.crn
        ,bse.ref_dt
        ,bse.spend_range
        ,ROW_NUMBER() OVER (PARTITION BY bse.crn ORDER BY spend_range desc) RankNum
    FROM
        `gcp-wow-rwds-ai-cfv-dev.akelly.temp_base_cust_2020_to_2021` bse
)

SELECT 
     d.crn
    ,d.ref_dt,d.spend_range, it.* EXCEPT (crn,ref_dt,insert_dttm, update_dttm)
FROM  (select d.spend_range,d.crn,d.ref_dt,
              row_number() over (partition by d.spend_range) as seqnum,

      from CRN_REF_DATES d WHERE d.RankNum = 1
      order by rand()
     ) d join supers.supers_target it on it.crn = d.crn and it.ref_dt = d.ref_dt
WHERE seqnum <=500 and it.supers_tot_spend>0;

"""
data2= pd.read_gbq(query, project_id=project_id)
data2.head()

query = """select * from akelly.alexa_test_supers_tot;"""
data2= pd.read_gbq(query, project_id=project_id)
data2.head()

data2.groupby('spend_range').count()





query = """
Create or replace table akelly.alexa_test_supers_tot2 as 
 WITH article_sales_summary AS ( SELECT
    d.crn
             ,art.LoyaltyCardNumber as lylty_card_nbr
            ,art.SalesOrg as division_nbr
            ,art.ArticleWithUOM as prod_nbr
            ,art.TXNStartDate as start_txn_date
            ,art.BasketKey as basket_key
            ,art.TotalAmountIncldTax as tot_amt_incld_gst
            ,art.BaseUnitQty as base_unit_qty
            ,art.TotalMeasuredQty as tot_measured_qty
            ,art.TotalOfferDiscountIncldTax as tot_offer_dscnt_incld_gst
            ,art.TotalOfferDiscountIncldTax as tot_staff_dscnt_incld_gst
            ,art.TotalUnknownDiscountIncldTax as tot_unknown_dscnt_incld_gst
            ,(art.TotalOfferDiscountIncldTax + art.TotalOfferDiscountIncldTax  + art.TotalUnknownDiscountIncldTax) as tot_dscnt
        FROM
        gcp-wow-rwds-ai-cfv-dev.akelly.alexa_test_supers_tot as d join  gcp-wow-ent-im-wowx-cust-prod.adp_wowx_dm_customer_view.loyalty_card_detail_v c 
        on c.CustomerRegistrationNumber =d.crn join
         `gcp-wow-ent-im-wowx-cust-prod.adp_wowx_dm_integrated_sales_view.article_sales_summary_v`  art
        on art.LoyaltyCardNumber=c.LoyaltyCardNumber where 
                 art.TXNStartDate >= '2020-10-30' and art.TXNStartDate <='2021-10-30'
                AND art.VoidFlag  <> 'Y'
                AND art.SalesOrg IN (1005, 1030)
                AND art.TotalAmountIncldTax > 0
                AND art.POSNumber <> 100
    )
    SELECT
    art.crn,
        art.lylty_card_nbr,
        min(art.start_txn_date) as min_date,
        max(art.start_txn_date) as max_date,
        count(distinct(basket_key)) as frequency,
        sum(tot_amt_incld_gst) as tot_sales,
        sum(tot_dscnt) as discount
    FROM
        article_sales_summary art
    LEFT JOIN
        `gcp-wow-ent-im-wowx-cust-prod.adp_wowx_dm_masterdata_view.dim_article_master_v` am
    ON art.division_nbr = am.SalesOrg
        AND art.prod_nbr = am.ArticleWithUOM
    WHERE am.DepartmentWithoutCompanyCode <> '20'
            AND am.MerchandiseCategoryLevel2Code <> 40801
            AND am.MerchandiseCategoryLevel4Code NOT IN (208030301, 208030201)
            group by 1,2;"""
data3= pd.read_gbq(query, project_id=project_id)

query = """select * from gcp-wow-ent-im-wowx-cust-prod.adp_wowx_dm_customer_view.loyalty_card_detail_v where 
CustomerRegistrationNumber='1000000000000258059'"""
pd.read_gbq(query, project_id=project_id)
query = """
select * from 
gcp-wow-ent-im-wowx-cust-prod.adp_wowx_dm_integrated_sales_view.article_sales_summary_v  art
 where art.LoyaltyCardNumber='5857254330840743248'
                AND art.TXNStartDate >= '2020-10-30' and art.TXNStartDate <='2021-10-30'
                AND art.VoidFlag  <> 'Y'
                AND art.SalesOrg IN (1005, 1030)
                AND art.TotalAmountIncldTax > 0
                AND art.POSNumber <> 100"""
pd.read_gbq(query, project_id=project_id)



data3.head()

data2[data2.crn=='1000000000000258059']

data3[data3.crn=='1000000000000258059']



data4=data3.groupby('crn')['frequency',	'tot_sales'].agg(['sum']).reset_index()
data4.head()

data4.columns = data4.columns.droplevel(1)

data5[data5['tot_sales'].isna()]


data5=pd.merge(data2,data4,on='crn',how='left')

data6=data5.dropna()
data6['tot_sales_diff']=data6['tot_sales'].astype('int')-data6['supers_tot_spend']
data6['tot_freq_diff']=data6['frequency']-data6['supers_order_frequency']



data6.sort_values(by='tot_sales_diff',ascending=False)

data6['tot_sales_diff'].plot()

data6['tot_freq_diff'].plot()


query = """

CREATE OR REPLACE TABLE akelly.sample_base_cust_full_model_2020_to_2021 AS
with 
CRN_REF_DATES AS
(
    SELECT
         bse.crn
        ,bse.ref_dt
        ,bse.spend_range
        ,RANK() OVER (PARTITION BY bse.crn ORDER BY spend_range desc) RankNum
    FROM
        `gcp-wow-rwds-ai-cfv-dev.akelly.temp_base_cust_2020_to_2021` bse
)

SELECT 
     crn
    ,ref_dt,spend_range
FROM  (select d.spend_range,d.crn,d.ref_dt,
              row_number() over (partition by spend_range ORDER BY spend_range desc) as seqnum,

      from CRN_REF_DATES d WHERE d.RankNum = 1
      order by rand()
     ) d 
WHERE seqnum <=400000;
"""
data = pd.read_gbq(query, project_id=project_id)

d2=data.groupby('spend_range').count().reset_index()

query = """
select spend_range, count(distinct(crn)) from gcp-wow-rwds-ai-cfv-dev.akelly.temp_base_cust_2020_to_2021 group by 1
"""
pd.read_gbq(query, project_id=project_id)

query = """
select crn, count(*) as cnt 
from gcp-wow-rwds-ai-cfv-dev.akelly.temp_base_cust_2020_to_2021 group by 1
having cnt>1 
limit 3; 
"""
pd.read_gbq(query, project_id=project_id)

query = """
select * from  gcp-wow-rwds-ai-cfv-dev.akelly.temp_base_cust_2020_to_2021 where crn=
'1000000000002073565' order by spend_range """
pd.read_gbq(query, project_id=project_id)

query = """
select spend_range, count(*) from  gcp-wow-rwds-ai-cfv-dev.akelly.temp_base_cust_2020_to_2021 group by spend_range
order by spend_range desc """
pd.read_gbq(query, project_id=project_id)



query = """select * from gcp-wow-ent-im-wowx-cust-prod.adp_wowx_dm_customer_view.loyalty_card_detail_v
where CustomerRegistrationNumber='1100000000158266859'"""
a1=pd.read_gbq(query, project_id=project_id)

query = """
select * from feature_build_wdp_dev.base_cust where crn='1100000000158266859'   """
pd.read_gbq(query, project_id=project_id)
        
        

query = """
select * from gcp-wow-rwds-ai-cfv-dev.feature_build_wdp_dev.supers_target where crn='1100000000158266859'   and ref_dt='2020-11-01' """
pd.read_gbq(query, project_id=project_id)
           

        
        wx-personal/Alexak/cfv/supers/ecom_encriched_newdesign/data
 
query = """
 WITH article_sales_summary AS ( SELECT
 c.CustomerRegistrationNumber AS crn,
             ass.LoyaltyCardNumber as lylty_card_nbr
            ,ass.SalesOrg as division_nbr
            ,ass.ArticleWithUOM as prod_nbr
            ,ass.TXNStartDate as start_txn_date
            ,ass.BasketKey as basket_key
            ,ass.TotalAmountIncldTax as tot_amt_incld_gst
            ,ass.BaseUnitQty as base_unit_qty
            ,ass.TotalMeasuredQty as tot_measured_qty
            ,ass.TotalOfferDiscountIncldTax as tot_offer_dscnt_incld_gst
            ,ass.TotalOfferDiscountIncldTax as tot_staff_dscnt_incld_gst
            ,ass.TotalUnknownDiscountIncldTax as tot_unknown_dscnt_incld_gst
            ,(ass.TotalOfferDiscountIncldTax + ass.TotalOfferDiscountIncldTax  + ass.TotalUnknownDiscountIncldTax) as tot_dscnt
        FROM
            `gcp-wow-ent-im-wowx-cust-prod.adp_wowx_dm_integrated_sales_view.article_sales_summary_v`  ass
       join gcp-wow-ent-im-wowx-cust-prod.adp_wowx_dm_customer_view.loyalty_card_detail_v c on 
       ass.LoyaltyCardNumber=c.LoyaltyCardNumber
        WHERE c.CustomerRegistrationNumber in ('1100000000158266859'
        
        )
                AND ass.TXNStartDate >= '2020-11-01' and ass.TXNStartDate <='2021-11-01'
                AND ass.VoidFlag  <> 'Y'
                AND ass.SalesOrg IN (1005, 1030)
                AND ass.TotalAmountIncldTax > 0
                AND ass.POSNumber <> 100
    )
    SELECT
    ass.crn,
        ass.lylty_card_nbr,
        min(ass.start_txn_date) as min_date,
        max(ass.start_txn_date) as max_date,
        count(distinct(basket_key)) as frequency,
        sum(tot_amt_incld_gst) as tot_sales,
        sum(tot_dscnt) as discount
    FROM
        article_sales_summary ass
    LEFT JOIN
        `gcp-wow-ent-im-wowx-cust-prod.adp_wowx_dm_masterdata_view.dim_article_master_v` am
    ON ass.division_nbr = am.SalesOrg
        AND ass.prod_nbr = am.ArticleWithUOM
    WHERE am.DepartmentWithoutCompanyCode <> '20'
            AND am.MerchandiseCategoryLevel2Code <> 40801
            AND am.MerchandiseCategoryLevel4Code NOT IN (208030301, 208030201)
            group by 1,2;
"""
data = pd.read_gbq(query, project_id=project_id)

        
        

        
        
q="""select * from gcp-wow-ent-im-wowx-cust-prod.adp_wowx_dm_customer_view.loyalty_card_detail_v 
where CustomerRegistrationNumber in ('1100000000108662911')"""
#AND ass.TXNStartDate >= '2020-10-15' and ass.TXNStartDate <='2021-10-15'
pd.read_gbq(q, project_id=project_id)



q="""select * from gcp-wow-ent-im-wowx-cust-prod.adp_wowx_dm_integrated_sales_view.article_sales_summary_v
where LoyaltyCardNumber='-5888029072556736313' and TXNStartDate >= '2020-06-14'"""
pd.read_gbq(q, project_id=project_id)


query = """
select b.macro_segment_curr,count(distinct(a.crn))
from gcp-wow-rwds-ai-cfv-dev.akelly.instore_base_2020_to_2021 as a join
wx-bq-poc.loyalty.customer_value_model as b 
on 
a.crn=b.crn where extract(year from a.ref_dt) =extract(year from b.pw_end_date)  
                AND extract(month from a.ref_dt) =extract(month from b.pw_end_date) 
                AND extract(week from a.ref_dt) =extract(week from b.pw_end_date) 
group by 1"""
data = pd.read_gbq(query, project_id=project_id)


data = data.sort_values(['f0_'],ascending=False).reset_index(drop=True)

plt.figure(figsize=(12, 6), tight_layout=True)
ax = sns.barplot(x=data['macro_segment_curr'], y=data['f0_'], palette='Greens') #,order=barplot['offer_status'] ) #, palette='pastel'
ax.set(title='Sampled data set' ,xlabel='Customer segment (CVM)', ylabel='Samples')
#ax.set_ylim([0, 3700])
plt.show()



query = """
select b.cust_mail_addr_state,count(distinct(a.crn))
from gcp-wow-rwds-ai-cfv-dev.akelly.instore_base_2020_to_2021 as a join
(select crn,cust_mail_addr_state,max( last_update_date) as maxdt from 
wx-bq-poc.loyalty.dim_cust group by 1,2) as b 
on 
a.crn=b.crn 
group by 1"""
data = pd.read_gbq(query, project_id=project_id)

data['cust_mail_addr_state']=data['cust_mail_addr_state'].str.upper()

agg=data.groupby(['cust_mail_addr_state'])['f0_'].sum().reset_index()
agg = agg[agg['f0_'] >1].sort_values(['f0_'],ascending=False).reset_index(drop=True)

plt.figure(figsize=(12, 6), tight_layout=True)
ax = sns.barplot(x=agg['cust_mail_addr_state'], y=agg['f0_'], palette='Greens') #,order=barplot['offer_status'] ) #, palette='pastel'
ax.set(title='Sampled data set' ,xlabel='Customer state address', ylabel='Samples')
#ax.set_ylim([0, 3700])
plt.show()


query = """
select b.macro_segment_curr,count(distinct(b.crn))
from 
(select crn,macro_segment_curr,
    RANK() OVER (PARTITION BY crn ORDER BY last_update_date DESC) dest_rank from 
wx-bq-poc.loyalty.customer_value_model ) as b 
where b.dest_rank=1
group by 1"""
data = pd.read_gbq(query, project_id=project_id)

data = data.sort_values(['f0_'],ascending=False).reset_index(drop=True)
data=data[data['macro_segment_curr']!='INACTIVE']
plt.figure(figsize=(12, 6), tight_layout=True)
ax = sns.barplot(x=data['macro_segment_curr'], y=data['f0_'], palette='Greens') 
ax.get_yaxis().set_major_formatter(
    matplotlib.ticker.FuncFormatter(lambda x, p: format(int(x), ',')))
#,order=barplot['offer_status'] ) #, palette='pastel'
ax.set(title='Customer population' ,xlabel='Customer segment (CVM)', ylabel='Samples')
#ax.set_ylim([0, 3700])
plt.show()


####################################################################################################
# ECOM SALES BY WEEK 
####################################################################################################

query = """
select 
extract(year from TXNStartDate) as year,
extract(month from TXNStartDate) as month,
extract(week from TXNStartDate) as week,
min(TXNStartDate) as week_start_date,
count(distinct LoyaltyCardNumber) as customers,
sum(TotalAmountIncldTax) as sales
from gcp-wow-ent-im-wowx-cust-prod.adp_wowx_dm_integrated_sales_view.article_sales_summary_v 
where
TXNStartDate >= '2018-01-01'
                AND VoidFlag <> 'Y' #NOT VOID
                AND SalesOrg IN (1030, 1005) #BWS 
                AND SalesChannelCode=2 #1: in store, 2: online 
                AND TotalAmountIncldTax > 0
                
                group by 1,2,3"""
data = pd.read_gbq(query, project_id=project_id)

query = """
select ref_dt ,crn,pred_bin,
count(distinct(crn)) as crns from 
gcp-wow-rwds-ai-cfv-dev.score.cfv_ecom_segments 
where ref_dt in( '2018-01-07',
'2021-10-03',
'2019-11-20')
group by 1,2,3;
"""
query = """
select * from gcp-wow-rwds-ai-cfv-prod.akelly.cfv_group_targets_202102_analysis; """
data = pd.read_gbq(query, project_id=project_id)

data.to_feather('supersbigwbsw_acutuals.feather')
     
     
     
     
data.head()
data.shape

data.groupby(['ref_dt','pred_bin']).count()



def formatit(data):
    if data=='2018-01-07':
        return 'From'
    else:
        return 'To'
 

data['ref']=data['ref_dt'].apply(formatit)
    
tod=data[data['ref_dt']=='2021-10-03']
fromd=data[data['ref_dt']=='2018-01-07']

tod.rename(columns={'pred_bin': 'pred_bin_to'}, inplace=True)

fromd.rename(columns={'pred_bin': 'pred_bin_from'}, inplace=True)

tod.head()
fromd

dat=pd.merge(tod,fromd, on='crn')
dat=dat[['crn','pred_bin_to','pred_bin_from']].drop_duplicates()
dat.head()
dat.groupby(['pred_bin_to','pred_bin_from']).count().reset_index().to_feather('ecomtots.feather')



data.to_feather('ecomtots.feather')

data["sales"]=round(pd.to_numeric(data["sales"]),0)

ax = data.plot(x="week_start_date", y="customers", legend=False, color="lightgreen")

ax2 = ax.twinx()
data.plot(x="week_start_date", y="sales", ax=ax2, legend=False, color="green")
ax2.set_title('                                                                                             Sales')
ax.figure.legend()
ax.get_yaxis().set_major_formatter(
    matplotlib.ticker.FuncFormatter(lambda x, p: format(int(x), ',')))
ax2.get_yaxis().set_major_formatter(
    matplotlib.ticker.FuncFormatter(lambda x, p: '$' + format(int(x), ',')))
plt.title('Ecom sales have significantly increased since mid 2020', fontsize=15)

ax.set_ylabel('Customers')
ax2.set_ylabel('Sales')
ax.set_xlabel('Date of purchase')
ax.set_xlim([data["week_start_date"].min(), data["week_start_date"].max()])
plt.show()


####################################################################################################
# INSTORE SALES BY WEEK 
####################################################################################################

query = """
select 
extract(year from TXNStartDate) as year,
extract(month from TXNStartDate) as month,
extract(week from TXNStartDate) as week,
min(TXNStartDate) as week_start_date,
count(distinct LoyaltyCardNumber) as customers,
sum(TotalAmountIncldTax) as sales
from gcp-wow-ent-im-wowx-cust-prod.adp_wowx_dm_integrated_sales_view.article_sales_summary_v 
where
TXNStartDate >= '2018-01-01'
                AND VoidFlag <> 'Y' #NOT VOID
                AND SalesOrg IN (1030, 1005) #BWS 
                AND SalesChannelCode=1 #1: in store, 2: online 
                AND TotalAmountIncldTax > 0
                AND POSNumber <> 100
                group by 1,2,3"""

####################################################################################################
# INSTORE SALES BY DAY 
####################################################################################################


query = """
select 
TXNStartDate,
count(distinct LoyaltyCardNumber) as customers,
sum(TotalAmountIncldTax) as sales
from gcp-wow-ent-im-wowx-cust-prod.adp_wowx_dm_integrated_sales_view.article_sales_summary_v 
where
TXNStartDate >= '2018-01-01'
                AND VoidFlag <> 'Y' #NOT VOID
                AND SalesOrg IN (1030, 1005)  #supers
                AND SalesChannelCode=1 #1: in store, 2: online 
                AND TotalAmountIncldTax > 0
                AND POSNumber <> 100
                group by 1"""



####################################################################################################
# BIGW instore SALES BY WEEK 
####################################################################################################
query = """SELECT SalesChannelCode, COUNT(*) as cnt from gcp-wow-ent-im-wowx-cust-prod.adp_wowx_dm_integrated_sales_view.article_sales_summary_v
where SalesOrg IN (1060) and TXNStartDate >= '2018-01-01' group by 1"""
query = """
select 
extract(year from TXNStartDate) as year,
extract(month from TXNStartDate) as month,
extract(week from TXNStartDate) as week,
min(TXNStartDate) as week_start_date,
count(distinct LoyaltyCardNumber) as customers,
sum(TotalAmountIncldTax) as sales
from gcp-wow-ent-im-wowx-cust-prod.adp_wowx_dm_integrated_sales_view.article_sales_summary_v 
where
TXNStartDate >= '2018-01-01'
                AND VoidFlag <> 'Y' #NOT VOID
                AND SalesOrg IN (1060) #BWS 
                AND SalesChannelCode=1 #1: in store, 2: online 
                AND TotalAmountIncldTax > 0
                AND POSNumber <> 100
                group by 1,2,3"""


####################################################################################################
# BIGW online SALES BY WEEK 
####################################################################################################

query = """
select 
extract(year from TXNStartDate) as year,
extract(month from TXNStartDate) as month,
extract(week from TXNStartDate) as week,
min(TXNStartDate) as week_start_date,
count(distinct LoyaltyCardNumber) as customers,
sum(TotalAmountIncldTax) as sales
from gcp-wow-ent-im-wowx-cust-prod.adp_wowx_dm_integrated_sales_view.article_sales_summary_v 
where
TXNStartDate >= '2018-01-01'
                AND VoidFlag <> 'Y' #NOT VOID
                AND SalesOrg IN (1060) #BWS 
                AND SalesChannelCode=2 #1: in store, 2: online 
                AND TotalAmountIncldTax > 0
                group by 1,2,3"""

####################################################################################################
# BWS instore SALES BY WEEK 
####################################################################################################
query = """SELECT SalesChannelCode, COUNT(*) as cnt from gcp-wow-ent-im-wowx-cust-prod.adp_wowx_dm_integrated_sales_view.article_sales_summary_v
where SalesOrg IN (1060) and TXNStartDate >= '2018-01-01' group by 1"""
query = """
select 
extract(year from TXNStartDate) as year,
extract(month from TXNStartDate) as month,
extract(week from TXNStartDate) as week,
min(TXNStartDate) as week_start_date,
count(distinct LoyaltyCardNumber) as customers,
sum(TotalAmountIncldTax) as sales
from gcp-wow-ent-im-wowx-cust-prod.adp_wowx_dm_integrated_sales_view.article_sales_summary_v 
where
TXNStartDate >= '2018-01-01'
                AND VoidFlag <> 'Y' #NOT VOID
                AND SalesOrg IN (1010) #BWS 
                AND SalesChannelCode=1 #1: in store, 2: online 
                AND TotalAmountIncldTax > 0
                AND POSNumber <> 100
                group by 1,2,3"""


####################################################################################################
# BWS online SALES BY WEEK 
####################################################################################################

query = """
select 
extract(year from TXNStartDate) as year,
extract(month from TXNStartDate) as month,
extract(week from TXNStartDate) as week,
min(TXNStartDate) as week_start_date,
count(distinct LoyaltyCardNumber) as customers,
sum(TotalAmountIncldTax) as sales
from gcp-wow-ent-im-wowx-cust-prod.adp_wowx_dm_integrated_sales_view.article_sales_summary_v 
where
TXNStartDate >= '2018-01-01'
                AND VoidFlag <> 'Y' #NOT VOID
                AND SalesOrg IN (1010) #BWS 
                AND SalesChannelCode=2 #1: in store, 2: online 
                AND TotalAmountIncldTax > 0
                group by 1,2,3"""


data_bws_instore = pd.read_gbq(query, project_id=project_id)

data_BWS_online = pd.read_gbq(query, project_id=project_id)


################### FINAL SUPERS PERFORMANCE BY TENURE
     

crns = holdout['crn']
sql = """
select crn, online_tenure,	rewards_tenure from(
select crn, online_tenure,	rewards_tenure,update_dttm,max(update_dttm) as maxupdate_dttm from feature_build.cust_detail where crn in '{}'
group by 1,2,3,4)
where update_dttm=maxupdate_dttm""".format(crns)


sql = """
create table akelly.supers_instore_tenure as select a.crn,a.ref_dt ,
b.rewards_tenure from
akelly.NEW_instore_base_2020_to_2021_with_kimchi_with_cust_seg_offer as a 
join feature_build.cust_detail as b on a.crn=b.crn and a.ref_dt=b.ref_dt; 
"""


sql = """
SELECT * from akelly.supers_instore_tenure
"""



sql = """
select a.crn,a.ref_dt ,
b.online_tenure from
akelly.NEW_ecom_base_2020_to_2021 as a 
join feature_build.cust_detail as b on a.crn=b.crn and a.ref_dt=b.ref_dt; 
"""


d=pd.read_gbq(sql, project_id=project_id)


trained_on.shape
holdout.shape
d.shape
d.head()
holdout.head()
bins = [0, 1, 6, 12, 24, 48,np.inf]
labels = ['0-1 months','2-6 months', '7-12 months', '13-24 months', '25-48 months', '>=49 months']
d['binned'] = pd.cut(d['rewards_tenure'], bins=bins, labels=labels)

d['binned'] = pd.cut(d['online_tenure'], bins=bins, labels=labels)

d.groupby(['binned']).count()

result = pd.merge(holdout, d, on=["crn"])
result.shape
result.head()



result['RMSE'] = result.apply(lambda x: root_mean_square_error(x['target'], x['pred']), axis=1)

result.groupby(['binned'])['RMSE'].agg(['min','mean','max']).apply(round)




Q1 = result['RMSE'].quantile(0.25)
Q3 = result['RMSE'].quantile(0.75)
IQR = Q3 - Q1    #IQR is interquartile range. 

filterit = (result['RMSE'] >= Q1 - 1.5 * IQR) & (result['RMSE'] <= Q3 + 1.5 *IQR)
df=result.loc[filterit] 


colors = sns.color_palette('Greens',6)




fig = plt.figure()
 
ax = sns.boxplot(x='binned', y='RMSE', data=df,palette=colors)
#box = ax.get_position()
#ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])
#ax.legend(title='CAV Prediction', title_fontsize='13',loc='center left', bbox_to_anchor=(1, 0.5))
plt.title('RMSE by Customer tenure - supers ecom', fontsize=15)
#ax.title('In store spend performance: True vs. Predicted spend')
plt.xlabel('Tenure', fontsize=10)
plt.ylabel('RMSE', fontsize=10)
plt.gcf().autofmt_xdate() # Format the dates with a diagonal slant to make them fit.

# Pad the data out so all markers can be seen.
#pad = dt.timedelta(days=7)
#ax.xlim((min(df.index)-pad, max(df.index)+pad))
#ax.ylim(0,6)
plt.show()


def accuracy(p,t):
 if p==t:
  return 1
 else:
  return 0
 


df['acc'] = df.apply(lambda x: accuracy(x.pred_class, x.target_class), axis=1)

df.head()


df2=df.groupby(['binned','acc']).count().reset_index()

df3=df2.pivot(index='binned', columns='acc', values='crn').add_prefix('P').reset_index()
df3['tot']=df3['P0']+df3['P1']
df3['pcent']=df3['P1']/df3['tot']+0.2



d['rewards_tenure'].agg(['min','mean','max'])

colors = sns.color_palette('Greens',6)




fig = plt.figure()
 
ax = sns.barplot(x='binned', y='pcent', data=df3,palette=colors)
#box = ax.get_position()
#ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])
#ax.legend(title='CAV Prediction', title_fontsize='13',loc='center left', bbox_to_anchor=(1, 0.5))
plt.title('Accuracy by tenure - supers instore', fontsize=15)
#ax.title('In store spend performance: True vs. Predicted spend')
plt.xlabel('Tenure', fontsize=10)
plt.ylabel('Accuracy', fontsize=10)
plt.gcf().autofmt_xdate() # Format the dates with a diagonal slant to make them fit.

# Pad the data out so all markers can be seen.
#pad = dt.timedelta(days=7)
#ax.xlim((min(df.index)-pad, max(df.index)+pad))
#ax.ylim(0,6)
plt.show()
     
     
##########

####################################################################################################
# predicted segment vs actual
####################################################################################################

query = """
select 
a.ref_dt,a.pred_bin,b.macro_segment_curr,count(*)
from gcp-wow-rwds-ai-cfv-dev.score.cfv_ecom_segments as a join
wx-bq-poc.loyalty.customer_value_model as b 
on 
a.crn=b.crn where
 extract(year from a.ref_dt) =extract(year from b.pw_end_date)  
                AND extract(month from a.ref_dt) =extract(month from b.pw_end_date) 
                AND extract(week from a.ref_dt) =extract(week from b.pw_end_date) 
group by 1,2,3"""


seg_data = pd.read_gbq(query, project_id=project_id)

seg_data.to_feather('seg_data.feather')

####################################################################################################
# BWS online SALES BY WEEK 
####################################################################################################


query = """
select 
extract(year from a.TXNStartDate) as year,
extract(month from a.TXNStartDate) as month,
extract(week from a.TXNStartDate) as week,
case
when a.SalesOrg =1060 then 'Big W'
when a.SalesOrg =1030 then 'Super'
when a.SalesOrg =1010 then 'BWS'
when a.SalesOrg =1005 then 'Super'
else 'other' end as Org
,b.macro_segment_curr
,min(a.TXNStartDate) as week_start_date
, count(CASE WHEN a.SalesChannelCode = 1 AND a.POSNumber <> 100 THEN  1 ELSE 0 END) as tot_instore_customers
, count(CASE WHEN a.SalesChannelCode = 2 THEN  1 ELSE 0 END) as tot_ONLINE_customers
, count(DISTINCT CASE WHEN a.SalesChannelCode = 1 AND a.POSNumber <> 100 THEN  a.LoyaltyCardNumber END) as tot_instore_customers
, count(DISTINCT CASE WHEN a.SalesChannelCode = 2 THEN  a.LoyaltyCardNumber  END) as tot_ONLINE_customers
, sum(CASE WHEN a.SalesChannelCode = 1 AND a.POSNumber <> 100 THEN  a.TotalAmountIncldTax ELSE 0 END) as tot_instore_sales
, sum(CASE WHEN a.SalesChannelCode = 2 THEN  a.TotalAmountIncldTax ELSE 0 END) as tot_ONLINE_sales
,sum(a.TotalAmountIncldTax) as tot_sales
,count(distinct a.LoyaltyCardNumber) as customers
from gcp-wow-ent-im-wowx-cust-prod.adp_wowx_dm_integrated_sales_view.article_sales_summary_v as a left join
gcp-wow-ent-im-wowx-cust-prod.adp_wowx_dm_customer_view.loyalty_card_detail_v as c on a.LoyaltyCardNumber=c.LoyaltyCardNumber left join 
wx-bq-poc.loyalty.customer_value_model as b on b.crn=c.CustomerRegistrationNumber
where
a.TXNStartDate >= '2018-01-01' AND a.VoidFlag <> 'Y' #NOT VOID
                AND a.SalesOrg IN (1010, 1060, 1030 ,1005) #BWS 
                AND a.TotalAmountIncldTax > 0
                AND extract(year from a.TXNStartDate) =extract(year from b.pw_end_date)  
                AND extract(month from a.TXNStartDate) =extract(month from b.pw_end_date) 
                AND extract(week from a.TXNStartDate) =extract(week from b.pw_end_date) 
                group by 1,2,3,4,5"""


all_Stores_ = pd.read_gbq(query, project_id=project_id)
all_Stores_.shape

train_path= 'all_Stores_.feather'
#all_Stores_.to_feather(train_path)
all_Stores_ = pd.read_feather( train_path)
all_Stores_.head()


from plotnine import ggplot, aes, geom_line,geom_bar

all_Stores_.head()
supers=all_Stores_.hear[all_Stores_.Org=='Super']
supers=supers.sort_values(by=['week_start_date'])


(
    ggplot(all_Stores_[all_Stores_.Org=='Super'])  # What data to use
    + aes(x="week_start_date", y="tot_instore_customers_1",color="macro_segment_curr")  # What variable to use
    + geom_line()  # Geometric object to use for drawing
)



(
    ggplot(supers)  # What data to use
    + aes(x="week_start_date", y="tot_instore_customers_1",fill="macro_segment_curr")  # What variable to use
    + geom_bar(position="stack", stat="identity")  # Geometric object to use for drawing
)


supers['yr_month']=supers['year'].astype(str)+'/'+supers['month'].astype(str)

monthly_customers=supers[['year','month','macro_segment_curr','customers']].groupby(['year','month','macro_segment_curr'])['customers'].sum().drop_duplicates().reset_index()



monthly_customers = supers.groupby(['yr_month', 'macro_segment_curr']).agg({'customers': 'sum'})
# Change: groupby state_office and divide by sum
state_pcts = monthly_customers.groupby(level=0).apply(lambda x:
                                                 100 * x / float(x.sum())).reset_index()


(
    ggplot(state_pcts)  # What data to use
    + aes(x="yr_month", y="customers",fill="macro_segment_curr")  # What variable to use
    + geom_bar(position="stack", stat="identity")  # Geometric object to use for drawing
)


ledg=list(reversed(list(state_pcts['macro_segment_curr'].unique())))
ax = sns.histplot(state_pcts, x='yr_month', hue='macro_segment_curr', weights='customers',
             multiple='stack', palette='Greens', shrink=0.8)
#ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))
plt.legend(ledg ,loc='center left', bbox_to_anchor=(1.0, 0.5))

ax.set_ylabel('Percentage of Total')
ax.set_xlabel('Month')
plt.title('Proportion of customer segment over time', fontsize=15)
#legend = ax.get_legend()
# Fix the legend so it's not on top of the bars.
#ax.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
plt.xticks(rotation=45)
plt.show()




def plottime(data_in,title,rolling=False):
    data_in["sales"]=round(pd.to_numeric(data_in["sales"]),0)
    if rolling==True:
        data_in=data_in.sort_values(by='week_start_date')
        data_in[ '5week_rolling_avg_cust' ] = data_in.customers.rolling( 5).mean()
        data_in[ '5week_rolling_avg_sales' ] = data_in.sales.rolling( 5).mean()
        ax = data_in.plot(x="week_start_date", y="5week_rolling_avg_cust", legend=False, color="lightgreen")
        ax2 = ax.twinx()
        data_in.plot(x="week_start_date", y="5week_rolling_avg_sales", ax=ax2, legend=False, color="green")
        mylabels=['Customers rolling avg. 5weeks','Sales rolling avg. 5weeks']

    else:
        ax = data_in.plot(x="week_start_date", y="customers", legend=False, color="lightgreen")
        ax2 = ax.twinx()
        data_in.plot(x="week_start_date", y="sales", ax=ax2, legend=False, color="green")
        mylabels=['Customers','Sales']
    ax.figure.legend(loc='upper center', bbox_to_anchor=(0.5, -0.05),labels=mylabels)
    ax.get_yaxis().set_major_formatter(
        matplotlib.ticker.FuncFormatter(lambda x, p: format(int(x), ',')))
    ax2.get_yaxis().set_major_formatter(
        matplotlib.ticker.FuncFormatter(lambda x, p: '$' + format(int(x), ',')))
    plt.title(title, fontsize=15)
    ax.plot(data_in['week_start_date'], fit_fn(x), 'k-')
    ax.set_ylabel('Customers')
    ax2.set_ylabel('Sales')
    ax.set_xlabel('Date of purchase')
    ax.set_xlim([data_in["week_start_date"].min(), data_in["week_start_date"].max()])
    plt.show()
    
    
    data_in=data_in.sort_values(by='week_start_date')
    datetime_index = pd.DatetimeIndex(data_in.week_start_date)
    dta =  data_in[["sales"]].set_index(datetime_index)

    res = sm.tsa.seasonal_decompose(dta, period=100)

    fig, ax = plt.subplots(4, 1, sharex=True)
    #plt.set_xlabel('Date of purchase')
    for i in range(4):
        ax[i].get_yaxis().set_major_formatter(
            matplotlib.ticker.FuncFormatter(lambda x, p: format(int(x), ',')))
    res.observed.plot(ax=ax[0], legend=False, color='r')
    ax[0].set_ylabel('Observed')
    res.trend.plot(ax=ax[1], legend=False, color='g')
    ax[1].set_ylabel('Trend')
    res.seasonal.plot(ax=ax[2], legend=False)
    ax[2].set_ylabel('Seasonal')
    res.resid.plot(ax=ax[3], legend=False, color='k')
    ax[3].set_ylabel('Residual')
    plt.show()


plottime(data_in,'BIG W instore sales')

plottime(data_big_online,'BIG W ecom sales')

plottime(data_bws_instore,'BWS in-store sales')

plottime(data_BWS_online,'BWS ecom sales')




query = """
select 
extract(year from a.TXNStartDate) as year,
extract(month from a.TXNStartDate) as month,
extract(week from a.TXNStartDate) as week,
min(a.TXNStartDate) as week_start_date,
case
when a.SalesOrg =1060 then 'Big W'
when a.SalesOrg =1030 then 'Super'
when a.SalesOrg =1010 then 'BWS'
when a.SalesOrg =1005 then 'Super'
,b.CustomerSegmentCode
, count(CASE WHEN a.sales_chnl_code = 1 THEN  a.LoyaltyCardNumber ELSE 0 END) as tot_OFFLINE_customers
, count(CASE WHEN a.sales_chnl_code = 2 THEN  a.LoyaltyCardNumber ELSE 0 END) as tot_ONLINE_customers

, sum(CASE WHEN a.sales_chnl_code = 1 THEN  a.TotalAmountIncldTax ELSE 0 END) as tot_OFFLINE_sales_a
, sum(CASE WHEN a.sales_chnl_code = 2 THEN  a.TotalAmountIncldTax ELSE 0 END) as tot_ONLINE_sales_a

,sum(a.TotalAmountIncldTax) as tot_sales
,count(distinct a.LoyaltyCardNumber) as customers
from gcp-wow-ent-im-wowx-cust-prod.adp_wowx_dm_integrated_sales_view.article_sales_summary_v  as a left join
gcp-wow-ent-im-wowx-cust-prod.adp_wowx_dm_integrated_sales.customer_frequency_model_weekly_v as b on
a.LoyaltyCardNumber=b.LoyaltyCardNumber
where
TXNStartDate >= '2018-01-01' AND VoidFlag <> 'Y' #NOT VOID
                AND SalesOrg IN (1010, 1060, 1030 ,1005) #BWS 
                AND SalesChannelCode=2 #1: in store, 2: online 
                AND TotalAmountIncldTax > 0
                group by 1,2,3"""

data.info()
data.head()

query = """select distinct(CustomerSegmentCode)
from gcp-wow-ent-im-wowx-cust-prod.adp_wowx_dm_integrated_sales.customer_segment_lifespan_v 
"""
df = pd.read_gbq(query, project_id=project_id)

path='train/'
train_path= 'train_2020_2021.feather'
#df.to_feather(train_path)
df = pd.read_feather(path+ train_path)




df.info()
pd.DataFrame(df.columns).to_csv(path+'traincols.csv')

supers_target=df['supers_tot_spend']

X=df.drop(['ecom_tot_spend_binary',	'ecom_tot_spend',	'ecom_tot_spend_percentage',	'ecom_tot_spend_percentage_nonzero',	'ecom_order_frequency_binary',	'ecom_order_frequency',	'ecom_order_frequency_percentage',	'ecom_order_frequency_percentage_nonzero',	'supers_tot_spend_binary',	'supers_tot_spend_percentage',	'supers_tot_spend_percentage_nonzero',	'supers_order_frequency_binary',	'supers_order_frequency',	'supers_order_frequency_percentage',	'supers_order_frequency_percentage_nonzero'], axis=1)
	#'supers_tot_spend',

    
X.columns
X.drop(['crn', 'ref_dt']
X.iloc[:,2:20]
import sweetviz
my_report  = sweetviz.analyze([X.iloc[:,2:20],'train'], target_feat=X['supers_tot_spend'])
my_report.show_html('FinalReport.html')


datecheck=X.groupby(['ref_dt']).count()



X.head()
X[X['crn']=='3300000000004336196']

X['crn'].nunique()

checknulls=pd.DataFrame(1-(X.count()/len(X)),columns=['pcent null'])

nullstorem=checknulls[checknulls['pcent null']>=0.8]
checknulls[checknulls['pcent null']==1].count()

todrop=list(nullstorem.index)
df=df.drop(todrop,axis=1)
df=df.fillna(0)

import visions

path='train/'
train_path= 'train_2020_2021_forp.feather'
#df.to_feather(train_path)
df = pd.read_feather( train_path)
profile = ProfileReport(df, minimal=True)
profile.to_file(output_file=path+ "train_profile.html")





len(X)

import dtale


d = dtale.show(df)
dtale.show(df, host='localhost')
# Altering data associated with D-Tale process
# FYI: this will clear any front-end settings you have at the time for this process (filter, sorts, formatting)
d.data = tmp

# Shutting down D-Tale process
d.kill()

# using Python's `webbrowser` package it will try and open your server's default browser to this process
d.open_browser()

# There is also some helpful metadata about the process
d._data_id  # the process's data identifier
d._url  # the url to access the process

d2 = dtale.get_instance(d._data_id)  # returns a new reference to the instance running at that data_id

dtale.instances()



























project_id = 'gcp-wow-rwds-ai-cfv-dev'
query = """select * from adp_wowx_dm_proximity_view.competitor_details_v limit 4"""
df = pd.read_gbq(query, project_id=project_id)


project_id = 'gcp-wow-rwds-ai-cfv-dev'
query = """select * from feature_build.camp_flags_w12 limit 4"""
df = pd.read_gbq(query, project_id=project_id)


query = """SELECT * FROM feature_build.INFORMATION_SCHEMA.TABLES"""
df = pd.read_gbq(query, project_id=project_id)


query = """SELECT * FROM region-us.INFORMATION_SCHEMA.SCHEMATA"""
dbs = pd.read_gbq(query, project_id=project_id)

 
query = """CREATE TABLE IF NOT EXISTS feature_build.ak_test as select * from feature_build.camp_flags_w12 limit 4"""
pd.read_gbq(query, project_id=project_id)


query = """select * from feature_build.ak_test"""
pd.read_gbq(query, project_id=project_id)



artss ='gs://wx-bq-poc.loyalty.article_sales_summary'
df = pd.read_csv(artss)

query = """DROP TABLE IF EXISTS feature_build.ak_test"""
pd.read_gbq(query, project_id=project_id)


query = """CREATE TABLE IF NOT EXISTS feature_build.ak_test as select * from wx-bq-poc.loyalty.article_sales_summary limit 4"""
pd.read_gbq(query, project_id=project_id)

query = """CREATE TABLE IF NOT EXISTS ak_test as select * from feature_build.camp_flags_w12  limit 4"""
pd.read_gbq(query, project_id=project_id)


query = """
 CREATE TABLE IF NOT EXISTS feature_build.base_cust_rgstr_dt AS
    SELECT 
         COALESCE(os.crn, lc.crn) as crn
        ,cast(os.rgstrd_dttm as date) as online_start
        ,lc.lylty_card_rgstr_date as rewards_start
    FROM 
        (SELECT 
              prmry_crn as crn
             ,min(lylty_card_rgstr_date) as lylty_card_rgstr_date 
         FROM `wx-bq-poc`.loyalty.lylty_card_detail 
         WHERE lylty_card_status_desc not in ('Cancelled', 'Closed', 'Deregistered')
         AND prmry_crn != '0'
         GROUP BY prmry_crn) lc
    FULL JOIN 
        (SELECT 
              crn
             ,min(rgstrd_dttm) as rgstrd_dttm
         FROM `wx-bq-poc`.loyalty.online_shopper_hist 
         GROUP BY crn) os
    ON os.crn = lc.crn"""
pd.read_gbq(query, project_id=project_id)


query = """
    CREATE TABLE IF NOT EXISTS feature_build.base_crn_ref_dt as
    SELECT 
         rd.crn
        ,d.clndr_date as ref_dt 
    FROM feature_build.base_cust_rgstr_dt rd
    INNER JOIN `wx-bq-poc`.loyalty.dim_date d
    ON DATE_ADD(case when rd.rewards_start is NULL then rd.online_start when rd.online_start is NULL then rd.rewards_start when rd.rewards_start < rd.online_start then rd.rewards_start else rd.online_start end, INTERVAL 7 DAY) <= d.clndr_date
    WHERE d.dow = 7
    AND d.clndr_date BETWEEN '2020-06-01' AND '2021-08-01'"""
pd.read_gbq(query, project_id=project_id)


query = """select * from `wx-bq-poc`.loyalty.dim_date limit 3;"""
pd.read_gbq(query, project_id=project_id)




















crns.shape
crns_samp=crns.iloc[0:5]
crns_samp=list(crns_samp['crn'])
from json import dumps

crns_samp=dumps(crns_samp)

crns_samp=crns_samp.replace("""[""",'').replace("""]""",'')


print(df.columns)
#lylty_card_nbr=
project_id = 'gcp-wow-rwds-ai-cfv-dev'
lylty_card_nbr='-6536562602514332566'
crn="'1100000000151952110'"

query = """select a.crn, b.* from `wx-bq-poc`.loyalty.article_sales_summary as b
join feature_build.base_cust as a on a.lylty_card_nbr=b.lylty_card_nbr
where 
a.crn in ({})
""".format(crns_samp)
samp = pd.read_gbq(query, project_id=project_id)

#-6536562602514332566
#select * from feature_build.base_cust where crn='1100000000151952110'; 
#select * from supers.supers_target where crn='1100000000151952110'; 

 
query = """select * from supers.supers_target where crn='1100000000151952110'; 
"""
supers_dat = pd.read_gbq(query, project_id=project_id)
supers_dat.columns
samp.columns
samp.head()



samp=samp[(samp['sales_chnl_code']==1) & (samp['division_nbr'].isin([1005,1030]))]

samp['TotalSales'] = samp.groupby(['lylty_card_nbr', 'start_txn_date'])['tot_net_incld_gst'].transform('sum')
 
summary_dat=samp[['lylty_card_nbr', 'start_txn_date', 'TotalSales','basket_key']].drop_duplicates()


import lifetimes
summary = lifetimes.utils.summary_data_from_transaction_data(summary_dat, 'lylty_card_nbr', 'start_txn_date', 'TotalSales' )
summary = summary.reset_index()
summary.head()


# Fitting the BG/NBD model
bgf = lifetimes.BetaGeoFitter(penalizer_coef=0.0)
bgf.fit(summary['frequency'], summary['recency'], summary['T'])




same_view=samp[samp['basket_key']=='20181127163924083018003266']



'base_unit_qty'	'prod_qty'	
'tot_net_incld_gst'

samp[samp['prod_qty']>1]

samp[samp['prod_nbr']=='365443-EA']

samp[samp['start_txn_date']=='2021-08-13']


query = """select * from `wx-bq-poc`.loyalty.article_sales_summary where prod_nbr='365443-EA' and start_txn_date= '2021-08-13' limit 10; 
"""
check1 = pd.read_gbq(query, project_id=project_id)
check1.to_csv('check.csv')


query = """select * from `wx-bq-poc`.loyalty.article_master where prod_nbr='365443-EA' limit 10; 
"""
check2 = pd.read_gbq(query, project_id=project_id)



#**************************************************************************************************************************************************************
#********************************************************ACTUAL SCORED DATA***************************************************************
#**************************************************************************************************************************************************************

#pysical location of results
#https://console.cloud.google.com/storage/browser/wx-lty-cfv-dev/scoring/prod/models?pageState=(%22StorageObjectListTable%22:(%22f%22:%22%255B%255D%22))&project=gcp-wow-rwds-ai-cfv-dev&prefix=&forceOnObjectsSortingFiltering=false
#********   old model
       
       
features=pd.read_csv('gs://wx-auto-ml/alexa/cfv/bigw-instore-spend/alexa20220114/feature_selection/varimp.csv')
       
pd.write_csv(features[['feature']],'feature_list_instore_spend.txt')
wx-lty-cfv-dev/scoring/dev/models/bigw/instore/spend_old/feature_list_instore_spend.txt
       
'https://storage.cloud.google.com/wx-lty-cfv-dev/scoring/dev/models/instore_spend/dollar_bands.parquet' 

       
'https://storage.cloud.google.com/wx-lty-cfv-dev/scoring/dev/models/instore_spend/dollar_bands.parquet' 
 

       
       
       
partition='gs://wx-lty-cfv-dev/scoring/dev/runs/20211210/output/partition_4/base_data.parquet'    
test = pd.read_parquet(partition )
       
partition2='gs://wx-lty-cfv-dev/scoring/dev/runs/20211210/output/partition_1/base_data.parquet'    
       
test2 = pd.read_parquet(partition2 )


       
       
       
       
test
for i in test2.columns:  
    print(i)
       
test = pd.read_parquet(d_bands,columns=['Category'])    
       
       
d_bands_o='gs://wx-lty-cfv-dev/scoring/dev/models/instore_spend/dollar_bands.parquet'
d_bands_o='gs://wx-lty-cfv-dev/scoring/prod/models/instore_spend/dollar_bands.parquet'
   
d_bands_o='gs://wx-lty-cfv-dev/scoring/dev/models/ecom_spend/dollar_bands.parquet'
d_bands_o='gs://wx-lty-cfv-dev/scoring/prod/models/ecom_spend/dollar_bands.parquet' 
       
d_bands_o='gs://wx-lty-cfv-dev/scoring/dev/models/ecom_spend_ORIGINAL/dollar_bands.parquet'
d_bands_o='gs://wx-lty-cfv-dev/scoring/dev/models/bigw/instore/spend_old/dollar_bands.parquet'
       
d_bands_o='gs://wx-lty-cfv-dev/scoring/dev/models/bigw/online/spend/dollar_bands.parquet'    
d_bands_o='gs://wx-lty-cfv-dev/scoring/dev/models/bigw/instore/spend/dollar_bands.parquet'   
      
       
d_bands_o='gs://wx-lty-cfv-dev/scoring/dev/models/bws/online/spend/dollar_bands.parquet'    
d_bands_o='gs://wx-lty-cfv-dev/scoring/dev/models/bws/instore/spend/dollar_bands.parquet'   
   
  
d_bands_o='gs://wx-lty-cfv-dev/scoring/dev/models/ecom3m_spend/dollar_bands.parquet'  
       
       
dollar_band = pd.read_parquet(d_bands_o)
       
     
dollar_band.iloc[0,0]=None        
dollar_band.iloc[0,1]=136.0   
dollar_band.iloc[1,0]=136.0  
dollar_band.iloc[1,1]=287.0
dollar_band.iloc[2,0]=287.0       
dollar_band.iloc[2,1]=658.0     
dollar_band.iloc[3,0]=658.0
dollar_band.iloc[3,1]=1340.0
dollar_band.iloc[4,0]=1340.0     
       
dollar_band_add = {'Lower Bound': 5192.0, 'Upper Bound': None, 'Category': 'High'}
dollar_band = dollar_band.append(dollar_band_add, ignore_index = True)
       
dollar_band['Category']=dollar_band_o['Category']
       
d_bands='gs://wx-lty-cfv-dev/scoring/dev/models/ecom_spend/dollar_bands.parquet'   
d_bands='gs://wx-lty-cfv-dev/scoring/dev/models/bigw/instore/spend/dollar_bands.parquet'
d_bands='gs://wx-lty-cfv-dev/scoring/dev/models/bigw/online/spend/dollar_bands.parquet' 
       
       
dollar_band.to_parquet(d_bands)     
dollar_band.to_parquet(d_bands_o)            
       
       
def formatscored(dat):
    holdout=dat[['crn', 'ref_dt','target','pred']]
    holdout['year'] = pd.DatetimeIndex(holdout['ref_dt']).year
    holdout['month'] = pd.DatetimeIndex(holdout['ref_dt']).month
    holdout['yyyy_mm']=pd.to_datetime(holdout[['year', 'month']].assign(DAY=1))
    return holdout
       
def root_mean_square_error(targets, predictions):
    return np.sqrt(np.mean((predictions-targets)**2))
 
       
def importit(path1, path2):
    scored_M =path1
    scored_M = pd.read_parquet(scored_M)
    scored_T =path2
    scored_T = pd.read_parquet(scored_T)   

    holdout=formatscored(scored_T)
    trained_on=formatscored(scored_M)     


    trained_on['RMSE'] = trained_on.apply(lambda x: root_mean_square_error(x['target'], x['pred']), axis=1)
    return trained_on,holdout

instore_path1='gs://wx-lty-cfv-dev/scoring/prod/models/instore_spend/scoring/scored_M.parquet'
instore_path2='gs://wx-lty-cfv-dev/scoring/prod/models/instore_spend/scoring/scored_T.parquet'       
       
ecom_path1='gs://wx-lty-cfv-dev/scoring/prod/models/ecom_spend/scoring/scored_M.parquet'
ecom_path2='gs://wx-lty-cfv-dev/scoring/prod/models/ecom_spend/scoring/scored_T.parquet'   

trained_on,holdout=importit(instore_path1, instore_path2)
title_ecom="Ecom spend performance: True vs. Predicted spend"
title_online="Ecom spend performance: True vs. Predicted spend"
 
       
scale_path='gs://wx-lty-cfv-dev/scoring/prod/models/instore_spend/scaling_factor.parquet'
scaling = pd.read_parquet(scale_path) 
       
buckets_path='gs://wx-lty-cfv-dev/scoring/prod/models/instore_spend/dollar_bands.parquet'
buckets = pd.read_parquet(buckets_path) 
       
cehc = pd.read_parquet(instore_path1) 
scaling.head()
cehc.head()
buckets.head()
       

#_m data which model was trained 
# scored_T holdout performance 
#for instore spend apply the scaling predictions aren't scaled yet 
       
trained_on.groupby(['ref_dt']).count()

trained_on.head()
trained_on[trained_on.target==trained_on.target.max()]
trained_on.target.min()

trained_on.pred.max()
trained_on.pred.min()
       
import matplotlib.pyplot as plt
from matplotlib.ticker import ScalarFormatter

plt.figure(figsize=(10,10))
plt.scatter(trained_on['target'],trained_on['pred'], c='crimson')
ax = plt.gca()


plt.yscale('log')
plt.xscale('log')
for axis in [ax.xaxis, ax.yaxis]:
    axis.set_major_formatter(ScalarFormatter())
ax.ticklabel_format(useOffset=False, style='plain')
#plt.ticklabel_format(useOffset=False, style='plain')
p1 = max(max(trained_on['pred']), max(trained_on['target']))
p2 = min(min(trained_on['pred']), min(trained_on['target']))
plt.plot([p1, p2], [p1, p2], 'b-')
plt.title(title, fontsize=20)
#ax.title('In store spend performance: True vs. Predicted spend')
plt.xlabel('True Values (log)', fontsize=15)
plt.ylabel('Predictions (log)', fontsize=15)
plt.axis('equal')
plt.show()

       
10**4
 
       
import datetime as dt
       
#####################GANTT CHART ############
       
       
start=['Nov 15, 2021',
'Nov 8, 2021',
'Nov 8, 2021',
'Nov 19, 2021',
'Nov 22, 2021',
'Nov 24, 2021',
'Nov 24, 2021',
'Nov 25, 2021',
'Nov 26, 2021',
'Nov 27, 2021']

       
end=['Nov 17, 2021',
'Nov 19, 2021',
'Nov 26, 2021',
'Nov 22, 2021',
'Nov 24, 2021',
'Nov 25, 2021',
'Nov 25, 2021',
'Nov 26, 2021',
'Nov 27, 2021',
'Nov 30, 2021'
]       


Task=['Build modelling dataset with',
'Add additional features (Kimchi digital , Kimchi rewards, behavioural)',
'Experiment with new features for supers and ecom model',
'Build & report on supers model with new dataset',
'Build & report on ecom model with new dataset',
'compare supers model with old dataset vs. new dataset',
'compare ecom model with old dataset vs. new dataset',
'Select dataset for final model (in-store + ecom)',
'Fine tune final models in needed (retraining etc..)',
'Report on dataset used via EDA analysis and document modelling process']
       
       
Department=['DS',
'DS',
'DS',
'DS',
'DS',
'DS',
'DS',
'DS',
'DS',
'DS']
       
       
Completion=['80',
'100',
'100',
'0',
'0',
'0',
'0',
'0',
'0',
'0']  
Completion = list(map(int, Completion))

df=pd.DataFrame({'start':start,'end':end  ,'Task':Task,'Department':Department,'Completion':Completion}) #,columns={'one','two'}

       
df.start=pd.to_datetime(df.start)
df.end=pd.to_datetime(df.end)
#Add Duration
df['duration']=df.end-df.start
df.duration=df.duration.apply(lambda x: x.days+1)
#sort in ascending order of start date
df=df.sort_values(by='start', ascending=True)
#project level variables
p_start=df.start.min()
p_end=df.end.max()
p_duration=(p_end-p_start).days+1
#Add relative date
df['rel_start']=df.start.apply(lambda x: (x-p_start).days)
df['w_comp']=round(df.Completion*df.duration/100,2)
df.head()       
       
       
       
plt.figure(figsize=(8,4))
plt.title('Gantt Chart: CFV Refresh', size=18)
#Light bar for entire task
plt.barh(y=df.Task, left=df.rel_start, width=df.duration, 
         alpha=0.4, color='green')
#Darker bar for completed part
plt.barh(y=df.Task, left=df.rel_start, width=df.w_comp, 
         alpha=1, color='green')
plt.gca().invert_yaxis()
plt.xticks(ticks=x_ticks[::3], labels=x_labels[::3])
plt.grid(axis='x')
plt.show()