#!/bin/python3
import os
import pickle
import shutil
import subprocess

from datetime import timedelta

def set_gcp_credentials():
    if not os.environ.get('GOOGLE_APPLICATION_CREDENTIALS'):
        os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = f"/home/jovyan/.config/gcloud/legacy_credentials/{os.environ.get('JUPYTERHUB_USER')}/adc.json"

def unpickle(file_path):
    """
    Load pickle file
    """
    try:
        with open(file_path, 'rb') as f:
            obj = pickle.load(f)
            return obj
    except Exception as e:
        raise

def copy_file(src_dir,dest_dir,filename,do_parallel=True,do_recursive=False):
    """
    Copies a file from one location to another using the
    gsutil cp command
    :param src_dir: full source directory path for file
    :param dest_dir: full destination directory path for file
    :param filename: specific file to copy

    :return: None
    """
    try:
        parallel = '-m' if do_parallel else ''
        recursive = '-r' if do_recursive else ''
        print(f'Copying {filename} from: {src_dir} to: {dest_dir}...')
        subprocess.call(f'gsutil {parallel} cp {recursive} {src_dir}/{filename} {dest_dir}',shell=True)
    except Exception as e:
        raise
    
def create_dir(dir_path):
    """
    Creates a directory at a given path if not already existing
    :param dir_path: full directory path to create
    
    :return: None
    """
    try:
        # check if does NOT exist
        if not os.path.exists(dir_path):
            os.mkdir(dir_path)
    except Exception as e:
        raise
    
def remove_dir(dir_path):
    """
    Removes a directory if it does exist
    :param dir_path: full directory path to remove
    
    :return: None
    """
    try:
        # check if exists
        if os.path.exists(dir_path):
            shutil.rmtree(dir_path)
    except Exception as e:
        raise
            
def remove_file(file_path):
    """
    Removes a specific file
    :param file_path: full file path to remove
    
    :return: None
    """
    
    try:
        # check if exists
        if os.path.isfile(file_path):
            os.remove(file_path)
    except Exception as e:
        raise
        
def remove_remote_file(remote_dir,do_parallel=True,do_recursive=False):
    """
    Removes a remote file
    :param remote_dir: full directory path to remove
    :param do_parallel: boolean to remove parallel
    :param do_recursive: boolean to remove recursively
    
    :return: None
    """
    
    try:
        parallel = '-m' if do_parallel else ''
        recursive = '-r' if do_recursive else ''
        subprocess.call(f'gsutil {parallel} rm {recursive} {remote_dir}',shell=True)
    except Exception as e:
        raise
        
# function to convert given dtime (%Y-%m-%d) to most recent Sunday
def last_day(ref_dtime, day_idx=6):
    """
    Returns last day from a given date time
    :param ref_dtime: datetime
    :param day_idx: integer (day of week)
    
    :return: datetime corresponding to last "day"
    """
    # default day_idx = 6, SUNDAY
    offset = ((ref_dtime.weekday()-day_idx)+7)%7
    return ref_dtime - timedelta(days=offset)

def onehot_decode(df, prefix, split_by='__'):
    """
    Map back to categories for one-hot encoded categories
    :param df: pandas dataframe
    :param prefix: string
    :param split_by: how to identify value to set decoded column
    
    :return: pandas series
    """
    try:
        df = df.copy()
        l_columns = [c for c in df.columns if c.startswith(prefix)]
        assert len(l_columns), "Prefix search returns empty columns"
        
        df['oh_decode'] = np.nan
        for col in l_columns:
            value = col.split(split_by)[-1]
            df.loc[df[col]==1,'oh_decode'] = value
        return df.oh_decode
    except Exception as e:
        raise