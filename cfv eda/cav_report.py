
import pandas as pd
import os
from google.cloud import bigquery
from google.cloud import storage
import pickle
os.environ['GOOGLE_APPLICATION_CREDENTIALS']=f"/home/jovyan/.config/gcloud/legacy_credentials/{os.getenv('JUPYTERHUB_USER')}/adc.json"
import numpy as np
import sys
np.set_printoptions(threshold=sys.maxsize)
pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 500)
from pandas_profiling import ProfileReport
import matplotlib.pyplot as plt
import matplotlib
import seaborn as sns
from matplotlib.ticker import ScalarFormatter
from sklearn.metrics import mean_squared_error
import statsmodels.api as sm
import subprocess
project_id = 'gcp-wow-rwds-ai-cfv-prod'
 
 

 
query= "select * from  akelly.cav_curr_vs_prev_yr;"
curr_prev_yr=pd.read_gbq(query, project_id=project_id)


curr_prev_yr['pred_bin_2']=['Churned' if v is None else v for v in curr_prev_yr['pred_bin_2']]

curr_prev_yr['category']=curr_prev_yr['pred_bin_1']+" to " +curr_prev_yr['pred_bin_2']

sns.set_style('darkgrid') # darkgrid, white grid, dark, white and ticks
plt.rc('axes', titlesize=18)     # fontsize of the axes title
plt.rc('axes', labelsize=14)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=13)    # fontsize of the tick labels
plt.rc('ytick', labelsize=13)    # fontsize of the tick labels
plt.rc('legend', fontsize=13)    # legend fontsize
plt.rc('font', size=13)          # controls default text sizes

#sns.set_palette(reversed(sns.color_palette("Greens", n_plots)), n_plots)

plt.figure(figsize=(12, 6), tight_layout=True)
ax = sns.barplot(x=curr_prev_yr['category'], y=curr_prev_yr['customers'], color="green")# ,order= curr_prev_yr['customers'])#,order=barplot['offer_status'] ) #, palette='pastel'
#ax.set(title='Distribution of supers total (Nov20-Nov21)' ,xlabel='Spend Range', ylabel='Customers')
ax.set(title='CAV category movement YOY' ,xlabel='Spend Range', ylabel='Customers')
plt.xticks(rotation=90)
#ax.get_yaxis().set_major_formatter(
 #   matplotlib.ticker.FuncFormatter(lambda x, p: format(int(x), ',')))
plt.show()


cav_curr_vs_prev_dec21
--cav_curr_vs_prev_jan22
 supers_cav_curr_vs_prev
query= "select * from  akelly.cav_curr_vs_prev;"

query= "select * from  akelly.supers_cav_curr_vs_prev;"
curr_prev=pd.read_gbq(query, project_id=project_id)


curr_prev['pred_bin_2']=['Churned' if v is None else v for v in curr_prev['pred_bin_2']]

curr_prev['category']=curr_prev['pred_bin_1']+" to " +curr_prev['pred_bin_2']


cavmove=curr_prev[curr_prev.pred_bin_1!=curr_prev.pred_bin_2]
cavmove['cnt'].sum()/curr_prev['cnt'].sum()

104323
(1623394+ 763312+104323)/14563892
14563892
0.14605925175496268
#mar 1163193 6%
#feb 1097876 6%
#jan 1938860 12%
#dec 737659 4%  

#nov 4%


#2314286
#mar 2055663 12%
#jan 4390936 26%
#dec 1972857 11%
#nov 1857103 12%

sns.set_style('darkgrid') # darkgrid, white grid, dark, white and ticks
plt.rc('axes', titlesize=18)     # fontsize of the axes title
plt.rc('axes', labelsize=14)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=13)    # fontsize of the tick labels
plt.rc('ytick', labelsize=13)    # fontsize of the tick labels
plt.rc('legend', fontsize=13)    # legend fontsize
plt.rc('font', size=13)          # controls default text sizes

#sns.set_palette(reversed(sns.color_palette("Greens", n_plots)), n_plots)

plt.figure(figsize=(12, 6), tight_layout=True)
ax = sns.barplot(x=curr_prev['category'], y=curr_prev['customers'], color="green")# ,order= curr_prev_yr['customers'])#,order=barplot['offer_status'] ) #, palette='pastel'
#ax.set(title='Distribution of supers total (Nov20-Nov21)' ,xlabel='Spend Range', ylabel='Customers')
ax.set(title='CAV category movement MOM' ,xlabel='Spend Range', ylabel='Customers')
plt.xticks(rotation=90)
#ax.get_yaxis().set_major_formatter(
 #   matplotlib.ticker.FuncFormatter(lambda x, p: format(int(x), ',')))
plt.show()


 
query= "select * from akelly.bernolli;"
bernolli=pd.read_gbq(query, project_id=project_id)

for i in bernolli.pred_bin.unique():

 bernolli_i=bernolli[bernolli.pred_bin==i].drop(['pred_bin'],axis=1)


 ax = sns.lineplot(x='ref_dt', y='value', hue='variable', 
              data=pd.melt(bernolli_low, ['ref_dt']))
 ax.set(title='Bernolli distribution for ' +i ,xlabel='Date', ylabel='Score')
 
 plt.show()