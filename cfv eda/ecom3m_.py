
import pandas as pd
import os
from google.cloud import bigquery
from google.cloud import storage
import pickle
os.environ['GOOGLE_APPLICATION_CREDENTIALS']=f"/home/jovyan/.config/gcloud/legacy_credentials/{os.getenv('JUPYTERHUB_USER')}/adc.json"
import numpy as np
import sys
from datetime import timedelta
import datetime


#Statistical LTV
from lifetimes import BetaGeoFitter, GammaGammaFitter
from lifetimes.utils import calibration_and_holdout_data, summary_data_from_transaction_data

#ML Approach to LTV
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
import tensorflow_docs as tfdocs
import tensorflow_docs.plots
import tensorflow_docs.modeling

#Evaluation
from sklearn.metrics import r2_score
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import mean_squared_error

#Plotting
import matplotlib.pyplot as plt
import seaborn as sns
np.set_printoptions(threshold=sys.maxsize)
pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 500)
#from pandas_profiling import ProfileReport




project_id = 'gcp-wow-rwds-ai-cfv-dev'


#query = """drop table if exists akelly.campaignanalysis"""

#CampaignId='29956'
#supermarkets
query = """select du_in_free_trial_period ,
onln_spend_catg_1502_w52 ,
onln_spend_catg_0572_26w ,
affluence ,
onln_spend_catg_2787_w52 ,
f__wow_ecom_cnt_visits_2w ,
remoteness_area ,
onln_spend_catg_0541_w52 ,
onln_spend_catg_1065_26w ,
gender ,
onln_pm_order_26w ,
onln_pct_dlv_orders_26w ,
onln_spend_catg_0586_w52 ,
onln_spend_catg_0581_26w ,
onln_spend_catg_0540_w52 ,
onln_spend_catg_0559_26w ,
onln_spend_catg_0574_26w ,
onln_spend_catg_0553_26w ,
f__wow_ecom_avg_session_length_2w ,
onln_spend_catg_1505_w52 ,
onln_spend_catg_0585_26w ,
onln_spend_catg_0564_26w ,
onln_spend_catg_0552_26w ,
onln_spend_catg_0546_w52 ,
onln_spend_catg_0548_26w ,
onln_tot_minutes_8w ,
supers_udp_avg_wkly_amt_w8 ,
onln_tot_subs_optin_26w ,
supers_udp_tot_dscnt_perc_w4 ,
supers_udp_ipi_w52 ,
supers_udp_tot_dscnt_perc_w2 ,
onln_spend_catg_0557_w52 ,
onln_tot_savings_8w ,
supers_udp_tot_dscnt_perc_w12 ,
onln_lines_ps_26w ,
onln_avg_fulfil_time_26w ,
udp_et_redeem_rate_w52 ,
onln_tot_lines_8w ,
f__wow_ecom_avg_session_length_8w ,
supers_udp_days_since_last_shop ,
supers_udp_max_amt_w8 ,
onln_avg_minutes_26w ,
customer_lifecycle_segment ,
onln_spend_catg_4760_w52 ,
supers_udp_max_amt_w12 ,
onln_spend_catg_0582_w52 ,
customer_state_addr ,
onln_tot_lines_4w ,
onln_spend_catg_1090_w52 ,
onln_std_pick_orders_26w ,
du_days_tenure ,
supers_udp_tot_dscnt_perc_w26 ,
onln_avg_savings_prop_4w ,
onln_spend_catg_1093_w2 ,
onln_spend_catg_0585_w52 ,
onln_spend_catg_0544_w52 ,
onln_orders_26w ,
ecom_udp_wks_since_third_last_shop ,
ncluster ,
onln_spend_catg_0576_w52 ,
onln_avg_lines_26w ,
onln_tot_savings_26w ,
supers_udp_avg_amt_w26 ,
Age ,
supers_udp_avg_wkly_amt_w26 ,
onln_spend_catg_3063_w2 ,
onln_dlv_addr_state ,
ecom_udp_ipi_w52 ,
ecom_udp_ipi_w26 ,
supers_udp_num_prod_w4 ,
onln_spend_catg_0564_w52 ,
onln_spend_catg_0574_w52 ,
onln_spend_catg_0552_w52 ,
onln_tot_minutes_26w ,
onln_reg_days ,
onln_avg_spend_8w ,
onln_avg_savings_prop_26w ,
onln_spend_catg_0553_w52 ,
supers_udp_tot_dscnt_perc_w52 ,
onln_spend_catg_1093_w52 ,
lifestage ,
onln_spend_catg_0548_w52 ,
ecom_udp_wks_since_second_last_shop ,
supers_udp_avg_wkly_amt_w52 ,
onln_spend_catg_0581_w52 ,
online_tenure ,
onln_tot_surveys ,
onln_avg_spend_4w ,
supers_udp_max_amt_w26 ,
onln_tot_items_4w ,
onln_spend_catg_3069_w52 ,
onln_tot_items_8w ,
onln_spend_catg_3063_w52 ,
segment_cell_curr ,
onln_avg_items_26w ,
ecom_udp_wks_since_last_shop ,
onln_age ,
onln_spend_catg_1091_w52 ,
supers_udp_max_amt_w52 ,
supers_udp_avg_amt_w52 ,
sow ,
onln_tot_lines_26w ,
postcode ,
time_diff_month ,
drv_time_to_WOW ,
drv_time_to_COLES ,
drv_time_to_ALDI ,
onln_tot_items_26w ,
drv_time_to_IGA ,
store_name ,
onln_avg_spend_26w ,
onln_tot_spend_4w ,
onln_tot_spend_26w ,
onln_tot_spend_8w,
ecom_tot_spend
from akelly.ecom_base_2020_to_20213m_with_kimchi3m_with_cust_seg3m
"""

data = pd.read_gbq(query, project_id=project_id)





data.to_feather('ecom3m_data.feather')

data=pd.read_feather('ecom3m_data.feather')
data.columns
data.type()
print(data.dtypes)

 
from sklearn.preprocessing import LabelEncoder

le = LabelEncoder()


weird=['sow','drv_time_to_WOW',	'drv_time_to_COLES'	,'drv_time_to_ALDI'	,'drv_time_to_IGA']
data.loc[:, weird] = data.loc[:, weird].astype(float)

float_cols = data.select_dtypes(include=['float64']).columns
str_cols =np.array(data.select_dtypes(include=['object']).columns) 


str_cols=[i for i in str_cols if i not in ['sow','drv_time_to_WOW',	'drv_time_to_COLES'	,'drv_time_to_ALDI'	,'drv_time_to_IGA']]





data.loc[:, float_cols] = data.loc[:, float_cols].fillna(0)
data.loc[:, str_cols] = data.loc[:, str_cols].fillna('')


data.loc[:, str_cols] = data.loc[:, str_cols].apply(le.fit_transform)

x = np.array(data.drop(["ecom_tot_spend"], 1))
y = np.array(data["ecom_tot_spend"])
from sklearn.model_selection import train_test_split
xtrain, xtest, ytrain, ytest = train_test_split(x, y, test_size=0.2, random_state=42)


#reg = LazyRegressor(verbose=0, 
                 #   ignore_warnings=False, 
                 #   custom_metric=None)
#models, predictions = reg.fit(xtrain, xtest, ytrain, ytest)

from sklearn.preprocessing import MinMaxScaler

scaler = MinMaxScaler(feature_range=(-1, 1))

dataset = scaler.fit_transform(data)
with open('dataset.npy', 'wb') as f:
    np.save(f, dataset)
# split into train and test sets
train_size = int(len(dataset) * 0.67)
test_size = len(dataset) - train_size
train, test = dataset[0:train_size,:], dataset[train_size:len(dataset),:]

Xtrain, ytrain = np.split(train,[-1],axis=1)
Xtest, ytest = np.split(test,[-1],axis=1)


print(len(train), len(test))

 
# convert an array of values into a dataset matrix


import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
from tensorflow.keras.layers  import Dense
from tensorflow.keras.models  import Sequential
from tensorflow.keras.optimizers import Adam 
from tensorflow.keras.callbacks import EarlyStopping
#from tensorflow.keras.utils import np_utils
from tensorflow.keras.layers import LSTM
from sklearn.model_selection import KFold, cross_val_score, train_test_split

# reshape input to be [samples, time steps, features]
trainX = np.reshape(Xtrain, (Xtrain.shape[0], 1, Xtrain.shape[1]))
testX = np.reshape(Xtest, (Xtest.shape[0], 1, Xtest.shape[1]))


model = Sequential()
model.add(LSTM(4, input_shape=(1, trainX.shape[2])))
model.add(Dense(1))
model.compile(loss='mean_squared_error', optimizer='adam')
model.fit(trainX, ytrain, epochs=5, batch_size=1, verbose=2)

model.save("my_model5epchs")

# make predictions
trainPredict = model.predict(trainX)
testPredict = model.predict(testX)
ytrain.shape
trainPredict.shape
trainPredict[2:4]
trainX.shape
Xtrain[2:4]
train=np.concatenate((Xtrain, trainPredict))


[:, [0]]
import math
# invert predictions
trainPredict = scaler.inverse_transform(trainPredict)
trainY = scaler.inverse_transform([ytrain])
testPredict = scaler.inverse_transform(testPredict)
testY = scaler.inverse_transform([testY])
# calculate root mean squared error
trainScore = math.sqrt(mean_squared_error(ytrain[0], trainPredict[:,0]))
print('Train Score: %.2f RMSE' % (trainScore))
testScore = math.sqrt(mean_squared_error(testY[0], testPredict[:,0]))
print('Test Score: %.2f RMSE' % (testScore))