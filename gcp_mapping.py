import pandas as pd

xls = pd.ExcelFile('gcp mapping/RWM - Data Dictionary.xlsx')

# Now you can list all sheets in the file
names=xls.sheet_names
# ['house', 'house_extra', ...]

names.remove('Summary')

appended_data = []
d = {}
df=pd.DataFrame()
for name in names:
    d[name]=pd.read_excel(xls, sheet_name=name, skiprows=[0])
    appended_data.append(d[name])
# to read just one sheet to dataframe:


d['Proximity']

appended_data = pd.concat(appended_data)

appended_data_quicklook=appended_data[['Table','ColumnName','Dataset', 'ViewName'  ,'ColumnName.1']]

appended_data_quicklook=appended_data_quicklook.drop_duplicates()
appended_data.shape
appended_data_quicklook.dropna()
appended_data.to_csv('gcp mapping/gcp datadict.csv')
