
import pandas as pd
import os
from google.cloud import bigquery
from google.cloud import storage
import pickle
os.environ['GOOGLE_APPLICATION_CREDENTIALS']=f"/home/jovyan/.config/gcloud/legacy_credentials/{os.getenv('JUPYTERHUB_USER')}/adc.json"
import numpy as np
import sys
np.set_printoptions(threshold=sys.maxsize)
pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 500)
#from pandas_profiling import ProfileReport


project_id = 'gcp-wow-rwds-ai-cfv-dev'

#query = """drop table if exists akelly.campaignanalysis"""
query = """create table akelly.campaignanalysis as 
select 
a.*
from gcp-wow-ent-im-wowx-cust-prod.adp_wowx_dm_campaign_view.targeted_customer_list_v as a 
where a.CampaignCode ='RTN-4879'
"""
#CampaignId='29956'
#supermarkets


df = pd.read_gbq(query, project_id=project_id)


query = """select * from akelly.campaignanalysis where CustomerRegistrationNumber ='1100000000001590357'
"""

query = """select * from akelly.campaignanalysis limit 3
"""
temp=pd.read_gbq(query, project_id=project_id)
temp.columns




query = """select ActivationType,count(*) from akelly.campaignanalysis group by 1
"""
query = """select COUNT(distinct(LoyaltyCardNumber)) from akelly.campaignanalysis
"""
query = """select * from akelly.campaignanalysis WHERE CustomerRegistrationNumber='1100000000072360650'
"""

query = """select UserActivationInd,count(*) from akelly.campaignanalysis group by 1"""

 
query = """create table akelly.campaignanalysis2 AS SELECT a.*, b.ActivationDate ,
	case when b.ActivationDate  is not null then 'ACTIVATED' ELSE 'NOT' END AS ACTIVATED_CHECK
	from akelly.campaignanalysis as a left join 
gcp-wow-ent-im-wowx-cust-prod.adp_wowx_dm_campaign_view.customer_activated_offers_v AS B on 
a.CommID=b.CommID"""
    
    
query = """select ACTIVATED_CHECK,count(distinct(CustomerRegistrationNumber)) as crns, count(*) from akelly.campaignanalysis2 group by 1"""

query = """select * from gcp-wow-ent-im-wowx-cust-prod.adp_wowx_dm_campaign_view.customer_activated_offers_v
limit 3
"""

query = """select * from gcp-wow-ent-im-wowx-cust-prod.adp_wowx_dm_campaign_view.customer_activated_offers_v
where EventdetailCampcode='RTN-4879' limit 6
"""

query = """drop table if exists akelly.campaignanalysis3
"""

query = """create table akelly.campaignanalysis2 AS SELECT a.*, b.ActivationDate ,b.EventmetadataEventtype,
	b.EventmetadataEventname,
	case when b.ActivationDate  is not null then 'ACTIVATED' ELSE 'NOT' END AS ACTIVATED_CHECK
	from akelly.campaignanalysis as a left join 
gcp-wow-ent-im-wowx-cust-prod.adp_wowx_dm_campaign_view.customer_activated_offers_v AS B on 
a.CampaignCode=b.EventdetailCampcode 
and a.LoyaltyCardNumber=b.LoyaltyCardNumber
and a.CustomerRegistrationNumber=b.CustomerRegistrationNumber;
"""
df=pd.read_gbq(query, project_id=project_id)



query = """select CustomerRegistrationNumber,OfferAllocationStartTS,OfferAllocationEndTS, count(*)

from akelly.campaignanalysis2 where CustomerRegistrationNumber in  ('1100000000003174344','1100000000001590357')
group by 1,2,3
"""
pd.read_gbq(query, project_id=project_id)

query = """
create table akelly.campaignanalysis3 as 
with base as (
    -- select crn
    -- from loyalty_campaign_analytics.rtn4879_final
    -- -- from `gcp-wow-rwds-ai-cfv-dev.azhang.true_audience_4879`
    -- where pred_bin = 'hvcfv'

select cast(crn as string) as crn ,"control" as aud_type from `gcp-wow-rwds-ai-cfv-dev.asach.all_campaign_audience` 
where crn not in (select cast(crn as string) as crn 
                                    from wx-bq-poc.loyalty.et_targeted_customer_offer 
                                    where campaign_code= 'RTN-4879')
union DISTINCT
select cast(crn as string) as crn ,"test" as aud_type from `gcp-wow-rwds-ai-cfv-dev.asach.all_campaign_audience` 
where crn in (select cast(crn as string) as crn from wx-bq-poc.loyalty_campaign_analytics.rtn4879_final where pred_bin = 'hvcfv' )
and cast(crn as string) in (select cast(crn as string) as crn 
                                    from wx-bq-poc.loyalty.et_targeted_customer_offer 
                                    where campaign_code= 'RTN-4879' )

)
,sent as (
select distinct crn,1 as send_ind 
from wx-bq-poc.loyalty.et_targeted_customer_offer
where campaign_code= 'RTN-4879'
)

,activation as (
Select distinct(cecoa.crn),1 as act_ind
    --    ce.cmpgn_code as campaign_code, offer_id
FROM        wx-bq-poc.loyalty.campaign_exec ce
INNER JOIN 	wx-bq-poc.loyalty.campaign_exec_cust_offer_allocation cecoa 
on 			ce.cmpgn_exec_id = cecoa.cmpgn_exec_id
and 		cecoa.offer_id IN (-218616,-216217) 
WHERE      	cecoa.activation_ts IS NOT NULL 
AND 		ce.cmpgn_code = 'RTN-4879'	
AND 		ce.test_exec_ind != 'Y'
)

,temp_base_aud as (
        select bse.crn,aud_type,
       case when send_ind=1 then 1 else 0 end as send_ind
      ,case when act_ind=1 then 1 else 0 end as act_ind
from base as bse
left join sent 
on cast(bse.crn as string)= sent.crn
left join activation as act 
on cast(bse.crn as string)=act.crn
)
,super_spend as (
    select crn
           ,sum(case when sp.ref_dt="2021-08-15" then sp.supers_udp_tot_amt_w8 else 0 end )as before_instore_spend_w8
           ,sum(case when sp.ref_dt="2021-10-10" then sp.supers_udp_tot_amt_w8 else 0 end) as after_instore_spend_w8
    from`gcp-wow-rwds-ai-cfv-dev.feature_build.cfv_supers_txn_w8` sp
    -- where ref_dt in("2021-08-15","2021-09-19"  )
    where ref_dt in("2021-08-15","2021-10-10"  )    
    	

    group by crn )
select * from temp_base_aud
"""
base=pd.read_gbq(query, project_id=project_id)

base[base['crn'] =='3300000000005244789']


base[base['aud_type'] !='test']

base.groupby(['aud_type','send_ind','act_ind']).count()




query = """
create table akelly.campaignanalysis3_test as 
with base as (
    -- select crn
    -- from loyalty_campaign_analytics.rtn4879_final
    -- -- from `gcp-wow-rwds-ai-cfv-dev.azhang.true_audience_4879`
    -- where pred_bin = 'hvcfv'

select cast(crn as string) as crn ,"control" as aud_type from `gcp-wow-rwds-ai-cfv-dev.asach.all_campaign_audience` 
where crn not in (select cast(crn as string) as crn 
                                    from wx-bq-poc.loyalty.et_targeted_customer_offer 
                                    where campaign_code= 'RTN-4879')
union DISTINCT
select cast(crn as string) as crn ,"test" as aud_type from `gcp-wow-rwds-ai-cfv-dev.asach.all_campaign_audience` 
where crn in (select cast(crn as string) as crn from wx-bq-poc.loyalty_campaign_analytics.rtn4879_final where pred_bin = 'hvcfv' )
and cast(crn as string) in (select cast(crn as string) as crn 
                                    from wx-bq-poc.loyalty.et_targeted_customer_offer 
                                    where campaign_code= 'RTN-4879' )

)
,sent as (
select distinct crn,1 as send_ind 
from wx-bq-poc.loyalty.et_targeted_customer_offer
where campaign_code= 'RTN-4879'
)

,activation as (
Select distinct(cecoa.crn),1 as act_ind
    --    ce.cmpgn_code as campaign_code, offer_id
FROM        wx-bq-poc.loyalty.campaign_exec ce
INNER JOIN 	wx-bq-poc.loyalty.campaign_exec_cust_offer_allocation cecoa 
on 			ce.cmpgn_exec_id = cecoa.cmpgn_exec_id
and 		cecoa.offer_id IN (-218616,-216217) 
WHERE      	cecoa.activation_ts IS NOT NULL 
AND 		ce.cmpgn_code = 'RTN-4879'	
AND 		ce.test_exec_ind != 'Y'
)

,temp_base_aud as (
        select bse.crn,aud_type,
       case when send_ind=1 then 1 else 0 end as send_ind
      ,case when act_ind=1 then 1 else 0 end as act_ind
from base as bse
left join sent 
on cast(bse.crn as string)= sent.crn
left join activation as act 
on cast(bse.crn as string)=act.crn
)
,super_spend as (
    select crn
           ,sum(case when sp.ref_dt="2021-08-15" then sp.supers_udp_tot_amt_w8 else 0 end )as before_instore_spend_w8
           ,sum(case when sp.ref_dt="2021-10-10" then sp.supers_udp_tot_amt_w8 else 0 end) as after_instore_spend_w8
    from`gcp-wow-rwds-ai-cfv-dev.feature_build.cfv_supers_txn_w8` sp
    -- where ref_dt in("2021-08-15","2021-09-19"  )
    where ref_dt in("2021-08-15","2021-10-10"  )    
    	

    group by crn )
select a.* , b.before_instore_spend_w8,b.after_instore_spend_w8 from temp_base_aud as a join super_spend as b on a.crn=b.crn
"""
withsuperspend=pd.read_gbq(query, project_id=project_id)
withsuperspend.head()

base3[base3['crn']=='1100000000103499441']


query = """
create table akelly.campaignanalysis4 as 
select a.*,
b.PrimaryCustomerRegistrationNumber as LoyaltyCardNumber,min(LoyaltyCardRegistrationDate) as lylty_card_rgstr_date 
from akelly.campaignanalysis3 as a 
LEFT JOIN `gcp-wow-ent-im-wowx-cust-prod`.adp_wowx_dm_customer_view.loyalty_card_detail_v b
ON a.crn = b.CustomerRegistrationNumber
where b.LoyaltyCardStatusDescription not in ('Cancelled', 'Closed', 'Deregistered')
 """
pd.read_gbq(query, project_id=project_id)


query = """
SELECT a.crn,
case 
when a.act_ind	=1 then 'accepted'
when a.aud_type='test' and a.send_ind=1 then 'ignored'
else 'control' end as cust_type
,a.LoyaltyCardNumber,
CAST(EXTRACT(WEEK from c.TXNStartDate) as string) as week ,
CAST(EXTRACT(MONTH from c.TXNStartDate) as string) as MONTH ,
case when c.TXNStartDate>="2021-08-19" then 'after offer' else 'before offer' end as offer_status,
sum(c.ProductQty) as tot_pro,
sum(c.TotalAmountIncldTax) as tot_spend
from akelly.campaignanalysis4 as a join 
gcp-wow-ent-im-wowx-cust-prod.adp_wowx_dm_integrated_sales_view.article_sales_summary_v as c
on a.LoyaltyCardNumber=c.LoyaltyCardNumber
where c.TXNStartDate>="2021-06-20" and c.TXNStartDate<="2021-10-10" and
c.SalesOrg in(1030, 1005)
AND c.VoidFlag <> 'Y'
group by 1,2,3,4,5,6
 """

#case when c.TXNStartDate>=EXTRACT(DATE from b.OfferAllocationStartTS) and c.TXNStartDate<=EXTRACT(DATE from b.OfferAllocationEndTS )  then 'in allocation' else 'outside allocation' end as allocation_status,

query = """ 
select c.LoyaltyCardNumber,EXTRACT(YEAR from c.TXNStartDate) as year,
EXTRACT(MONTH from c.TXNStartDate) as month, 
count(distinct(c.LoyaltyCardNumber)) as customers,
sum(c.ProductQty) as tot_pro,
sum(c.TotalAmountIncldTax) as tot_spend,
avg( c.TotalAmountIncldTax) as avg_spend,
min( c.TotalAmountIncldTax) as min_spend,
max( c.TotalAmountIncldTax) as max_spend
from gcp-wow-ent-im-wowx-cust-prod.adp_wowx_dm_integrated_sales_view.article_sales_summary_v as c
where 
c.LoyaltyCardNumber='-8482206883256382192'
AND c.TXNStartDate>="2021-01-01" 
AND c.SalesOrg in(1030, 1005)
AND c.VoidFlag <> 'Y' 
AND c.TotalAmountIncldTax>0
AND c.POSNumber <> 100 
group by 1,2,3
"""
seasonal_spend=pd.read_gbq(query, project_id=project_id)



query = """
SELECT c.TXNStartDate,
case 
    when a.act_ind	=1 then 'accepted'
    when a.aud_type='test' and a.send_ind=1 then 'ignored'
    else 'control' end as cust_type,
case when c.TXNStartDate>=EXTRACT(DATE from b.OfferActivationStartTs) and c.TXNStartDate<=EXTRACT(DATE from b.OfferActivationEndTs )  then 'in offer' else 'outside offer' end as offer_status,
case when SalesChannelCode=1 then 'In store' else 'Online' end as shop_type,
count(distinct(a.crn)) as crns,
sum(c.ProductQty) as tot_pro,
sum(c.TotalAmountIncldTax) as tot_spend,
avg( c.TotalAmountIncldTax) as avg_spend,
min( c.TotalAmountIncldTax) as min_spend,
max( c.TotalAmountIncldTax) as max_spend
from akelly.campaignanalysis4 as a join 
gcp-wow-ent-im-wowx-cust-prod.adp_wowx_dm_integrated_sales_view.article_sales_summary_v as c
on a.LoyaltyCardNumber=c.LoyaltyCardNumber left join 
 akelly.campaignanalysis as b on a.crn=b.CustomerRegistrationNumber
where c.TXNStartDate>="2021-06-20" and c.TXNStartDate<="2021-10-10" and
c.SalesOrg in(1030, 1005)
AND c.VoidFlag <> 'Y' and c.TotalAmountIncldTax>0
AND c.POSNumber <> 100 
group by 1,2,3,4
 """
agg2=pd.read_gbq(query, project_id=project_id)

agg2.groupby('shop_type').count()

agg2.count()

query = """
SELECT b.CustomerRegistrationNumber,count(distinct(b.OfferAllocationStartTS)) 
from akelly.campaignanalysis4 as a  
left join 
akelly.campaignanalysis as b on a.crn=b.CustomerRegistrationNumber
where 
a.act_ind	=1 
group by 1
 """
no_activations=pd.read_gbq(query, project_id=project_id)

query = """
SELECT * from akelly.campaignanalysis limit 3
 """
pd.read_gbq(query, project_id=project_id)


agg1


base3=pd.read_gbq(query, project_id=project_id)

base3.shape
base3.head()

#accepted crns ('1100000000003174344','1100000000001590357')

path='campaign analysis /'
train_path= 'base3.feather'
#base3.to_feather(train_path)
#agg2.to_feather('agg2.feather')
base3 = pd.read_feather('base3.feather')
agg1 = pd.read_feather('agg1.feather')



base3[base3['cust_type']=='accepted']

base3.groupby(['offer_status'	,'MONTH'	,'week']).count()

forgraph=base3.groupby(['cust_type','offer_status','MONTH','week'])['tot_pro'	,'tot_spend'].agg(['count', 'sum','mean'])

forgraph.to_csv(path+'forg.csv')

base3.groupby(['cust_type','offer_status'])['tot_pro'	,'tot_spend'].agg(['count', 'sum','mean'])

import matplotlib.pyplot as plt
import seaborn as sns


sns.set_style('darkgrid') # darkgrid, white grid, dark, white and ticks
plt.rc('axes', titlesize=18)     # fontsize of the axes title
plt.rc('axes', labelsize=14)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=13)    # fontsize of the tick labels
plt.rc('ytick', labelsize=13)    # fontsize of the tick labels
plt.rc('legend', fontsize=13)    # legend fontsize
plt.rc('font', size=13)          # controls default text sizes
green = sns.color_palette("Greens")


base3['offer_status'] = base3['offer_status'].astype('category').cat.reorder_categories(['before offer', 'after offer'])
base3['cust_type'] = base3['cust_type'].astype('category').cat.reorder_categories(['control', 'ignored','accepted'])


barplot = base3.groupby(['cust_type', 'offer_status'], as_index=False).count()

plt.figure(figsize=(12, 6), tight_layout=True)
ax = sns.barplot(x=barplot['cust_type'], y=barplot['crn'], hue=barplot['offer_status'], palette=green )#,order=barplot['offer_status'] ) #, palette='pastel'
ax.set(title='Customers that accepted the offer had a lower rate of decline in customer numbers' ,xlabel='Experiment group', ylabel='Customer numbers')
ax.legend(title='Campaign period', title_fontsize='13', loc='upper right')
plt.show()

barplot2 = base3.groupby(['cust_type', 'offer_status'], as_index=False)['tot_spend'].sum()

plt.figure(figsize=(12, 6), tight_layout=True)
ax = sns.barplot(x=barplot2['cust_type'], y=barplot2['tot_spend'], hue=barplot2['offer_status'], palette=green )#,order=barplot['offer_status'] ) #, palette='pastel'
ax.set(title='Customers that accepted the offer had a lower rate of decline in total spend' ,xlabel='Experiment group', ylabel='Total spend')
ax.legend(title='Campaign period', title_fontsize='13', loc='upper right')
plt.show()



base3.groupby('crn')['offer_status'].count()
before=base3[base3['offer_status']=='before offer']['crn'].drop_duplicates()
after=base3[base3['offer_status']!='before offer']['crn'].drop_duplicates()

base_onlyinboth=base3[base3['crn'].isin(after)]

barplot3 = base_onlyinboth.groupby(['cust_type', 'offer_status'], as_index=False)['tot_spend'].sum()

plt.figure(figsize=(12, 6), tight_layout=True)
ax = sns.barplot(x=barplot3['cust_type'], y=barplot3['tot_spend'], hue=barplot3['offer_status'], palette=green )#,order=barplot['offer_status'] ) #, palette='pastel'
ax.set(title='Customers that accepted the offer had a lower rate of decline in total spend' ,xlabel='Experiment group', ylabel='Total spend')
ax.legend(title='Campaign period', title_fontsize='13', loc='upper right')
plt.show()

base3.head()

from scipy import stats

 
withsuperspend.head()

base3[base3['crn']=='1000000000000000063']
#-8482206883256382192	

validate=pd.merge(res,withsuperspend, on='crn')

cust_clean=pd.DataFrame(base3.groupby(['crn','offer_status'], as_index=False)['tot_spend'].sum())
res = cust_clean.pivot(index='crn', columns='offer_status', values='tot_spend')

res=pd.merge(res, base3[['crn','cust_type']], on='crn').drop_duplicates()
res2=res[res['cust_type'] !='control']


plt.figure(figsize=(12, 6), tight_layout=True)
ax = sns.scatterplot(x=res2['before offer'], y=res2['after offer'], hue=res2['cust_type'], palette=sns.color_palette('Greens', n_colors=3))#,order=barplot['offer_status'] ) #, palette='pastel', palette=green
ax.set(title='Customer 8 week spend, before and after offer' ,xlabel='before offer spend', ylabel='after offer spend')
ax.legend(title='Campaign period', title_fontsize='13', loc='upper right')
plt.show()


def moreless(x,y):
    if x==y:
        return 'Same'
    if x<y:
        return 'More'
    else:
        return 'Less'
    
res['moreandless']= res.apply(lambda x:moreless(x['before offer'], x['after offer']) ,axis=1)


moreandless=pd.DataFrame(res.groupby(['cust_type','moreandless'], as_index=False).count())

state_office = res.groupby(['cust_type', 'moreandless']).count()
state = res.groupby(['cust_type']).count()
barplot4=state_office.div(state, level='cust_type') * 100

barplot4 = barplot4.drop(['moreandless','crn'],axis=1).reset_index().melt(id_vars=['cust_type'	,'moreandless']).drop_duplicates()
barplot4=barplot4[barplot4['moreandless'] !='Same']


plt.figure(figsize=(12, 6), tight_layout=True)
ax = sns.barplot(x=barplot4['moreandless'], y=barplot4['value'], hue=barplot4['cust_type'], palette=green )#,order=barplot['offer_status'] ) #, palette='pastel'
ax.set(title='Customers that accepted the offer were more likely to spend more' ,xlabel='Experiment group', ylabel='% of group')
ax.legend(title='Customer type', title_fontsize='13', loc='upper right')
plt.show()





agg2['groupbyit']=agg2['cust_type']+' '+ agg2['shop_type']

plt.figure(figsize=(12, 6), tight_layout=True)
ax = sns.lineplot(x=agg2['TXNStartDate'], y=agg2['avg_spend'], hue=agg2['cust_type'], palette=sns.color_palette('Greens', n_colors=3) )#,order=barplot['offer_status'] ) #, palette='pastel', palette=green
ax.set(title='Customer total spend, 8 weeks before and after offer' ,xlabel='Experiment group', ylabel='Count')
ax.legend(title='Campaign period', title_fontsize='13', loc='upper right')
plt.axvline(x='2021-06-23', ymin=0.10, ymax=0.70, color='b', ls='--', lw=1.5, label='axvline - % of full height')
plt.show()

 

(mdates.date2num(datetime.datetime(2021,08,15)), 50)
plt.axvline(x='2021-06-23')

accepted = agg1[(agg1['cust_type'] == 'accepted' & agg1['cust_type'] == 'accepted' )] #['TXNStartDate','cust_type','allocation_status','offer_status']
accepted['groupbyit']=accepted['offer_status']+' '+ accepted['allocation_status']

accepted2=accepted.groupby(['TXNStartDate','groupbyit'])['tot_spend'].agg('mean')

plt.figure(figsize=(12, 6), tight_layout=True)
ax = sns.lineplot(x=accepted['TXNStartDate'], y=accepted['avg_spend'], hue=accepted['groupbyit'], palette='pastel' )#,order=barplot['offer_status'] ) #, palette='pastel', palette=green
ax.set(title='Customer type, before and after offer' ,xlabel='Experiment group', ylabel='Count')
ax.legend(title='Campaign period', title_fontsize='13', loc='upper right')
plt.show()


import datetime
mydate="2021-10-10" 
mydate2="2021-08-15"
aDate ,aDate2= datetime.datetime.strptime(mydate,"%Y-%m-%d"), datetime.datetime.strptime(mydate2,"%Y-%m-%d")
_8Weeks = datetime.timedelta(weeks = 8)

print (aDate + _8Weeks)
print (aDate2 - _8Weeks)

query = """
SELECT c.LoyaltyCardNumber,
sum(case when c.TXNStartDate>="2021-06-20" and c.TXNStartDate<="2021-08-15" then ProductQty else 0 end ) as tot_products_before_offer_2021,
sum(case when c.TXNStartDate>="2020-06-20" and c.TXNStartDate<="2020-08-15" then ProductQty else 0 end ) as tot_products_before_offer_2020,
sum(case when c.TXNStartDate>="2021-06-20" and c.TXNStartDate<="2021-08-15" then TotalAmountIncldTax else 0 end ) as tot_spend_before_offer_2021,
sum(case when c.TXNStartDate>="2020-06-20" and c.TXNStartDate<="2020-08-15" then TotalAmountIncldTax else 0 end ) as tot_spend_before_offer_2020,
sum(case when  c.TXNStartDate>"2021-08-15" then ProductQty else 0 end ) as tot_products_after_offer_2021,
sum(case when c.TXNStartDate>"2020-08-15" and c.TXNStartDate<"2021-01-10" then ProductQty else 0 end ) as tot_products_after_offer_2020,
sum(case when c.TXNStartDate>"2021-08-15" then TotalAmountIncldTax else 0 end ) as tot_spend_after_offer_2021,
sum(case when c.TXNStartDate>"2020-08-15" and c.TXNStartDate<"2021-01-10" then TotalAmountIncldTax else 0 end ) as tot_spend_after_offer_2020,
FROM 
gcp-wow-ent-im-wowx-cust-prod.adp_wowx_dm_integrated_sales_view.article_sales_summary_v as c
WHERE c.LoyaltyCardNumber='-483621050623971481' AND
((c.TXNStartDate>="2020-06-20" AND
c.TXNStartDate<="2020-10-10") OR
(c.TXNStartDate>="2021-06-20" AND
c.TXNStartDate<="2021-10-10"))
group by 1
"""

query = """ drop table if exists akelly.campaignanalysis4"""
pd.read_gbq(query, project_id=project_id)

 

query = """
create table akelly.campaignanalysis4 AS 
SELECT 
c.LoyaltyCardNumber
,a.crn	
,a.aud_type	
,a.send_ind	
,a.act_ind
,a.ActivationType
,a.UserActivationInd
,a.EventmetadataEventtype	
,a.EventmetadataEventname,
sum(case when c.TXNStartDate>="2021-06-20" and c.TXNStartDate<="2021-08-15" then ProductQty else 0 end ) as tot_products_before_offer_2021,
sum(case when c.TXNStartDate>="2020-06-20" and c.TXNStartDate<="2020-08-15" then ProductQty else 0 end ) as tot_products_before_offer_2020,
sum(case when c.TXNStartDate>="2021-06-20" and c.TXNStartDate<="2021-08-15" then TotalAmountIncldTax else 0 end ) as tot_spend_before_offer_2021,
sum(case when c.TXNStartDate>="2020-06-20" and c.TXNStartDate<="2020-08-15" then TotalAmountIncldTax else 0 end ) as tot_spend_before_offer_2020,
sum(case when  c.TXNStartDate>"2021-08-15" then ProductQty else 0 end ) as tot_products_after_offer_2021,
sum(case when c.TXNStartDate>"2020-08-15" and c.TXNStartDate<"2021-01-10" then ProductQty else 0 end ) as tot_products_after_offer_2020,
sum(case when c.TXNStartDate>"2021-08-15" then TotalAmountIncldTax else 0 end ) as tot_spend_after_offer_2021,
sum(case when c.TXNStartDate>"2020-08-15" and c.TXNStartDate<"2021-01-10" then TotalAmountIncldTax else 0 end ) as tot_spend_after_offer_2020,
FROM 
gcp-wow-ent-im-wowx-cust-prod.adp_wowx_dm_integrated_sales_view.article_sales_summary_v as c
 join akelly.campaignanalysis3 as a
ON A.LoyaltyCardNumber=c.LoyaltyCardNumber
WHERE 

((c.TXNStartDate>="2020-06-20" AND
c.TXNStartDate<="2020-10-10") OR
(c.TXNStartDate>="2021-06-20" AND
c.TXNStartDate<="2021-10-10")) AND 
c.SalesOrg in(1030, 1005) AND c.VoidFlag <> 'Y' AND c.POSNumber <> 100
group by 1,2,3,4,5,6,7,8,9
"""
base3=pd.read_gbq(query, project_id=project_id)

base3=base3[base3['crn'] is not None]
base3=base3[base3['crn'] !='None']


base3.groupby(['aud_type', 'UserActivationInd']).count()
base.groupby(['aud_type', 'UserActivationInd']).count()


base3.shape
base3.head()
'1100000000072360650'
'-6824689176242724049'



path='train/'
train_path= 'train_2020_2021.feather'
#df.to_feather(train_path)
df = pd.read_feather(path+ train_path)