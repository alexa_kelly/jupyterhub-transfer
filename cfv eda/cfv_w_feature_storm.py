import pandas as pd
import os
from google.cloud import bigquery
from google.cloud import storage
import pickle
os.environ['GOOGLE_APPLICATION_CREDENTIALS']=f"/home/jovyan/.config/gcloud/legacy_credentials/{os.getenv('JUPYTERHUB_USER')}/adc.json"
import numpy as np
import sys
np.set_printoptions(threshold=sys.maxsize)
pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 500)
from pandas_profiling import ProfileReport
import matplotlib.pyplot as plt
import matplotlib
import seaborn as sns
from matplotlib.ticker import ScalarFormatter
from sklearn.metrics import mean_squared_error
import statsmodels.api as sm
import subprocess
project_id = 'gcp-wow-rwds-ai-cfv-dev'



query = """ 
select * from  akelly.ecom_base_2020_to_2021_2
order by 1 limit 4; """
d=pd.read_gbq(query, project_id=project_id)    
d.head()
1578956
1578956
query = """ 
select count(*) from  akelly.instore_base_2020_to_2021_with_kimchi_with_cust_seg_offer  ; """
pd.read_gbq(query, project_id=project_id)    
d.head()
for i in d.columns:
 print(i)

query = """ 
select count(*) from  akelly.ecom_base_2020_to_20213mooo_with_kimchi3mooo_with_cust_seg_offer
"""
pd.read_gbq(query, project_id=project_id)    
#2026677

d.to_parquet('/home/jovyan/cfv1/sample-data.parquet')

###########################in terminal ###########################
#cd gen-akl-feature-config
#python create-excel-config.py -i /home/jovyan/cfv1/sample-data.parquet -o /home/jovyan/cfv1/feature-config.xlsx -t 10
###########################
import subprocess


path_out="gs://wx-personal/Alexak/cfv/bigw_bws/online/bws_config"
path_out="gs://wx-personal/Alexak/cfv/bigw_bws/instore/bws_config"

path_out="gs://wx-personal/Alexak/cfv/bigw_bws/online/bigw_config"
path_out="gs://wx-personal/Alexak/cfv/bigw_bws/instore/bigw_config"
path_out="gs://wx-personal/Alexak/cfv/supers_new_target/config"
#wx-personal/Alexak/cfv/supers new target/data
#argo_config_class
#wx-personal/Alexak/cfv/supers/instore_enriched/data
featureconfig='/home/jovyan/cfv1/feature-config_3m.xlsx'
subprocess.check_call([f'gsutil -mq cp -r {featureconfig} {path_out}'], shell=True)

 #'argo_config.yaml',
configs=['model_config_v5.yaml']
configs=['model_config_supers2.yaml']

for i in (configs):
    path_in='/home/jovyan/cfv1/'+i
    subprocess.check_call([f'gsutil -mq cp -r {path_in} {path_out}'], shell=True)
    
    
#$ jupyter nbconvert --to script gnn-exercise.ipynb



query = """ select ref_dt,count(*) from feature_build.cfv_app_online_w12  group by 1 order by 1; """

pd.read_gbq(query, project_id=project_id)


query = """ SELECT distinct pw_end_Date FROM gcp-wow-rwds-ai-beh-seg-prod.segmentations.super 
order by 1;
"""

pd.read_gbq(query, project_id=project_id)


   
###############################################################3
# script to install argo when using cloud shell
# Note: this script only need to be run when starting a new cloud shell
###############################################################3

    
    
gcloud container clusters get-credentials project-melon --zone=us-west1-a
#Download Argo
sudo curl -sSL -o /usr/local/bin/argo https://github.com/argoproj/argo-workflows/releases/download/v2.2.0/argo-linux-amd64
sudo chmod +x /usr/local/bin/argo
  
  
argo submit argo_config.yaml
## delete your job
argo delete ak-test-aklrjs2n

## list all jobs
argo list



#****************************


#****************************



from feature_store import FeatureStore
from feature_store.model import Feature
project_id = 'gcp-wow-rwds-ai-cfv-prod'
fs = FeatureStore(project=project_id, env='prod')

fs.management.list_feature_group()
fs.management.list_feature_set('kimchi_digital_features')
fs.management.list_feature_set('behavior_segmentation')
fs.management.list_feature_set('cfv')
fs.management.list_feature_set('cmd3')
fs.management.list_feature_set('digital_features')

#To explore using substring matching, use a list comprehension along the lines of 
[s for s in fs.management.list_feature_set('kimchi_digital_features') if '07' in str(s)]

fs.management.get_feature_set('kimchi_digital_features', 'crn__wow_ecom')
fs.management.get_feature_set('behavior_segmentation', 'super_segmentation')
fs.management.get_feature_set('kimchi_digital_features', 'crn__rapp')

fs.management.get_feature_set('cfv', 'supers_instore_cfv_prod')



[s for s in fs.management.get_feature_set('kimchi_digital_features', 'crn__wow_ecom')['features'] if 'open_rate' in str(s)]

query = """
SELECT column_name, COUNT(1) AS nulls_count
FROM akelly.ecom_base_2020_to_2021_2,
UNNEST(REGEXP_EXTRACT_ALL(TO_JSON_STRING(ecom_base_2020_to_2021_2), r'"(\w+)":null')) column_name
GROUP BY column_name
ORDER BY nulls_count DESC""" 
d=pd.read_gbq(query, project_id=project_id)

d.to_csv('nulls.csv')


query = """ SELECT crn,ecom_tot_spend from akelly.NEW_ecom_base_2020_to_2021_with_kimchi_with_cust_seg_offer order by 1 limit 3 """ 
 


client = bigquery.Client('gcp-wow-rwds-ai-cfv-dev')
table_id='akelly.ecom_log_transformed'

dat.to_gbq(table_id, 
                 project_id,
                 chunksize=10000, 
                 if_exists='append'
                 )
query = """ 
create table akelly.NEW_ecom_base_2020_to_2021_with_kimchi_with_cust_seg_offer_log as select 
a.* except(ecom_tot_spend), b.ecom_tot_spend
from akelly.NEW_ecom_base_2020_to_2021_with_kimchi_with_cust_seg_offer as a join
akelly.ecom_log_transformed as b on a.crn=b.crn
"""

query = """        
create table gcp-wow-rwds-ai-features-test.input.SUPERS_instore_target_sample_100 as 
select * from akelly.SUPERS_instore_target_sample
        order by 1 limit 100; 
 """
d=pd.read_gbq(query, project_id=project_id)


query = """        
select * from akelly.ecom_base_2020_to_2021_with_kimchi_with_cust_seg_offer 
order by 1 limit 30 ; 
 """
d=pd.read_gbq(query, project_id=project_id)


query = """        
select * from akelly.instore_base_2020_to_2021_with_kimchi_with_cust_seg_offer
order by 1 limit 30 ; 
 """
d1=pd.read_gbq(query, project_id=project_id)

query = """ SELECT ref_dt, count(*) as cnt from akelly.instore_base_2020_to_2021_with_kimchi_with_cust_seg_offer group by 1; """
pd.read_gbq(query, project_id=project_id)


'kimchi_digital_features': {'crn__wow_ecom': '*'}
'behavior_segmentation': {'super_segmentation': '*'}
'kimchi_digital_features': {'crn__wow_ecom': '*'},
'cfv': {'supers_instore_cfv': '*'}    
'cmd3': {'b39_31_bigw_txn_time': '*','b06_04_txn_bskt': '*'}      
    

    
d=fs.batch.fetch_features_to_pandas(
    keys=['crn'],
    feature_selection={'cmd3': {'b01_01_membership': '*'}  },
    entity_path='gcp-wow-rwds-ai-features-test.input.SUPERS_instore_target_sample_100',
    entity_file_type=None,
    sparsity_uplimit = 1,
)

d.head()
d.shape
d.columns
d[~d['f__wow_ecom_uniq_prod_add_cart_4w'].isna()]



d=fs.batch.fetch_features_to_pandas(
    keys=['crn'],
    feature_selection={'cmd3': {'b01_01_membership': '*'}  },
    entity_path='gcp-wow-rwds-ai-features-test.input.SUPERS_instore_target_sample_100',
    entity_file_type=None,
    sparsity_uplimit = 1,
)


akelly.instore_base_2020_to_2021


#ADD KIMCHI FEATURES TO BASE 
query = """ 
CREATE TABLE akelly.instore_base_2020_to_2021_with_kimchi as select a.*
,b.* EXCEPT (crn,ref_ts) from akelly.instore_base_2020_to_2021 as a left join 
gcp-wow-rwds-ai-mlt-evs-dev.kimchi_digital_features.crn__wow_ecom 
on a.crn=b.crn and a.ref_dt=DATE(b.ref_ts); """
d=pd.read_gbq(query, project_id=project_id)




#gcp-wow-rwds-ai-mlt-evs-dev.kimchi_digital_features.crn__rapp

query = """ 
select * from 
gcp-wow-rwds-ai-mlt-evs-dev.kimchi_digital_features.crn__rapp 
order by 1 limit 20;  """
d=pd.read_gbq(query, project_id=project_id)


query = """ 
select * from 
gcp-wow-rwds-ai-features-prod.cmd3.b22_17_health
order by 1 limit 20;  """
d=pd.read_gbq(query, project_id=project_id)






query = """ 
select distinct( udo_tealium_event) from `wx-bq-poc.wx_lty_digital_attribution_dev.tealium_events`"""
d2=pd.read_gbq(query, project_id=project_id)
d2.to_csv('telium_Events.csv')


query = """ 
select ref_dt,count(*) as cnt from akelly.instore_base_2020_to_2021_with_kimchi_with_cust 
group by 1; """
pd.read_gbq(query, project_id=project_id)


 
 
     
query = """ 
create  or replace table akelly.SUPERS_instore_target_sample_old_features_with_kimchi_with_cust_supers_seg_ as select * from akelly.SUPERS_instore_target_sample_old_features_with_kimchi_with_cust_supers_seg
where total_SUPERS_instore>=50
; """

query = """ 
select* from akelly.bigw_bws_instore_base_2020_to_2021_with_kimchi_with_cust_seg_offer 
ORDER BY 1 LIMIT 4
; """
d=pd.read_gbq(query, project_id=project_id)

import xlsxwriter

        

df = pd.read_parquet('/home/jovyan/cfv/sample-data.parquet')




d.to_feather('ecom_testing.feather')
data=pd.read_feather('ecom_testing.feather')
for i in d.columns:
 print(i)
 
 
import lazypredict
from sklearn.pipeline import Pipeline
 
ecom_tot_spend

from lazypredict.Supervised import LazyRegressor
from sklearn.utils import shuffle
import numpy as np
from sklearn import preprocessing
le = preprocessing.LabelEncoder()

X, y = shuffle(data.drop(['crn','ref_dt','ecom_tot_spend'],axis=1), data.ecom_tot_spend, random_state=13)

data.info()
mydtypes=pd.DataFrame(data.dtypes).reset_index()
toencode=list(mydtypes[mydtypes[0]=='object']['index'])
toencode.remove('crn')


X=X.fillna(0)
for i in ['gender']:
 X[i]=le.fit_transform(str(X[i]))



X['gender']=le.fit_transform(X['gender'].to_string())


X['gender']=X['gender'].to_string() 
X['gender'].unique()

offset = int(X.shape[0] * 0.9)

X_train, y_train = X[:offset], y[:offset]
X_test, y_test = X[offset:], y[offset:]

reg = LazyRegressor(verbose=0, ignore_warnings=False, custom_metric=None)
models, predictions = reg.fit(X_train, X_test, y_train, y_test)

print(models)



import pandas as pd
import argparse
import pickle
import pyarrow.parquet as pq
import lightgbm as lgb
import numpy as np
import lgbmfit
from akl.preprocessor.numeric import PreprocessorNum
from akl.preprocessor.categorical import PreprocessorCat

import akl

df = pd.read_parquet('/home/jovyan/cfv/sample-data.parquet')

df = pd.read_parquet('gs://wx-lty-cfv-dev/scoring/prod/runs/20220106/output/combined/bws_online_spend_scored.parquet')
df.head()


df0 = pd.read_parquet('gs://wx-lty-cfv-dev/scoring/prod/runs/20220106/output/partition_8/bws_online_spend_scored.parquet')
df0.head()

path= "gs://wx-lty-cfv-dev/scoring/prod/models/bws/online/spend/model_object.pickle"
 

def copy_file(src_dir,dest_dir,filename,do_parallel=True,do_recursive=False, logger=None):
    """
    Copies a file from one location to another using the
    gsutil cp command
    :param src_dir: full source directory path for file
    :param dest_dir: full destination directory path for file
    :param filename: specific file to copy

    :return: None
    """
    try:
        parallel = '-m' if do_parallel else ''
        recursive = '-r' if do_recursive else ''
        if logger:
            logger.info(f'Copying {filename} from: {src_dir} to: {dest_dir}...')
        subprocess.call(f'gsutil {parallel} cp {recursive} {src_dir}/{filename} {dest_dir}',shell=True)
    except Exception as e:
        raise

_model_path= "gs://wx-lty-cfv-dev/scoring/prod/models/bws/online/spend"
_model_object_name="model_object.pickle"

copy_file(_model_path, '.', _model_object_name) 
 
import os
os.getcwd() 
import lightgbm as lgb
loaded_model = pickle.load(open('model_object.pickle', 'rb'))
loaded_model = pd.read_pickle('model_object.pickle')

feats='gs://wx-lty-cfv-dev/scoring/prod/models/bws/online/spend/feature_list.txt'
 
features=pd.read_csv(feats)
features = list(features.values.flatten())


p0=pd.read_parquet('gs://wx-lty-cfv-dev/scoring/prod/runs/20220106/output/partition_'
                       +'0'+'/base_data.parquet')

p0.shape

parts=['3','4','5','6','7','8','9','10','11','12','13','14']
for i in parts:
    p=pd.read_parquet('gs://wx-lty-cfv-dev/scoring/prod/runs/20220106/output/partition_'
                       +i+'/base_data.parquet')
    p_=p.loc[:, p.columns.isin(features)]
    p0.append(p)
    print(pd.DataFrame(p.count()).iloc[:, 0].sum())


pd.set_option('display.max_rows', None)
pd.set_option('display.max_columns', None)
pd.set_option('display.width', None)
pd.set_option('display.max_colwidth', -1)
print(pd.DataFrame(p0.count()))

p0.ref_dt.head()


base='gs://wx-lty-cfv-dev/scoring/prod/runs/20220106/base_audience.parquet'
b=pd.read_parquet(base)
b.head()


base='gs://wx-lty-cfv-dev/scoring/prod/runs/20220102/base_audience.parquet'
b=pd.read_parquet(base)
b.head()


base='gs://wx-lty-cfv-dev/scoring/prod/runs/20211215/base_audience.parquet'
b=pd.read_parquet(base)
b.head()
