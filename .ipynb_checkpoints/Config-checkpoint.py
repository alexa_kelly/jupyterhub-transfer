import os
import pendulum

class BaseConfig:
    
    # Global Configs
    CFV_PROJECT = 'gcp-wow-rwds-ai-cfv-dev'
    CFV_BUCKET = 'wx-lty-cfv-dev'
    CFV_ENV = os.environ.get('CFV_ENV') if os.environ.get('CFV_ENV') else 'dev'
    if CFV_ENV not in ('dev', 'prod', 'train'):
        raise ValueError('Invalid CFV_ENV, choose dev, prod or train')
    if CFV_ENV == 'prod' :
        BQ_ENV = 'score'
        FS_ENV = 'prod'
    elif CFV_ENV == 'dev':
        BQ_ENV = 'dev_score'
        FS_ENV = 'test'
    elif CFV_ENV == 'train':
        BQ_ENV = 'train' 
        FS_ENV = 'test'
    RUN_DATE = os.environ.get('RUN_DATE') if os.environ.get('RUN_DATE') else pendulum.now('Australia/Sydney').format('YYYYMMDD')
    INPUT_SAMPLE_RATIO = float(os.environ.get('INPUT_SAMPLE_RATIO')) if os.environ.get('INPUT_SAMPLE_RATIO') else 1
    NUM_PARTITIONS = int(os.environ.get('NUM_PARTITIONS')) if os.environ.get('NUM_PARTITIONS') else 15
    BASE_CUST_START_DATE = os.environ.get('BASE_CUST_START_DATE') if os.environ.get('BASE_CUST_START_DATE') else pendulum.now('Australia/Sydney').subtract(days=7).format('YYYY-MM-DD')
    BASE_CUST_END_DATE = os.environ.get('BASE_CUST_END_DATE') if os.environ.get('BASE_CUST_END_DATE') else pendulum.now('Australia/Sydney').format('YYYY-MM-DD')

    # GCS Locations
    if CFV_ENV == 'train':
        GCS_ROOT_LOCATION = f'gs://{CFV_BUCKET}/{CFV_ENV}'
    else:
        GCS_ROOT_LOCATION = f'gs://{CFV_BUCKET}/scoring/{CFV_ENV}'
    MODELS_LOCATION = f'{GCS_ROOT_LOCATION}/models'
    BASE_DATA_LOCATION = f'{GCS_ROOT_LOCATION}/runs/{RUN_DATE}'
    EXTRACT_DATA_LOCATION = f'{BASE_DATA_LOCATION}/extract'
    OUTPUT_DATA_LOCATION = f'{BASE_DATA_LOCATION}/output'
    LOGS_LOCATION = f'{BASE_DATA_LOCATION}/logs'
    INSTORE_FINAL_OUTPUT_LOCATION = f'{BASE_DATA_LOCATION}/instore_scored_segmented.parquet'
    ECOM_FINAL_OUTPUT_LOCATION = f'{BASE_DATA_LOCATION}/ecom_scored_segmented.parquet'
    BIGW_INSTORE_FINAL_OUTPUT_LOCATION = f'{BASE_DATA_LOCATION}/bigw_instore_scored_segmented.parquet'
    BIGW_ONLINE_FINAL_OUTPUT_LOCATION = f'{BASE_DATA_LOCATION}/bigw_online_scored_segmented.parquet'
    BWS_INSTORE_FINAL_OUTPUT_LOCATION = f'{BASE_DATA_LOCATION}/bws_instore_scored_segmented.parquet'
    BWS_ONLINE_FINAL_OUTPUT_LOCATION = f'{BASE_DATA_LOCATION}/bws_online_scored_segmented.parquet'

    # Log File Names
    BUILD_BASE_AUDIENCE_LOG = 'build_base_audience.txt'
    EXTRACT_BASE_AUDIENCE_LOG = 'extract_base_audience.txt'
    GET_FEATURE_COLUMNS_LOG = 'get_feature_columns.txt'
    EXTRACT_FEATURES_LOG = 'extract_features.txt'
    COMBINE_FEATURES_LOG = 'combine_features.txt'
    MODEL_SCORER_LOG = 'model_scorer.txt'
    COMBINE_SCORES_LOG = 'combine_scores.txt'
    POST_PROCESSING_LOG = 'postprocessing.txt'
    BQ_UPLOAD_LOG = 'upload_bq.txt'
    SUMMARY_REPORT_LOG = 'summary_report.txt'
    DATE_CHECK_LOG = 'check_date.txt'
    FS_UPLOAD_LOG = 'fs_ingest_features.txt'

    # Model Configurations   
    FEATURE_TABLE_PATH = os.environ.get('FEATURE_TABLE_PATH') if os.environ.get('FEATURE_TABLE_PATH') else f'{MODELS_LOCATION}/feature_tables.txt'
    MODEL_CONFIG_PATH = os.environ.get('MODEL_CONFIG_PATH') if os.environ.get('MODEL_CONFIG_PATH') else f'{MODELS_LOCATION}/model_config.json'
    
    # SQL Paths
    INPUT_SAMPLE_SQL = 'bq_sql/input_sample.sql'
    GET_BASE_AUDIENCE_SQL = 'bq_sql/get_base_audience.sql'
    GET_FEATURE_COLUMNS_SQL = 'bq_sql/get_feature_columns.sql'
    OUTPUT_MERGE_SQL = 'bq_sql/merge_staging_to_output.sql'
    RPT_BASE_AUDIENCE_SQL = 'bq_sql/rpt_base_cust.sql'
    RPT_CFV_SEGMENTS_SQL = 'bq_sql/rpt_cfv_segments.sql'

    # Training Pipeline
    EXCLUDED_COLUMN_NAMES = {'crn', 'ref_dt', 'insert_dttm', 'update_dttm'}

    # Output Filenames
    BASE_AUDIENCE_FILENAME = 'base_audience.parquet'
    PARTITIONS_FILENAME = 'partitions.txt'
    BASE_DATA_FILENAME = 'base_data.parquet'

    # Output BQ_SCHEMA
    STAGING_SCHEMA = {
        "crn": "STRING",
        "ref_dt": "DATE",
        "score_spend": "FLOAT64",
        "score_bernoulli": "FLOAT64",
        "pred_band": "INT64",
        "scaling_factor": "FLOAT64",
        "adjusted_score_spend": "FLOAT64",
        "pred_bin": "STRING",
    }

    INSTORE_BQ_OUTPUT_TABLE = 'cfv_instore_segments'
    ECOM_BQ_OUTPUT_TABLE = 'cfv_ecom_segments'
    BIGW_INSTORE_BQ_OUTPUT_TABLE = 'cfv_bigw_instore_segments'
    BIGW_ONLINE_BQ_OUTPUT_TABLE = 'cfv_bigw_online_segments'
    BWS_INSTORE_BQ_OUTPUT_TABLE = 'cfv_bws_instore_segments'
    BWS_ONLINE_BQ_OUTPUT_TABLE = 'cfv_bws_online_segments'

    # Instore Postprocessing Paths
    INSTORE_POSTPROCESSING_ARGS = {
        'bernoulli_path': f'{OUTPUT_DATA_LOCATION}/combined/instore_bernoulli_scored.parquet', 
        'spend_path': f'{OUTPUT_DATA_LOCATION}/combined/instore_spend_scored.parquet', 
        'n': 50, 
        'scaling_factor_path': f'{MODELS_LOCATION}/instore_spend/scaling_factor.parquet', 
        'dollar_bands_path': f'{MODELS_LOCATION}/instore_spend/dollar_bands.parquet', 
        'output_path': INSTORE_FINAL_OUTPUT_LOCATION,
    }

    # Ecom Postprocessing Paths
    ECOM_POSTPROCESSING_ARGS = {
        'bernoulli_path': f'{OUTPUT_DATA_LOCATION}/combined/ecom_bernoulli_scored.parquet', 
        'spend_path': f'{OUTPUT_DATA_LOCATION}/combined/ecom_spend_scored.parquet', 
        'n': 10, 
        'scaling_factor_path': f'{MODELS_LOCATION}/ecom_spend/scaling_factor.parquet', 
        'dollar_bands_path': f'{MODELS_LOCATION}/ecom_spend/dollar_bands.parquet', 
        'output_path': ECOM_FINAL_OUTPUT_LOCATION,
    }

    # BIGW_Instore Postprocessing Paths
    BIGW_INSTORE_POSTPROCESSING_ARGS = {
        'bernoulli_path': f'{OUTPUT_DATA_LOCATION}/combined/bigw_instore_bernoulli_scored.parquet', 
        'spend_path': f'{OUTPUT_DATA_LOCATION}/combined/bigw_instore_spend_scored.parquet', 
        'n': 37, 
        'scaling_factor_path': f'{MODELS_LOCATION}/bigw/instore/spend/scaling_factor.parquet', 
        'dollar_bands_path': f'{MODELS_LOCATION}/bigw/instore/spend/dollar_bands.parquet', 
        'output_path': BIGW_INSTORE_FINAL_OUTPUT_LOCATION,
    }
    
    # BIGW_Online Postprocessing Paths
    BIGW_ONLINE_POSTPROCESSING_ARGS = {
        'bernoulli_path': f'{OUTPUT_DATA_LOCATION}/combined/bigw_online_bernoulli_scored.parquet', 
        'spend_path': f'{OUTPUT_DATA_LOCATION}/combined/bigw_online_spend_scored.parquet', 
        'n': 3, 
        'scaling_factor_path': f'{MODELS_LOCATION}/bigw/online/spend/scaling_factor.parquet', 
        'dollar_bands_path': f'{MODELS_LOCATION}/bigw/online/spend/dollar_bands.parquet', 
        'output_path': BIGW_ONLINE_FINAL_OUTPUT_LOCATION
    }

    # BWS_Instore Postprocessing Paths
    BWS_INSTORE_POSTPROCESSING_ARGS = {
        'bernoulli_path': f'{OUTPUT_DATA_LOCATION}/combined/bws_instore_bernoulli_scored.parquet', 
        'spend_path': f'{OUTPUT_DATA_LOCATION}/combined/bws_instore_spend_scored.parquet', 
        'n': 27, 
        'scaling_factor_path': f'{MODELS_LOCATION}/bws/instore/spend/scaling_factor.parquet', 
        'dollar_bands_path': f'{MODELS_LOCATION}/bws/instore/spend/dollar_bands.parquet', 
        'output_path': BWS_INSTORE_FINAL_OUTPUT_LOCATION,
    }

    # BWS_Online Postprocessing Paths
    BWS_ONLINE_POSTPROCESSING_ARGS = {
        'bernoulli_path': f'{OUTPUT_DATA_LOCATION}/combined/bws_online_bernoulli_scored.parquet', 
        'spend_path': f'{OUTPUT_DATA_LOCATION}/combined/bws_online_spend_scored.parquet', 
        'n': 0.144,
        'scaling_factor_path': f'{MODELS_LOCATION}/bws/online/spend/scaling_factor.parquet', 
        'dollar_bands_path': f'{MODELS_LOCATION}/bws/online/spend/dollar_bands.parquet', 
        'output_path': BWS_ONLINE_FINAL_OUTPUT_LOCATION,
    }
    # Feature Storm 
    FS_FEATURE_GROUP = 'cfv'
    FS_SUPERS_INSTORE_FEATURE_SET = 'supers_instore_cfv'
    FS_SUPERS_INSTORE_BQ_TABLE = f'{CFV_PROJECT}.{BQ_ENV}.fs_cfv_supers_instore_output'
    FS_SUPERS_ONLINE_FEATURE_SET = 'supers_online_cfv'
    FS_SUPERS_ONLINE_BQ_TABLE = f'{CFV_PROJECT}.{BQ_ENV}.fs_cfv_supers_online_output'

    FS_BIGW_INSTORE_FEATURE_SET = 'bigw_instore_cfv'
    FS_BIGW_INSTORE_BQ_TABLE = f'{CFV_PROJECT}.{BQ_ENV}.fs_cfv_bigw_instore_output'
    FS_BIGW_ONLINE_FEATURE_SET = 'bigw_online_cfv'
    FS_BIGW_ONLINE_BQ_TABLE = f'{CFV_PROJECT}.{BQ_ENV}.fs_cfv_bigw_online_output'

    FS_BWS_INSTORE_FEATURE_SET = 'bws_instore_cfv'
    FS_BWS_INSTORE_BQ_TABLE = f'{CFV_PROJECT}.{BQ_ENV}.fs_cfv_bws_instore_output'
    FS_BWS_ONLINE_FEATURE_SET = 'bws_online_cfv'
    FS_BWS_ONLINE_BQ_TABLE = f'{CFV_PROJECT}.{BQ_ENV}.fs_cfv_bws_online_output'

    # Slackbot Channel
    SLACKBOT_CHANNEL = 'C01SH6DBHMH'

    # CFV SEGMENT RANK ORDERING
    CFV_SEGMENT_RANK = {
        'zero': 0,
        'low': 1,
        'lowmed': 2,
        'med': 3,
        'highmed': 4,
        'high': 5,
    }

    # Read in slackbot token
    with open('/slackbot/slack-bot-token.txt', 'r') as f:
        SLACKBOT_TOKEN = f.read()

Config = BaseConfig()