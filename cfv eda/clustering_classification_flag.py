
import pandas as pd
import os
from google.cloud import bigquery
from google.cloud import storage
import pickle
os.environ['GOOGLE_APPLICATION_CREDENTIALS']=f"/home/jovyan/.config/gcloud/legacy_credentials/{os.getenv('JUPYTERHUB_USER')}/adc.json"
import numpy as np
import sys
from datetime import timedelta
import datetime


#Statistical LTV
from lifetimes import BetaGeoFitter, GammaGammaFitter
from lifetimes.utils import calibration_and_holdout_data, summary_data_from_transaction_data

#ML Approach to LTV
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
import tensorflow_docs as tfdocs
import tensorflow_docs.plots
import tensorflow_docs.modeling

#Evaluation
from sklearn.metrics import r2_score
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import mean_squared_error

#Plotting
import matplotlib.pyplot as plt
import seaborn as sns
np.set_printoptions(threshold=sys.maxsize)
pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 500)
#from pandas_profiling import ProfileReport
from sklearn.preprocessing import StandardScaler
from sklearn.cluster import KMeans

project_id = 'gcp-wow-rwds-ai-cfv-prod'




ecom_spend='gs://wx-lty-cfv-dev/scoring/prod/models/ecom_spend/features.txt'

ecom_spend_feat = pd.read_parquet(ecom_spend)

#query = """drop table if exists akelly.campaignanalysis"""

#CampaignId='29956'
#supermarkets

#ecom_udp_wks_since_last_shop
 


q='drop table if exists akelly.group_for_cluster'
pd.read_gbq(q, project_id=project_id)
query = """CREATE OR REPLACE TABLE akelly.group_for_cluster as
select 
crn,
ref_dt
,total_supers_spend_actual,total_bigw_spend_actual,total_bws_spend_actual 
,tot_spend

,frequency

from  train.sample_base_cust_full_model_group_cav
"""



q='drop table if exists akelly.bigw_instore_and_ecom_for_cluster'
pd.read_gbq(q, project_id=project_id)


query = """CREATE OR REPLACE TABLE akelly.bigw_instore_and_ecom_for_cluster as
select 
crn,
ref_dt,
bigw_instore_tot_spend as tot_spend,
'instore' as flag

from  akelly.bigw_instore_base_2020_to_2021_with_kimchi_with_cust_seg_offer
UNION ALL( SELECT
crn,
ref_dt,
bigw_online_tot_spend as tot_spend ,
'ecom' as flag
from akelly.bigw_online_base_2020_to_2021_with_kimchi_with_cust_seg_offer)
"""

data = pd.read_gbq(query, project_id=project_id)
data.head()



query = """
select 
crn,
ref_dt,
ecom_tot_spend as tot_spend,
ecom_order_frequency as frequency,
'ecom' as flag

from  akelly.ecom_base_2020_to_20213mooo_with_kimchi3mooo_with_cust_seg_offer
"""


query = """
select 
crn,
ref_dt,
ecom_tot_spend as tot_spend,
ecom_order_frequency as frequency,
'ecom' as flag

from  akelly.ecom_base_2020_to_2021_with_kimchi_with_cust_seg_offer
"""



data = pd.read_gbq(query, project_id=project_id)


data = pd.read_gbq('select * from akelly.bws_instore_and_ecom_for_cluster', project_id=project_id)

ecom=data[data.flag!='instore']  
instore=data[data.flag=='instore']  

import pandas as pd
import numpy as np
 
data['tot_spend']=data['total_spend']
 
data['frequency']=data['total_freq']
 
data['tot_spend'].max()



def newbuckets(typeof,data,store):
    data=data[(data['tot_spend']<50000) & (data['tot_spend']>5)]
    if typeof=='all':
        data=data

    elif typeof=='instore':
        data=data[data.flag=='instore'] 
    elif typeof=='ecom':
        data=data[data.flag=='ecom'] 
    if store=='ecom':
        numofcols=4
        labels = ['Low 20%', 'Low Medium 20%', 'High Medium 20%', 'High 20%']
        data['distribution'] = pd.qcut(data['tot_spend'], [0.,.25, .5, .75, 1.], labels=labels)
    if store in ('supers', 'group'):
        numofcols=5
        labels = ['Low 20%', 'Low Medium 20%', 'Medium 20%', 'High Medium 20%', 'High 20%']
        data['distribution'] = pd.qcut(data['tot_spend'], [0.,.2, .4,.6, .8, 1.], labels=labels)
     
    if store=='bigw':
        numofcols=3
        labels = ['Low 33.333%','Medium 33.333%','High 33.333%']
        data['distribution'] = pd.qcut(data['tot_spend'], [0.,.333, .666, 1.], labels=labels)
    if store=='bigw2':
        numofcols=2
        labels = ['Low 50%','High 50%']
        data['distribution'] = pd.qcut(data['tot_spend'], [0.,.5, 1.], labels=labels)
    #top20distribution = data.loc[data['distribution'] == 'High 20%']

    d1=data.groupby(["distribution"])["tot_spend"].agg(['min','mean','max','count'])
    
    d2=data.groupby(["distribution"])['crn'].count()

    fig = plt.figure(figsize=(10,6))
    ax=sns.scatterplot(data = data, x = "tot_spend", y ="frequency",
                       hue="distribution", 
                       #alpha = 0.7,
                      palette=sns.color_palette('Greens',numofcols))
    plt.title('Annual Frequency vs. Expendiure '+ typeof, fontsize=20)
    ax.set_ylim([data["frequency"].min(), data["frequency"].max()])
    ax.set_xlim([data["tot_spend"].min(), data["tot_spend"].max()])
    #ax.title('In store spend performance: True vs. Predicted spend')
    ax.legend(title='Buckett', title_fontsize='13', loc='upper right')
    plt.xlabel('Annual Spend', fontsize=15)
    plt.ylabel('Frequency', fontsize=15)
    ax.ticklabel_format(useOffset=False, style='plain')

    plt.show()
    return round(d1,0),round(d2,0)


   
g1,g2= newbuckets('all',data,'group')
e1,e2= newbuckets('ecom',data,'bigw')
e1,e2= newbuckets('ecom',data,'bigw2')
i1,i2= newbuckets('instore',data,'bigw')
d1,d2= newbuckets('all',data,'bigw')


e1,e2= newbuckets('ecom',data,'ecom')


print(e1)
print(e2)

      
d_bands_instore='gs://wx-lty-cfv-dev/scoring/dev/models/instore_spend/dollar_bands.parquet'
d_bands_instore_prod='gs://wx-lty-cfv-dev/scoring/prod/models/instore_spend/dollar_bands.parquet'
dollar_band_instore = pd.read_parquet(d_bands_instore)
 
dollar_band_instore.iloc[0,1]=i1['max'][0]      
dollar_band_instore.iloc[1,0]=i1['max'][0]   
dollar_band_instore.iloc[1,1]=i1['max'][1]   
dollar_band_instore.iloc[2,0]=i1['max'][1]        
dollar_band_instore.iloc[2,1]=i1['max'][2]        
dollar_band_instore.iloc[3,0]=i1['max'][2] 
dollar_band_instore.iloc[3,1]=i1['max'][3] 
dollar_band_instore.iloc[4,0]=i1['max'][3]     
dollar_band_instore.iloc[4,1]=None

   
dollar_band_ecom.to_parquet(d_bands_instore)     
dollar_band_ecom.to_parquet(d_bands_instore_prod)     
 
 
 
d_bands_ecom='gs://wx-lty-cfv-dev/scoring/dev/models/ecom_spend/dollar_bands.parquet'
d_bands_ecom_prod='gs://wx-lty-cfv-dev/scoring/prod/models/ecom_spend/dollar_bands.parquet'
dollar_band_ecom = pd.read_parquet(d_bands_ecom)
       
       
dollar_band_ecom.iloc[0,1]=e1['max'][0]      
dollar_band_ecom.iloc[1,0]=e1['max'][0]   
dollar_band_ecom.iloc[1,1]=e1['max'][1]   
dollar_band_ecom.iloc[2,0]=e1['max'][1]        
dollar_band_ecom.iloc[2,1]=e1['max'][2]        
dollar_band_ecom.iloc[3,0]=e1['max'][2] 
dollar_band_ecom.iloc[3,1]=e1['max'][3] 
dollar_band_ecom.iloc[4,0]=e1['max'][3]     
dollar_band_ecom.iloc[4,1]=None

   
dollar_band_ecom.to_parquet(d_bands_ecom)     
dollar_band_ecom.to_parquet(d_bands_ecom_prod)         



fig = plt.figure(figsize=(10,6))
sns.kdeplot(instore.tot_spend, color='green', shade=True, Label='In-store')
sns.kdeplot(ecom.tot_spend, color='limegreen', shade=True, Label='E-com')
sns.kdeplot(data.tot_spend, color='springgreen', shade=True, Label='In-store & E-com')

# Setting the X and Y Label
plt.xlabel('Total Spend - In-store vs E-com')
plt.ylabel('Probability Density')
plt.title('Total distribution', fontsize=20)
fig.legend(labels=['In-store','E-com','In-store & E-com'])

plt.show()




data.groupby(['flag'])['tot_spend'].agg(['count','min','max','mean'])

 


query = """select spend_range, count(*), count(distinct(crn)) from akelly.sample_base_cust_full_model_2020_to_2021_instore group by 1; 
"""

check3 = pd.read_gbq(query, project_id=project_id)

check['f0_'].sum()

query = """with 
CRN_REF_DATES AS
(
    SELECT
         bse.crn
        ,bse.ref_dt
        ,bse.spend_range
        ,RANK() OVER (PARTITION BY bse.crn ORDER BY spend_range desc) RankNum,
        ROW_NUMBER() OVER (PARTITION BY bse.crn ) as RankNum2
    FROM
        `gcp-wow-rwds-ai-cfv-dev.akelly.temp_base_cust_2020_to_2021_instore` bse
        #where bse.crn not in (select crn from gcp-wow-rwds-ai-cfv-dev.akelly.sample_base_cust_full_model_2020_to_2021_ecom)
)

SELECT 
     crn
    ,ref_dt,spend_range,d.RankNum,d.seqnum,d.RankNum2
FROM  (
        select d.spend_range,d.crn,d.ref_dt,d.RankNum,d.RankNum2,
              row_number() over (partition by spend_range ORDER BY spend_range desc) as seqnum
      from CRN_REF_DATES d WHERE d.RankNum = 1
      order by rand()
     ) d 
WHERE crn='1100000000004900515'; """

check2 = pd.read_gbq(query, project_id=project_id)



check.to_csv('check.csv')


data2.head()
data.shape


data2=data[data.tot_spend.notnull()]
data=data2[data2.tot_spend >0].reset_index()
data.to_feather('econandinstore_2020.feather')
data=pd.read_feather('econandinstore_2020.feather')
#instore=data[data.flag!='instore']  
instore=data 
instore=data[data.flag!='instore']     
tot_spend_all=instore[['tot_spend']].fillna(0).astype(int)


scaler = StandardScaler()
scaled_features = scaler.fit_transform(instore[['tot_spend','frequency']])



kmeans_kwargs = {
"init": "random",
"n_init": 10,
 "max_iter": 300,
"random_state": 42,
}

sse = []
for k in range(1, 11):
    kmeans = KMeans(n_clusters=k, **kmeans_kwargs)
    kmeans.fit(tot_spend_all)
    sse.append(kmeans.inertia_)
    

    
from kneed import KneeLocator

kl = KneeLocator(
    range(1, 11), sse, curve="convex", direction="decreasing"
)


plt.style.use("fivethirtyeight")
plt.plot(range(1, 11), sse)
plt.xticks(range(1, 11))
plt.axvline(x=kl.elbow,color='red')
plt.ticklabel_format(useOffset=False)
plt.title('Squared estimate of errors by cluster', fontsize=15)
plt.xlabel("Number of Clusters")
plt.ylabel("SSE")
plt.show()



def cluster(typeof):
    if typeof=='instore':
        instore=data[data.flag=='instore'] 
    elif typeof=='ecom':
        instore=data[data.flag!='instore'] 
    elif typeof=='all':
        instore=data
     
    tot_spend_all=instore[['tot_spend']].fillna(0).astype(int)

    kmeans = KMeans(n_clusters=5)
    kmeans.fit(tot_spend_all)
    instore['tot_spend_frq_Cluster'] = kmeans.predict(tot_spend_all)
    return instore 

instore=cluster('instore')
ecom=cluster('ecom')
ecom.groupby(["tot_spend_frq_Cluster"])["tot_spend"].agg(['min','mean','max','count'])
ecom.groupby(["tot_spend_frq_Cluster"])['crn'].count()

allof=cluster('all')
allof.groupby(["tot_spend_frq_Cluster"])["tot_spend"].agg(['min','mean','max','count'])
allof.groupby(["tot_spend_frq_Cluster"])['crn'].count()


import matplotlib.pyplot as plt
import seaborn as sns
instore.groupby(["tot_spend_frq_Cluster"])["tot_spend"].agg(['min','mean','max','count'])
instore.groupby(["tot_spend_frq_Cluster"])['crn'].count()
colors = ["palegreen", "limegreen", "SpringGreen"]
# Set your custom color palette
plt.figure(figsize=(10,10))




ax=sns.scatterplot(data = instore, x = "tot_spend", y ="frequency",
                   hue="tot_spend_frq_Cluster", 
                   alpha = 0.7,
                  palette=sns.color_palette('Greens',5))
plt.title('INSTORE Annual Frequency vs. Expendiure (10/20-10/21)', fontsize=20)
ax.set_ylim([instore["frequency"].min(), instore["frequency"].max()])
#ax.set_xlim([data["tot_spend"].min(), data["tot_spend"].max()])
#ax.title('In store spend performance: True vs. Predicted spend')
ax.legend(title='Cluster', title_fontsize='13', loc='upper right')
plt.xlabel('Annual Spend', fontsize=15)
plt.ylabel('Frequency', fontsize=15)
ax.ticklabel_format(useOffset=False, style='plain')
 
plt.show()
 

  
  
p1 = max(max(data['frequency']), max(data['tot_spend']))
p2 = min(min(data['frequency']), min(data['tot_spend']))
plt.plot([p1, p2], [p1, p2], 'b-')



pd.read_gbq("""drop table if exists akelly.ecom_model_trans""", project_id=project_id)

query = """
create table akelly.ecom_model_trans as 
with loyalty as ( 
select b.PrimaryCustomerRegistrationNumber as crn, b.LoyaltyCardNumber ,a.ref_dt,min(LoyaltyCardRegistrationDate) as lylty_card_rgstr_date
from `gcp-wow-ent-im-wowx-cust-prod`.adp_wowx_dm_customer_view.loyalty_card_detail_v b join
akelly.sample_ecom_cust_full_model_2020_to_2021 as a  on a.crn=b.PrimaryCustomerRegistrationNumber
WHERE 
b.LoyaltyCardStatusDescription not in ('Cancelled', 'Closed', 'Deregistered')
group by 1,2,3)

select 
l.crn,
l.ref_dt,
b.LoyaltyCardNumber,
b.TXNStartDate,
b.Site,
min(EXTRACT(HOUR from b.TXNStartTime) )as hour,
sum(b.ProductQty) as tot_pro,
sum(b.TotalAmountIncldTax) as tot_spend
from gcp-wow-ent-im-wowx-cust-prod.adp_wowx_dm_integrated_sales_view.article_sales_summary_v as b join loyalty as l 
on b.LoyaltyCardNumber = l.LoyaltyCardNumber
where
b.TXNStartDate  between  DATE_SUB(current_date(), INTERVAL 6 MONTH) and current_date()
                AND b.VoidFlag <> 'Y' #NOT VOID
                AND b.SalesOrg IN (1005,1030)
                AND b.SalesChannelCode=2
                AND b.POSNumber = 100 # not tot_sales_excld_gst
group by 1,2,3,4,5

"""

data = pd.read_gbq(query, project_id=project_id)
data.shape

data['crn'].nunique()

data['TXNStartDate'].nunique()



query = """
select * from akelly.sample_1k_cust_full_model limit 3
"""
data.to_feather('ecom6months.feather')
data = pd.read_gbq(query, project_id=project_id)

data.head()
data.info()
data['tot_spend']=data['tot_spend'].astype('int')
data['weekend'] = data['TXNStartDate'].apply(lambda x: x.weekday() in [5, 6])
data['dayofweek'] = data['TXNStartDate'].apply(lambda x: x.dayofweek)
#Get revenue column
 
print(data.sample(5))

#Plots a timeseries of total sales
data.groupby('TXNStartDate')['tot_spend'].sum().plot()

data.groupby('TXNStartDate')['tot_pro'].sum().plot()

#Prints the total number of days between start and end
print(data['TXNStartDate'].max() - data['TXNStartDate'].min())

mid=(data['TXNStartDate'].max() - data['TXNStartDate'].min())/2

feature_start= data['TXNStartDate'].min()
feature_end= data['TXNStartDate'].min() +mid
target_start=feature_end+ timedelta(days=1) 
target_end=data['TXNStartDate'].max()

#data.to_feather('data1ksample.feather')
data.shape
data2.shape


data['tot_spend'].min()
data['tot_spend'].max()
data2=data[data['tot_spend'] <=1000]
 

from sklearn.model_selection import train_test_split
from sklearn.model_selection import GroupShuffleSplit
train, test = train_test_split(data, test_size=0.33, random_state=27)

train_inds, test_inds = next(GroupShuffleSplit(test_size=.20, n_splits=2, random_state = 7).split(data, groups=data['crn']))

train = data.iloc[train_inds]
test = data.iloc[test_inds]



def order_cluster(cluster_field_name, target_field_name,df,ascending):
    new_cluster_field_name = 'new_' + cluster_field_name
    df_new = df.groupby(cluster_field_name)[target_field_name].mean().reset_index()
    df_new = df_new.sort_values(by=target_field_name,ascending=ascending).reset_index(drop=True)
    df_new['index'] = df_new.index
    df_final = pd.merge(df,df_new[[cluster_field_name,'index']], on=cluster_field_name)
    df_final = df_final.drop([cluster_field_name],axis=1)
    df_final = df_final.rename(columns={"index":cluster_field_name})
    return df_final
#t = data.groupby('crn')['TXNStartDate'].min().apply(lambda x: (data['TXNStartDate'].min() - x).days).rename('t')
def get_features(data, feature_start, feature_end, target_start, target_end):

    #Double check the periods length
    features_data = data.loc[(data.TXNStartDate >= feature_start) & (data.TXNStartDate <= feature_end), :]
    print(f'Using data from {(pd.to_datetime(feature_end) - pd.to_datetime(feature_start)).days} days')
    print(f'To predict {(pd.to_datetime(target_end) - pd.to_datetime(target_start)).days} days')
    total_rfdt = features_data.groupby('crn')['ref_dt'].min().rename('ref_dt')
    #Transactions data features
    total_rev = features_data.groupby('crn')['tot_spend'].sum().rename('total_tot_spend')
    recency = (features_data.groupby('crn')['TXNStartDate'].max() - features_data.groupby('crn')['TXNStartDate'].min()).apply(lambda x: x.days).rename('recency')
    frequency = features_data.groupby('crn')['TXNStartDate'].count().rename('frequency')
    t = features_data.groupby('crn')['TXNStartDate'].min().apply(lambda x: (feature_end - x).days).rename('t')
    time_between = (t / frequency).rename('time_between')
    avg_basket_value = (total_rev / frequency).rename('avg_basket_value')
    avg_basket_size = (features_data.groupby('crn')['tot_pro'].sum() / frequency).rename('avg_basket_Size')
    returns = features_data.loc[features_data['tot_spend'] < 0, :].groupby('crn')['TXNStartDate'].count().rename('num_returns')
    hour = features_data.groupby('crn')['hour'].median().rename('purchase_hour_med')
    dow = features_data.groupby('crn')['dayofweek'].median().rename('purchase_dow_med')
    weekend =  features_data.groupby('crn')['weekend'].mean().rename('purchase_weekend_prop')
    #train_data = pd.DataFrame(index = features_data.index)
   # train_data = train_data.join([total_rev, recency, frequency, t, time_between, avg_basket_value, avg_basket_size, returns, hour, dow, weekend])
    train_data = pd.concat([total_rev, recency, frequency, t, time_between, avg_basket_value, avg_basket_size, returns, hour, dow, weekend], axis=1)

    train_data = train_data.fillna(0)
    
    #Target data
    target_data = data.loc[(data.TXNStartDate >= target_start) & (data.TXNStartDate <= target_end), :]
    target_quant = target_data.groupby(['crn'])['TXNStartDate'].nunique()
    target_rev = target_data.groupby(['crn'])['tot_spend'].sum().rename('target_rev')
    train_data = train_data.join(target_rev).fillna(0)
    train_all=pd.concat([train_data,total_rfdt], axis=1)
    kmeans = KMeans(n_clusters=4)
    kmeans.fit(train_data[['recency']])
    train_data['RecencyCluster'] = kmeans.predict(train_data[['recency']])

    train_data = order_cluster('RecencyCluster', 'recency',train_data,False)


    kmeans = KMeans(n_clusters=4)
    kmeans.fit(train_data[['frequency']])
    train_data['FrequencyCluster'] = kmeans.predict(train_data[['frequency']])

    train_data = order_cluster('FrequencyCluster', 'frequency',train_data,True)

    kmeans = KMeans(n_clusters=4)
    kmeans.fit(train_data[['total_tot_spend']])
    train_data['RevenueCluster'] = kmeans.predict(train_data[['total_tot_spend']])

    train_data = order_cluster('RevenueCluster', 'total_tot_spend',train_data,True)

    train_data['OverallScore'] = train_data['RecencyCluster'] + train_data['FrequencyCluster'] + train_data['RevenueCluster']
    train_data['Segment'] = 'Low-Value'
    train_data.loc[train_data['OverallScore']>2,'Segment'] = 'Mid-Value' 
    train_data.loc[train_data['OverallScore']>4,'Segment'] = 'High-Value' 


    train_data = train_data[train_data['total_tot_spend']<train_data['total_tot_spend'].quantile(0.99)]

    kmeans = KMeans(n_clusters=3)
    kmeans.fit(train_data[['target_rev']])
    train_data['LTVCluster'] = kmeans.predict(train_data[['target_rev']])
    train_data=train_data.drop(['t'],axis=1)
    train_data = pd.get_dummies(train_data)
    X = train_data.drop(['LTVCluster','target_rev'],axis=1)
    y = train_data['LTVCluster']
    
    return train_data,X,y
 
#X_train, y_train ,train_all= get_features(train, feature_start, feature_end, target_start, target_end)
#X_test, y_test ,test_all= get_features(test,feature_start, feature_end, target_start, target_end)

train_out,X_train,y_train=get_features(train, feature_start, feature_end, target_start, target_end)
test_out,X_test,y_test=get_features(test, feature_start, feature_end, target_start, target_end)

train_out.reset_index().to_feather('trainout.feather')
import xgboost as xgb
from sklearn.metrics import classification_report,confusion_matrix

#create X and y, X will be feature set and y is the label - LTV


ltv_xgb_model = xgb.XGBClassifier(max_depth=5, learning_rate=0.001,objective= 'multi:softprob',n_jobs=-1).fit(X_train, y_train)

print('Accuracy of XGB classifier on training set: {:.2f}'
       .format(ltv_xgb_model.score(X_train, y_train)))
print('Accuracy of XGB classifier on test set: {:.2f}'
       .format(ltv_xgb_model.score(X_test[X_train.columns], y_test)))

y_pred = ltv_xgb_model.predict(X_test)
print(classification_report(y_test, y_pred))

#gs://wx-personal/Alexak/akl test/data/train_all.parquet

train_all.reset_index(level=0, inplace=True)
train_all.head()
X_train.head()
X_train.shape
from sklearn import preprocessing
scaler = preprocessing.StandardScaler().fit(X_train)
X_train_scaled = scaler.transform(X_train)
X_test_scaled = scaler.transform(X_test)


train_out['target_rev'].shape
X_train.shape

def build_model():
    model = keras.Sequential([
    layers.Dense(256, activation='relu', input_shape=[len(X_train.columns), ]),
    layers.Dropout(0.3),
    layers.Dense(64, activation='relu'),
    layers.Dropout(0.3),
    layers.Dense(32, activation='relu'),
    layers.Dense(1)
    ])

    optimizer = tf.keras.optimizers.Adam(0.001)

    model.compile(loss='mse',
            optimizer=optimizer,
            metrics=['mae', 'mse'])
    
    return model

# The patience parameter is the amount of epochs to check for improvement
early_stop = keras.callbacks.EarlyStopping(monitor='val_mse', patience=50)

model = build_model()
#Should take 10 sec
early_history = model.fit(X_train, train_out['target_rev'], 
                    epochs=1000, validation_split = 0.2, verbose=0, 
                    callbacks=[early_stop, tfdocs.modeling.EpochDots()])



def evaluate(actual, sales_prediction):
    print(f"Total Sales Actual: {np.round(actual.sum())}")
    print(f"Total Sales Predicted: {np.round(sales_prediction.sum())}")
    print(f"Individual R2 score: {r2_score(actual, sales_prediction)} ")
    print(f"Individual Mean Absolute Error: {mean_absolute_error(actual, sales_prediction)}")
    print(f"Individual Root Mean Squared Error: {mean_squared_error(actual, sales_prediction)}")

    plt.scatter(sales_prediction, actual)
    plt.xlabel('Prediction')
    plt.ylabel('Actual')      
    plt.show()

#Predicting
dnn_preds = model.predict(X_test).ravel()

evaluate(test_out['target_rev'], dnn_preds)

 

train_out[train_out['target_rev']==0].count()
train_out[train_out['target_rev']!=0].count()
    
X_train[X_train['total_tot_spend']==0].count()
X_train[X_train['total_tot_spend']!=0].count()
      
    
project='wx-personal'
bucketname='Alexak/aktest.csv'

from io import StringIO # if going with no saving csv file

f = StringIO()
df.to_csv(f)
f.seek(0)
gcs.get_bucket(project).blob(bucketname).upload_from_file(f, content_type='text/csv')
