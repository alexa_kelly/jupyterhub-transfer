import pandas as pd
import numpy as np
import tensorflow as tf
import os
import torch
import math
import datetime
import gc
from joblib import Parallel, delayed
from tqdm import tqdm  
from transformers import pipeline, DistilBertModel, BertModel, BertConfig, DistilBertTokenizer, BertTokenizer, AutoTokenizer, AlbertTokenizer, AlbertModel,AlbertConfig, MobileBertModel, MobileBertTokenizer

os.environ['GOOGLE_APPLICATION_CREDENTIALS']=f"/home/jovyan/.config/gcloud/legacy_credentials/{os.getenv('JUPYTERHUB_USER')}/adc.json"
#https://bitbucket.org/wx_rds/survey_sentiment_modelling/src/e8b8d900e967f2dff683b7a000ce99cecfc55b6b/models/BERT/bert-embeddings-extract.ipynb?at=master&viewer=nbviewer


df = pd.read_parquet('gs://wx-projects/2021/Chapter/Targeted-Marketing/sentiment-modelling/data/base_crn_survey_resp_v3.parquet')


df.shape
df.head()

df = df.sample(n = 100000)

comment_cols = [col for col in df.columns if df[col].dtype == 'object' and df[col].nunique() >=10 and col not in ['crn', 'ref_dt', 'combined_string', 'general_feedback', 'camp_avg_spd']]


##Fix text
df['combined_string'] = df[comment_cols].astype(str).replace('', 'None').add('. ').replace('None. ', '').sum(1).str.strip()
df['general_feedback'] = df[['OVERALL_SATISFACTION_COMMENT', 'FINAL_FEEDBACK_COMMENT', 'FINAL_FEEDBACK']].astype(str).replace('', 'None').add('. ').replace('None. ', '').sum(1).str.strip()


df_sub = df[~(df['combined_string'] == '')].reset_index(drop = True)

model_class, tokenizer_class, pretrained_weights = (MobileBertModel, MobileBertTokenizer, 'google/mobilebert-uncased')

tokenizer = tokenizer_class.from_pretrained(pretrained_weights)
model = model_class.from_pretrained(pretrained_weights)

batch_size = 4096
n_rows = df_sub.shape[0]
df_sampled_list = [df_sub['combined_string'].iloc[i*batch_size:(i+1)*batch_size] for i in range(math.ceil(n_rows/batch_size))]


def parallel_tokenize(df):
    tokens = df.apply((lambda x: tokenizer.encode(x, add_special_tokens=True, max_length = 100, padding = 'max_length', truncation = True)))
    return tokens

df_tokenized = Parallel(n_jobs=8)(
    delayed(parallel_tokenize)(df_sampled) for df_sampled in tqdm(df_sampled_list)
)
tokenized = pd.concat(df_tokenized, ignore_index=False, axis=0)


padded = np.array([i for i in tokenized.values])


attention_mask = np.where(padded != 0, 1, 0)
attention_mask.shape

torch.cuda.get_device_name(0)



torch.cuda.is_available()
device = torch.device("cuda")
model.to(device)
