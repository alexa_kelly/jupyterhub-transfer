
import pandas as pd
import os
from google.cloud import bigquery
from google.cloud import storage
import pickle
os.environ['GOOGLE_APPLICATION_CREDENTIALS']=f"/home/jovyan/.config/gcloud/legacy_credentials/{os.getenv('JUPYTERHUB_USER')}/adc.json"
import numpy as np
import sys
np.set_printoptions(threshold=sys.maxsize)
pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 500)
import matplotlib.pyplot as plt
import seaborn as sns
from matplotlib.ticker import ScalarFormatter
from sklearn.metrics import mean_squared_error
import math
import json
import seaborn as sns
from sklearn.metrics import confusion_matrix
import sklearn.metrics as skm
sns.set_style('darkgrid') # darkgrid, white grid, dark, white and ticks
plt.rc('axes', titlesize=18)     # fontsize of the axes title
plt.rc('axes', labelsize=14)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=13)    # fontsize of the tick labels
plt.rc('ytick', labelsize=13)    # fontsize of the tick labels
plt.rc('legend', fontsize=13)    # legend fontsize
plt.rc('font', size=13)          # controls default text sizes
green = sns.color_palette("Greens")


def unlog(dat):
    dat['target']=dat['target'].apply(np.exp)
    dat['pred']=dat['pred'].apply(np.exp)
    return dat



# def classification_transform_bigw(var):
#     if var>=0 and var<=121.0:
#         return r"Low"
#     elif var>121.0 and var<=418.0:
#         return 'Medium'
#     elif var>418.0:
#         return 'High'
#     else:
#         return var  
     
# def classification_transform_ecom_bigw(var):
#     if var>=0 and var<=97.0:
#         return r"Low"
#     elif var>97.0 and var<=285.0:
#         return 'Medium'
#     elif var>285.0:
#         return 'High'
#     else:
#         return var    
   
def classification_transform_bws(var):
    if var>=0 and var<=113.0:
        return r"Low"
    elif var>113.0 and var<=757.0:
        return 'Medium'
    elif var>757.0:
        return 'High'
    else:
        return var  
     
def classification_transform_ecom_bws(var):
    if var>=0 and var<=583.0:
        return r"Low"
    elif var>583.0:
        return 'High'
    else:
        return var    
 
def classification_transform_bigw(var):
    if var>=0 and var<=243.0:
        return r"Low"
    elif var>243.0 and var<=697.0:
        return 'Medium'
    elif var>697.0:
        return 'High'
    else:
        return var  
     
def classification_transform_ecom_bigw(var):
    if var>=0 and var<=311.0:
        return r"Low"
    elif var>311.0:
        return 'High'
    else:
        return var    
     
     
     
def classification_transform(var):
    if var>=0 and var<=1930.0:
        return r"Low"
    elif var>1930.0 and var<=3977.0:
        return 'Low Medium'
    elif var>3977.0 and var<=5955.0:
        return 'Medium'
    elif var>5955.0 and var<=8194.0:
        return 'High Medium'
    elif var>8194.0:
        return 'High'
    else:
        return var  

def classification_transform_ecom(var):
    if var>=0 and var<=599.0:
        return r"Low"
    elif var>599.0 and var<=1725.0:
        return 'Low Medium'
    elif var>1725.0 and var<=3516.0:
        return 'Medium'
    elif var>3516.0 and var<=6724.0:
        return 'High Medium'
    elif var>6724.0:
        return 'High'
    else:
        return var     
     
   
     
def classification_transform_ecom3m(var):
    if var>=0 and var<=180.0:
        return r"Low"
    elif var>180.0 and var<=495.0:
        return 'Low Medium'
    elif var>495.0 and var<=1156.0:
        return 'High Medium'
    elif var>1156.0:
        return 'High'
    else:
        return var      
      
def formatscored(dat):
    holdout=dat[['crn', 'ref_dt','target','pred']]
    holdout['year'] = pd.DatetimeIndex(holdout['ref_dt']).year
    holdout['month'] = pd.DatetimeIndex(holdout['ref_dt']).month
    holdout['yyyy_mm']=pd.to_datetime(holdout[['year', 'month']].assign(DAY=1))
    return holdout
       
def root_mean_square_error(targets, predictions):
    return np.sqrt(np.mean((predictions-targets)**2))
 
#T is test or holdout, check config 

def scaleit(df,path2):
    scaling_factor=pd.read_parquet(path2)
    
    df['pred_band'] = pd.qcut(df['pred'] ,q=100).cat.rename_categories(list(range(1, 101)))
    df['pred_band'] = df['pred_band'].cat.codes
    df['pred_band'] = df['pred_band'] + 1
    
    df = (df
    .merge(scaling_factor[['pred_band', 'scaling_factor']], how='left', on=['pred_band'])
    )
    df['pred'] = df['pred'] * df['scaling_factor']
    df['pred'] = df['pred'].fillna(-1)
    return df

def importit(path,typeof,yr,scale=False,path2='',transform='none'):
    
    scored_M ='/scoring/scored_M.parquet'
    scored_M = pd.read_parquet(path+scored_M)
    scored_T ='/scoring/scored_T.parquet'
    scored_T = pd.read_parquet(path+scored_T)   

    holdout=formatscored(scored_T)
    trained_on=formatscored(scored_M)     
    if scale==True:
     holdout=scaleit(holdout,path2)
     trained_on=scaleit(trained_on,path2)
    else:
        pass   
    if transform=='log':  
        trained_on=unlog(trained_on)
        holdout=unlog(holdout)
    else:
        pass

    trained_on['RMSE'] = trained_on.apply(lambda x: root_mean_square_error(x['target'], x['pred']), axis=1)
    mse = mean_squared_error(trained_on['target'],trained_on['pred'])
    rmse = math.sqrt(mse)
    print(typeof, ' rmse:', rmse)
    if typeof=='ecom':
        title="Ecom spend performance: True vs. Predicted spend"
        trained_on['target_class'],trained_on['pred_class']=trained_on['target'].apply(classification_transform_ecom),trained_on['pred'].apply(classification_transform_ecom)
        holdout['target_class'],holdout['pred_class']=holdout['target'].apply(classification_transform_ecom),holdout['pred'].apply(classification_transform_ecom)
    elif typeof=='ecom3m':
        title="Ecom spend performance: True vs. Predicted spend"
        trained_on['target_class'],trained_on['pred_class']=trained_on['target'].apply(classification_transform_ecom3m),trained_on['pred'].apply(classification_transform_ecom3m)
        holdout['target_class'],holdout['pred_class']=holdout['target'].apply(classification_transform_ecom3m),holdout['pred'].apply(classification_transform_ecom3m)
    elif typeof=='ecom4':
        title="Ecom spend performance: True vs. Predicted spend"
        trained_on['target_class'],trained_on['pred_class']=trained_on['target'].apply(classification_transform_ecom4),trained_on['pred'].apply(classification_transform_ecom4)
        holdout['target_class'],holdout['pred_class']=holdout['target'].apply(classification_transform_ecom4),holdout['pred'].apply(classification_transform_ecom4)
    elif typeof=='instore':
        title="Instore spend performance: True vs. Predicted spend"
        trained_on['target_class'],trained_on['pred_class']=trained_on['target'].apply(classification_transform),trained_on['pred'].apply(classification_transform)
        holdout['target_class'],holdout['pred_class']=holdout['target'].apply(classification_transform),holdout['pred'].apply(classification_transform)
    elif typeof=='ecom_bigw':
        title="Big W Ecom spend performance: True vs. Predicted spend"
        trained_on['target_class'],trained_on['pred_class']=trained_on['target'].apply(classification_transform_ecom_bigw),trained_on['pred'].apply(classification_transform_ecom_bigw)
        holdout['target_class'],holdout['pred_class']=holdout['target'].apply(classification_transform_ecom_bigw),holdout['pred'].apply(classification_transform_ecom_bigw)
    elif typeof=='instore_bigw':
        title="Big W Instore spend performance: True vs. Predicted spend"
        trained_on['target_class'],trained_on['pred_class']=trained_on['target'].apply(classification_transform_bigw),trained_on['pred'].apply(classification_transform_bigw)
        holdout['target_class'],holdout['pred_class']=holdout['target'].apply(classification_transform_bigw),holdout['pred'].apply(classification_transform_bigw)
    elif typeof=='ecom_bws':
        title="BWS Ecom spend performance: True vs. Predicted spend"
        trained_on['target_class'],trained_on['pred_class']=trained_on['target'].apply(classification_transform_ecom_bws),trained_on['pred'].apply(classification_transform_ecom_bws)
        holdout['target_class'],holdout['pred_class']=holdout['target'].apply(classification_transform_ecom_bws),holdout['pred'].apply(classification_transform_ecom_bws)
    elif typeof=='instore_bws':
        title="BWS Instore spend performance: True vs. Predicted spend"
        trained_on['target_class'],trained_on['pred_class']=trained_on['target'].apply(classification_transform_bws),trained_on['pred'].apply(classification_transform_bws)
        holdout['target_class'],holdout['pred_class']=holdout['target'].apply(classification_transform_bws),holdout['pred'].apply(classification_transform_bws)
       
        
    if yr=='2019':
        title=title+' v.2019'
    else:
        title=title+' v.2021'
    plt.figure(figsize=(10,10))
    plt.scatter(trained_on['target'],trained_on['pred'], c='crimson')
    ax = plt.gca()


    plt.yscale('log')
    plt.xscale('log')
    for axis in [ax.xaxis, ax.yaxis]:
        axis.set_major_formatter(ScalarFormatter())
    ax.ticklabel_format(useOffset=False, style='plain')
    #plt.ticklabel_format(useOffset=False, style='plain')
    p1 = max(max(trained_on['pred']), max(trained_on['target']))
    p2 = min(min(trained_on['pred']), min(trained_on['target']))
    plt.plot([p1, p2], [p1, p2], 'b-')
    plt.title(title, fontsize=20)
    #ax.title('In store spend performance: True vs. Predicted spend')
    plt.xlabel('True Values (log)', fontsize=15)
    plt.ylabel('Predictions (log)', fontsize=15)
    plt.axis('equal')
    plt.show()
    
    plt.figure(figsize=(10,10))
    plt.scatter(trained_on['target'],trained_on['pred'], c='crimson')
    ax = plt.gca()

    for axis in [ax.xaxis, ax.yaxis]:
        axis.set_major_formatter(ScalarFormatter())
    ax.ticklabel_format(useOffset=False, style='plain')
    #plt.ticklabel_format(useOffset=False, style='plain')
    p1 = max(max(trained_on['pred']), max(trained_on['target']))
    p2 = min(min(trained_on['pred']), min(trained_on['target']))
    plt.plot([p1, p2], [p1, p2], 'b-')
    plt.title(title, fontsize=20)
    #ax.title('In store spend performance: True vs. Predicted spend')
    plt.xlabel('True Values', fontsize=15)
    plt.ylabel('Predictions', fontsize=15)
    plt.axis('equal')
    plt.show()
    
    # Plotting the KDE Plot
    fig = plt.figure(figsize=(10,6))

    sns.kdeplot(trained_on['target'], color='green', shade=True, Label='Target')
    sns.kdeplot(trained_on['pred'], color='limegreen', shade=True, Label='Predicted')

    # Setting the X and Y Label
    plt.xlabel('Train Total Spend')
    plt.ylabel('Probability Density')
    plt.title('Total distribution', fontsize=20)
    fig.legend(labels=['Target','Predicted'])

    plt.show()

    fig = plt.figure(figsize=(10,6))

    sns.kdeplot(holdout['target'], color='green', shade=True, Label='Target')
    sns.kdeplot(holdout['pred'], color='limegreen', shade=True, Label='Predicted')

    # Setting the X and Y Label
    plt.xlabel('Test Total Spend')
    plt.ylabel('Probability Density')
    plt.title('Total distribution', fontsize=20)
    fig.legend(labels=['Target','Predicted'])

    plt.show()

    return trained_on,holdout 

def produce_metrics(path, title):
    test=path+'/scoring/scored_M.parquet'
    train=path+'/scoring/scored_T.parquet'       

    metrics='/diagnosis/metric_dict.json'
    var_imp_p='/diagnosis/var_imp_dict.json'

    BestModelSummary_p='/diagnosis/BestModelSummary.json'
    lift_and_gain_p='/diagnosis/lift.json'



    dat= pd.read_json(path+ metrics)
    #dat.round(2)
    #print(dat.round(2))

    var_imp= pd.read_json(path+ var_imp_p)  
    top_20=var_imp[var_imp.relative_importance>=0.01].nlargest(20,'relative_importance')


    plt.figure(figsize=(12, 6), tight_layout=True)
    ax = sns.barplot(data=top_20,x='relative_importance',y='feature', orient = 'h',palette=green ,order=top_20.sort_values('relative_importance', ascending=False).feature)
    ax.set(title=title+' Top 20 Features' ,xlabel='Experiment group', ylabel='% of group')
    plt.show()


    BestModelSummary=pd.read_json(path+ BestModelSummary_p) 
    print(BestModelSummary)

    lift_and_gain=pd.read_json(path+ lift_and_gain_p) 


    def unpack(settype, var):
        return lift_and_gain.loc[var][settype]


    baseline_percentage_train=unpack('T','baseline_percentage')
    baseline_percentage_test=unpack('M','baseline_percentage')
    lift_percentage_train=unpack('T','lift_percentage')
    lift_percentage_test=unpack('M','lift_percentage')

    baseline_index_train=unpack('T','baseline_index')
    baseline_index_test=unpack('M','baseline_index')

    lift_index_train=unpack('T','lift_index')
    lift_index_test=unpack('M','lift_index')

    gain_train=unpack('T','gain')
    gain_test=unpack('M','gain')

    Deciles=unpack('T','Deciles')



    data_preproc = pd.DataFrame({
        'Deciles': Deciles, 
        'Lift index train': lift_index_train,
        'Lift index test': lift_index_test,
        'baseline index': baseline_index_train})



    plt.figure(figsize=(12, 6), tight_layout=True)
    ax=sns.lineplot(x='Deciles', y='value', hue='variable', style="variable", markers=True, 
                 data=pd.melt(data_preproc, ['Deciles']))
    ax.set(title='Lift Chart' ,xlabel='Deciles', ylabel='Lift')
    plt.show()

    data_preproc_gain = pd.DataFrame({
        'Deciles': Deciles, 
        'Gain train': gain_train,
        'Gain test': gain_test,
        'baseline gain':  [x / 10 for x in Deciles]})

    plt.figure(figsize=(12, 6), tight_layout=True)
    ax=sns.lineplot(x='Deciles', y='value', hue='variable', style="variable", markers=True, 
                 data=pd.melt(data_preproc_gain, ['Deciles']))
    ax.set(title='Gain Chart' ,xlabel='Deciles', ylabel='Lift')
    plt.show()
    return dat.round(2),var_imp, BestModelSummary
 

def classification_metrics(dat,title,typeof='instore'):

    #if typeof=='instore':
    #    categories=["0-1800", '1800-4000' ,'4000-8000','8000-12000', '12000+' ]
    #if typeof=='ecom':
     #   categories=["0-500", '500-2000' ,'2000-3500','3500-6000', '6000+' ]
    if typeof in (['ecom_bigw' ,'ecom_bws']):
        categories=['Low','High']
      
    elif typeof in (['instore_bigw','instore_bws']):
        categories=['Low','Medium','High']
    elif typeof in(['ecom3m', 'ecom4']):
        categories=['Low','Low Medium','High Medium','High']
    else:
        categories=['Low','Low Medium','Medium','High Medium','High']
     
    dat.target_class = pd.Categorical(dat.target_class, 
                      categories=categories,
                      ordered=True)
    dat.pred_class = pd.Categorical(dat.pred_class, 
                      categories=categories,
                      ordered=True)
    labels=dat['target_class'].unique().sort_values()
    #labels = unique_labels(dat['target_class'],dat['pred_class'])
    conf_matrix = confusion_matrix(dat['target_class'].astype(str),dat['pred_class'].astype(str),labels=labels)
      
    col_sums=[sum(x) for x in zip(*conf_matrix)]

    
    print( skm.classification_report(dat['target_class'].astype(str),dat['pred_class'].astype(str)))
    def showmatrix(val,title):
        ax= plt.subplot()
        sns.heatmap(val , annot=True, fmt='.2%', ax=ax, cmap='Greens');  #annot=True to annotate cells, ftm='g' to disable scientific notation
        # labels, title and ticks
        ax.set_xlabel('Predicted labels');ax.set_ylabel('True labels'); 
        ax.set_title(title); 
        ax.xaxis.set_ticklabels(categories); 
        ax.yaxis.set_ticklabels(categories);
        plt.yticks(rotation=0) 
        plt.xticks(rotation=90) 
        plt.show()
  
    
    showmatrix(conf_matrix/col_sums,'Confusion Matrix - Category Total Distribution')
    showmatrix( conf_matrix/np.sum(conf_matrix),'Confusion Matrix - Total Distribution')

    
    FP = conf_matrix.sum(axis=0) - np.diag(conf_matrix)  
    FN = conf_matrix.sum(axis=1) - np.diag(conf_matrix)
    TP = np.diag(conf_matrix)
    TN = conf_matrix.sum() - (FP + FN + TP)

    def printacc(var,num):
        print(var, " \n True Positives:",TP[num],"\n True Negatives:",TN[num],"\n False Positivess:",FP[num],"\n False Negatives:" ,FN[num]," \n Accuracy: ",round(( (TP[num]+TN[num]) / (TP[num] + TN[num] + FP[num] + FN[num])),2)," \n Mis-Classification:",round( 1- ( (TP[num]+TN[num]) / (TP[num] + TN[num] + FP[num] + FN[num])),2))
        print(f'Sensitivity: {round((TP[num] / (TP[num] + FN[num])) ,2)}') 
        print(f'Specificity: {round((TN[num] / (TN[num] + FP[num])),2)}') 
        print(f'Precision: {round( (TN[num] / float(TN[num] + FP[num])),2)}')
        return pd.DataFrame({'True Positives':[TP[num]],
              'True Negatives':[TN[num]],
              'False Positivess':[FP[num]],
              'False Negatives':[FN[num]],
              'Accuracy':[round( (TP[num]+TN[num]) / (TP[num] + TN[num] + FP[num] + FN[num]),2)],
              'Mis-Classification':[round( 1- ( (TP[num]+TN[num]) / (TP[num] + TN[num] + FP[num] + FN[num])),2)],
              'Sensitivity':[round((TP[num] / (TP[num] + FN[num])) ,2)],
              'Specificity':[round((TN[num] / (TN[num] + FP[num])),2)],
              'Precision':[round( (TN[num] / float(TN[num] + FP[num])),2)]
             }, index=[var])


    if typeof in (['ecom_bigw' ,'ecom_bws']):
        d=printacc('Category Low:',0)
        d.append(printacc('Category High:',1))
    elif typeof in (['instore_bigw','instore_bws']):  
        d=printacc('Category Low:',0)
        d.append(printacc('Category Medium:',1))
        d.append(printacc('Category High:',2))
    elif typeof in (['ecom3m', 'ecom4']):  
        d=printacc('Category Low:',0)
        d.append(printacc('Category Low/Med:',1))
        d.append(printacc('Category Med/High:',2))
        d.append(printacc('Category High:',3))
    else:
        d=printacc('Category Low:',0)
        d.append(printacc('Category Low/Med:',1))
        d.append(printacc('Category Medium:',2))
        d.append(printacc('Category Med/High:',3))
        d.append(printacc('Category High:',4))
    
    FP = FP.sum()  
    FN = FN.sum() 
    TP = TP.sum() 
    TN = TN.sum() 
    
    
    # calculate accuracy
    conf_accuracy = ( (TP+TN) / (TP + TN + FP + FN))

    # calculate mis-classification
    conf_misclassification = 1- conf_accuracy

    # calculate the sensitivity
    conf_sensitivity = (TP / (TP + FN)) 
    # calculate the specificity
    conf_specificity = (TN / (TN + FP))

    # calculate precision
    conf_precision = (TN / float(TN + FP))
    # calculate f_1 score
    conf_f1 = 2 * ((conf_precision * conf_sensitivity) / (conf_precision + conf_sensitivity))
    print('-'*50)
    print(f'Accuracy: {round(conf_accuracy,2)}') 
    print(f'Mis-Classification: {round(conf_misclassification,2)}') 
    print(f'Sensitivity: {round(conf_sensitivity,2)}') 
    print(f'Specificity: {round(conf_specificity,2)}') 
    print(f'Precision: {round(conf_precision,2)}')
    print(f'f_1 Score: {round(conf_f1,2)}')
    return d