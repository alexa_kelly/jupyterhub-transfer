WITH base_crn as 
(
        select crn

    from `gcp-wow-rwds-ai-cfv-dev.azhang.true_audience_4879` 
    where pred_bin = 'hvcfv' 
        and cast(crn as string) in (select cast(crn as string) as crn 
                                    from loyalty.et_targeted_customer_offer 
                                    where campaign_code= 'RTN-4879')
    
-- select cast(crn as string) as crn ,
--         "control" as aud_type 
-- from `gcp-wow-rwds-ai-cfv-dev.asach.all_campaign_audience` 
-- where crn not in (select cast(crn as string) as crn
--                  from loyalty_campaign_analytics.rtn4879_final 
--                  where pred_bin = 'hvcfv' )
-- union DISTINCT
-- select cast(crn as string) as crn ,
--         "test" as aud_type 
-- from `gcp-wow-rwds-ai-cfv-dev.asach.all_campaign_audience` 
-- where crn in (select cast(crn as string) as crn 
--               from loyalty_campaign_analytics.rtn4879_final 
--               where pred_bin = 'hvcfv' )
-- and cast(crn as string) in (select cast(crn as string) as crn 
--                                     from loyalty.et_targeted_customer_offer 
--                                     where campaign_code= 'RTN-4879' 

--      )


),
Activation as 
(
Select distinct(cecoa.crn),1 as act_ind
    --    ce.cmpgn_code as campaign_code, offer_id
FROM        loyalty.campaign_exec ce
INNER JOIN  loyalty.campaign_exec_cust_offer_allocation cecoa 
on          ce.cmpgn_exec_id = cecoa.cmpgn_exec_id
and         cecoa.offer_id IN (-218616,-216217) 
-- /RTN-4879 campaign Offer Id's'/
--INNER JOIN  Audience wc
--on wc.crn = cecoa.crn 
WHERE       cecoa.activation_ts IS NOT NULL 
AND         ce.cmpgn_code = 'RTN-4879'  
AND         ce.test_exec_ind != 'Y'
-- AND         cecoa.offer_activn_start_date between '2020-08-11' and '2020-09-28'
),
RANKED AS 
(
    SELECT 
          D.crn
          ,D.pw_end_date
          ,ROW_NUMBER() OVER (PARTITION BY D.crn ORDER BY D.pw_end_date) AS DateRank    
          ,CASE WHEN substr(D.segment_cell_curr,1,2) = 'HV' THEN 'high value'
                WHEN substr(D.segment_cell_curr,1,2) = 'MV' THEN 'medium value'
                WHEN substr(D.segment_cell_curr,1,2) = 'LV' THEN 'low value'
                ELSE D.segment_cell_curr
           END AS cvm
          ,CASE WHEN substr(D.segment_cell_curr,1,2) = 'HV' THEN 3
                WHEN substr(D.segment_cell_curr,1,2) = 'MV' THEN 2
                WHEN substr(D.segment_cell_curr,1,2) = 'LV' THEN 1
                ELSE 0
           END AS cvm_rank

    FROM `wx-bq-poc.loyalty.customer_value_model` D

    -- WHERE D.pw_end_date in ("2021-09-21","2021-06-29")
       WHERE D.pw_end_date in ("2021-10-12","2021-06-29")
    

)

, Transition AS
(
    SELECT
        case when a.act_ind=1 then 1 else 0 end as act_ind
        ,bs.crn
        -- ,bs.aud_type
        ,FORMAT_DATE('%Y-%m-%d', src.pw_end_date) AS SourceDate

        ,src.cvm as SourceCVM
        ,src.cvm_rank as SourceCVMRank
        
        ,FORMAT_DATE('%Y-%m-%d', tgt.pw_end_date) AS TargetDate
        ,tgt.cvm as TargetCVM
        ,tgt.cvm_rank as TargetCVMRank

        ,CASE WHEN tgt.cvm_rank - src.cvm_rank = 0 THEN 'stable'
            WHEN tgt.cvm_rank - src.cvm_rank < 0 THEN 'down'
            WHEN tgt.cvm_rank - src.cvm_rank > 0 THEN 'up'
        END AS cvm_trend

FROM    
        base_crn bs
        left join Activation a
        on cast(bs.crn as string)=a.crn
        LEFT JOIN 
        RANKED AS src
        ON cast(bs.crn as string)=src.crn
    INNER JOIN 
        RANKED AS tgt
    ON src.crn = tgt.crn
        AND src.DateRank + 1 = tgt.DateRank
        -- AND DATE_DIFF(tgt.pw_end_date, src.pw_end_date, WEEK) <= 1
    
    --     INNER JOIN
    -- RANKED AS lgs
    -- ON lgs.crn = tgt.crn
    --     AND tgt.DateRank + 1 = lgs.DateRank
    --     AND DATE_DIFF(tgt.ref_dt, lgs.ref_dt, MONTH) <= 1
     )

    select
    T.SourceDate
    ,T.TargetDate
    -- ,T.LgsDate
    -- ,T.cfv_trend
    -- ,T.act_ind
    -- ,T.EComSourceSegment
    -- ,T.EComTargetSegment
    -- ,T.cfv_trend_n
    -- ,T.InstoreSourceSegment
    -- ,T.InstoreTargetSegment
    ,act_ind
    -- ,aud_type
    ,T.SourceCVM
    ,T.TargetCVM
    -- ,T.cvm_trend
    -- ,T.cvm_macro_segment_curr
    -- ,avg(T.InstoreSourceSegmentRank) as  InstoreSourceSegmentRank
    -- ,avg(T.InstoreTargetSegmentRank) as InstoreTargetSegmentRank
    
    -- ,avg(T.EcomSourceSegmentRank) as  EcomSourceSegmentRank
    -- ,avg(T.EcomTargetSegmentRank) as EcomTargetSegmentRank

    -- ,avg(T.CombinedSourceSegmentRank) as CombinedSourceSegmentRank
    -- ,avg(T.CombinedTargetSegmentRank) as CombinedTargetSegmentRank
    ,count(t.crn) as numcrns
    -- ,avg(SourcePredictedSpend) as SourcePredictedSpend
    -- ,avg(TargetPredictedSpend) as Avg_TargetPredictedSpend
from Transition T 

group by 
T.TargetDate
,T.SourceDate
-- ,T.LgsDate
    -- ,T.InstoreSourceSegment
    -- ,T.InstoreTargetSegment
    -- ,T.cfv_trend
    ,T.act_ind
    -- ,aud_type
    -- ,cvm_macro_segment_curr
    -- ,T.cfv_trend_n
    -- ,T.LgsDate
    -- ,T.SourceDate
    -- ,T.EComSourceSegment
    -- ,T.EComTargetSegment
    ,T.SourceCVM
    ,T.TargetCVM
    -- ,T.cvm_trend

order by 3, 1, 2
;